<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Background Module
 *
 * Background Setting
 *
 */
class Admin_background extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'background';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'access_background_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('background');		
		$this->load->model('background_m');
    }

    /**
	 * List all background
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'view_all_background') AND ! group_has_role('background', 'view_own_background')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/background/background/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->background_m->count_all_background();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['background']['entries'] = $this->background_m->get_background($pagination_config);
		$data['background']['total'] = count($data['background']['entries']);
		$data['background']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('background:background:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('background:background:plural'))
			->build('admin/background_index', $data);
    }
	
	/**
     * Display one background
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'view_all_background') AND ! group_has_role('background', 'view_own_background')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['background'] = $this->background_m->get_background_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('background', 'view_all_background')){
			if($data['background']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('background:background:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('background:background:plural'), '/admin/background/background/index')
			->set_breadcrumb(lang('background:background:view'))
			->build('admin/background_entry', $data);
    }
	
	/**
     * Create a new background entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'create_background')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_background('new')){	
				$this->session->set_flashdata('success', lang('background:background:submit_success'));				
				redirect('admin/background/background/index');
			}else{
				$data['messages']['error'] = lang('background:background:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/background/background/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('background:background:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('background:background:plural'), '/admin/background/background/index')
			->set_breadcrumb(lang('background:background:new'))
			->build('admin/background_form', $data);
    }
	
	/**
     * Edit a background entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the background to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'edit_all_background') AND ! group_has_role('background', 'edit_own_background')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('background', 'edit_all_background')){
			$entry = $this->background_m->get_background_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_background('edit', $id)){	
				$this->session->set_flashdata('success', lang('background:background:submit_success'));				
				redirect('admin/background/background/index');
			}else{
				$data['messages']['error'] = lang('background:background:submit_failure');
			}
		}
		
		$data['fields'] = $this->background_m->get_background_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/background/background/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('background:background:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('background:background:plural'), '/admin/background/background/index')
			->set_breadcrumb(lang('background:background:view'), '/admin/background/background/view/'.$id)
			->set_breadcrumb(lang('background:background:edit'))
			->build('admin/background_form', $data);
    }
	
	/**
     * Delete a background entry
     * 
     * @param   int [$id] The id of background to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('background', 'delete_all_background') AND ! group_has_role('background', 'delete_own_background')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('background', 'delete_all_background')){
			$entry = $this->background_m->get_background_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->background_m->delete_background_by_id($id);
        $this->session->set_flashdata('error', lang('background:background:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/background/background/index');
    }
	
	/**
     * Insert or update background entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_background($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('background:background:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->background_m->insert_background($values);
				
			}
			else
			{
				$result = $this->background_m->update_background($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}