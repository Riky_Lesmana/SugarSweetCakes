<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Background extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Background',
			'id' => 'Background',
		);
		$info['description'] = array(
			'en' => 'Background Setting',
			'id' => 'Background Setting',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'background';
		$info['roles'] = array(
			'access_background_backend', 'view_all_background', 'view_own_background', 'edit_all_background', 'edit_own_background', 'delete_all_background', 'delete_own_background', 'create_background',
		);
		
		if(group_has_role('background', 'access_background_backend')){
			$info['sections']['background']['name'] = 'background:background:plural';
			$info['sections']['background']['uri'] = 'admin/background/background/index';
			
			if(group_has_role('background', 'create_background')){
				$info['sections']['background']['shortcuts']['create'] = array(
					'name' => 'background:background:new',
					'uri' => 'admin/background/background/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		unset($menu_items['lang:cp:nav_background']);
		$menu_items['Background'] = 'admin/background/';
		$menu_order[2] = 'Background';
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// background
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'image_id' => array(
				'type' => 'char',
				'constraint' => '15',
				'default' => NULL,
				'null' => TRUE,
			),
			'active' => array(
				'type' => 'tinyint',
				'constraint' => '1',
				'default' => 0,
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('background', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_background (created_by)");

		$this->load->library('files/files');
		Files::create_folder(0,'Background');

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('background');


		$this->load->library('files/files');
		$id1 = Files::get_id_by_name('Background');
		Files::delete_folder($id1);

        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}