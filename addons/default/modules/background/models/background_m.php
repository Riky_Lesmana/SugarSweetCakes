<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Background model
 *
 * @author Aditya Satrya
 */
class Background_m extends MY_Model {
	
	public function get_background($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$this->db->order_by('active desc, id desc');

		$query = $this->db->get('default_background');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function count_all_background()
	{
		return $this->db->count_all('default_background');
	}
	
	public function get_active_background()
	{
		$this->db->select('*');
		
		$this->db->where('active', 1);

		$query = $this->db->get('default_background');
		$result = $query->row_array();
		
        return $result;
	}
	
	public function delete_background_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_background');
	}
	
	public function insert_background($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_background', $values);
	}

	public function set_deactive_all(){
		$values['active'] = 0;
		return $this->db->update('default_background', $values); 
	}

	public function set_active_background($row_id){
		$values['active'] = 1;
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_background', $values); 
	}
	
	public function update_background($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_background', $values); 
	}
	
}