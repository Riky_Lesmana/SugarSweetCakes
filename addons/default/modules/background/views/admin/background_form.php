<div class="page-header">
	<h1><?php echo lang('background:background:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="image_id"><?php echo lang('background:image_id'); ?></label>

		<div class="col-xs-10 col-md-3">
			<input type="file" name="image_id" id="kecil" />
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>

<script type="text/javascript">
$(document).ready(function() {
			
	/**/$('#kecil').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:true,
		onchange:null,
		thumbnail:'large', //| true | large
		whitelist:'gif|png|jpg|jpeg',
		blacklist:'exe|php'
		//onchange:''
		//
	});
});
</script>