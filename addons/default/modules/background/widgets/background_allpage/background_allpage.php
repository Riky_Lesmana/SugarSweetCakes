<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Background_allpage extends Widgets
{
	// The widget title,  this is displayed in the admin interface
	public $title = 'Background Active';

	//The widget description, this is also displayed in the admin interface.  Keep it brief.
	public $description = 'Menampilkan background yang sedang aktif.';

	//duh
	public $author = 'Riky H. Lesmana';

	//duh
	public $website = '';

	//current version of your widget
	public $version = '1.0';

	public function form()
	{
		$fields = $this->fields;

		return array('fields' => $fields);
	}

	public function run($options)
	{
		$this->load->model('background/background_m');

		$entries = $this->background_m->get_active_background();

		return array('entries' => $entries);
	}
}
/* End of file status_pelaporan_kegiatan.php */