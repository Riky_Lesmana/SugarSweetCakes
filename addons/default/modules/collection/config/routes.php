<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['collection/admin/collection(:any)'] = 'admin_collection$1';
$route['collection/collection(:any)'] = 'collection_collection$1';