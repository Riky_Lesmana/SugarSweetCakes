<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Collection Module
 *
 * module collection
 *
 */
class Admin_collection extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'collection';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'access_collection_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('collection');		
		$this->load->model('collection_m');
    }

    /**
	 * List all collection
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'view_all_collection') AND ! group_has_role('collection', 'view_own_collection')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/collection/collection/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->collection_m->count_all_collection();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		$data['cari'][0] = 'Tidak';
		$data['cari'][1] = 'Ya';
		
        $data['collection']['entries'] = $this->collection_m->get_collection($pagination_config);
		$data['collection']['total'] = count($data['collection']['entries']);
		$data['collection']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('collection:collection:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('collection:collection:plural'))
			->build('admin/collection_index', $data);
    }
	
	/**
     * Display one collection
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'view_all_collection') AND ! group_has_role('collection', 'view_own_collection')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['collection'] = $this->collection_m->get_collection_by_id($id);

		$data['cari'][0] = 'Tidak';
		$data['cari'][1] = 'Ya';
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'view_all_collection')){
			if($data['collection']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('collection:collection:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('collection:collection:plural'), '/admin/collection/collection/index')
			->set_breadcrumb(lang('collection:collection:view'))
			->build('admin/collection_entry', $data);
    }
	
	/**
     * Create a new collection entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'create_collection')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_collection('new')){	
				$this->session->set_flashdata('success', lang('collection:collection:submit_success'));				
				redirect('admin/collection/collection/index');
			}else{
				$data['messages']['error'] = lang('collection:collection:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/collection/collection/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('collection:collection:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('collection:collection:plural'), '/admin/collection/collection/index')
			->set_breadcrumb(lang('collection:collection:new'))
			->build('admin/collection_form', $data);
    }
	
	/**
     * Edit a collection entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the collection to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'edit_all_collection') AND ! group_has_role('collection', 'edit_own_collection')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('collection', 'edit_all_collection')){
			$entry = $this->collection_m->get_collection_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_collection('edit', $id)){	
				$this->session->set_flashdata('success', lang('collection:collection:edit_success'));				
				redirect('admin/collection/collection/index');
			}else{
				$data['messages']['error'] = lang('collection:collection:submit_failure');
			}
		}
		
		$data['fields'] = $this->collection_m->get_collection_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/collection/collection/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('collection:collection:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('collection:collection:plural'), '/admin/collection/collection/index')
			->set_breadcrumb(lang('collection:collection:view'), '/admin/collection/collection/view/'.$id)
			->set_breadcrumb(lang('collection:collection:edit'))
			->build('admin/collection_form', $data);
    }
	
	/**
     * Delete a collection entry
     * 
     * @param   int [$id] The id of collection to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('collection', 'delete_all_collection') AND ! group_has_role('collection', 'delete_own_collection')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('collection', 'delete_all_collection')){
			$entry = $this->collection_m->get_collection_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->collection_m->delete_collection_by_id($id);
        $this->session->set_flashdata('error', lang('collection:collection:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/collection/collection/index');
    }
	
	/**
     * Insert or update collection entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_collection($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', lang('collection:nama'), 'required');
		$this->form_validation->set_rules('deskripsi', lang('collection:deskripsi'), 'required');
		$this->form_validation->set_rules('detail', lang('collection:detail'), 'required');
		$this->form_validation->set_rules('kode', lang('collection:kode'), 'alpha_numeric');
		if ($method == 'new'){
			if (empty($_FILES['kecil']['name']))
			{
			    $this->form_validation->set_rules('kecil', 'Gambar List', 'required');
			}
			if (empty($_FILES['besar']['name']))
			{
			    $this->form_validation->set_rules('besar', 'Gambar Detail', 'required');
			}
		}
			
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			$this->load->library('files/files');
			$id1 = Files::get_id_by_name('Gambar List');
			$id2 = Files::get_id_by_name('Gambar Detail');

			if ($method == 'new')
			{
				if($_FILES['kecil']['name']!=''){
						$data = Files::upload($id1, $_FILES['kecil']['name'], 'kecil');
						$values['kecil'] = $data['data']['id'];
				}
				if($_FILES['besar']['name']!=''){
						$data = Files::upload($id2, $_FILES['besar']['name'], 'besar');
						$values['besar'] = $data['data']['id'];
				}

				$result = $this->collection_m->insert_collection($values);
				
			}
			else
			{
				if($_FILES['kecil']['name']!=''){
						$data = Files::upload($id1, $_FILES['kecil']['name'], 'kecil');
						$values['kecil'] = $data['data']['id'];
				}
				if($_FILES['besar']['name']!=''){
						$data = Files::upload($id2, $_FILES['besar']['name'], 'besar');
						$values['besar'] = $data['data']['id'];
				}
				if($this->input->post('show')!=1){
					$values['show'] = 0;
				}
				$result = $this->collection_m->update_collection($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}