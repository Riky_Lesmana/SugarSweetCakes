<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Collection Module
 *
 * module collection
 *
 */
class Collection extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'collection';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('collection');
		
		$this->load->model('collection_m');
    }

    /**
	 * List all collection
     *
     * @return	void
     */
    public function index()
    {

		$pagination_config['base_url'] = base_url(). 'collection/index/';
		$pagination_config['uri_segment'] = 3;
		$pagination_config['total_rows'] = $this->collection_m->count_all_collection();
		$pagination_config['per_page'] = 8;
		$pagination_config['full_tag_open'] = "<ul class='pagination'>";
		$pagination_config['full_tag_close'] ="</ul>";
		$pagination_config['num_tag_open'] = '<li>';
		$pagination_config['num_tag_close'] = '</li>';
		$pagination_config['cur_tag_open'] = "<li class='active'><a href='#'>";
		$pagination_config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$pagination_config['next_tag_open'] = "<li>";
		$pagination_config['next_tag_close'] = "</li>";
		$pagination_config['prev_tag_open'] = "<li>";
		$pagination_config['prev_tag_close'] = "</li>";
		$pagination_config['first_tag_open'] = "<li>";
		$pagination_config['first_tag_close'] = "</li>";
		$pagination_config['last_tag_open'] = "<li>";
		$pagination_config['last_tag_close'] = "</li>";
	    $pagination_config['first_link'] = FALSE;
	    $pagination_config['last_link'] = FALSE;
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['collection']['entries'] = $this->collection_m->get_collection_public($pagination_config);
		$data['collection']['total'] = count($data['collection']['entries']);
		$data['collection']['pagination'] = $this->pagination->create_links();
		$data['pageNow'] = 'collection';

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('collection:collection:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('collection:collection:plural'))
			->build('collection_index', $data);
    }
	
	/**
     * Display one collection
     *
     * @return  void
     */
    public function view($seo = '')
    {
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['collection'] = $this->collection_m->get_collection_by_seo($seo);
		$data['pageNow'] = 'collection';

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title($data['collection']['nama'].' | '.lang('collection:collection:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('collection:collection:plural'), '/collection/collection/index')
			->set_breadcrumb(lang('collection:collection:view'))
			->build('collection_entry', $data);
    }
		/*
		public function add_settings(){
			$settings = array(
				'cp_active' => array(
					'title' => 'Apakah Kontak AKtif ?',
					'description' => 'Jika anda memakai kontak, aktifkan ini.',
					'type' => 'radio',
					'default' => true,
					'value' => '',
					'options' => '1=Aktif|0=Tidak Aktif',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 1000,
				),
				'cp_name' => array(
					'title' => 'Nama Kontak',
					'description' => 'Nama Penerima dari Kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 999,
				),
				'cp_bbm' => array(
					'title' => 'Alamat BBM',
					'description' => 'Alamat dari BBM untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 998,
				),
				'cp_telepon' => array(
					'title' => 'Nomor Telepon',
					'description' => 'Nomor Telepon untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 997,
				),
				'cp_line' => array(
					'title' => 'Alamat Line',
					'description' => 'Alamat dari Line untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 996,
				),
				'cp_wa' => array(
					'title' => 'Alamat Whatsapp',
					'description' => 'Alamat dari Whatsapp untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 995,
				),
				'cp_email' => array(
					'title' => 'Alamat E-mail',
					'description' => 'Alamat dari E-mail untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 994,
				),
				'cp_ym' => array(
					'title' => 'Alamat Yahoo Messenger',
					'description' => 'Alamat dari Yahoo Messenger untuk melakukan kontak.',
					'type' => 'text',
					'default' => '',
					'value' => '',
					'options' => '',
					'is_required' => 0,
					'is_gui' => 1,
					'module' => 'Contact_Person',
					'order' => 993,
				),
			);
		foreach ($settings as $slug => $setting_info)
		{
			log_message('debug', '-- Settings: installing '.$slug);
			$setting_info['slug'] = $slug;
			if ( ! $this->db->insert('settings', $setting_info))
			{
				log_message('debug', '-- -- could not install '.$slug);

				return false;
			}
		}

		return true;
		}
		*/
}