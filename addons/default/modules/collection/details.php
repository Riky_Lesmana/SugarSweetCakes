<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Collection extends Module
{
    public $version = '1.2';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Collection',
			'id' => 'Collection',
		);
		$info['description'] = array(
			'en' => 'module collection',
			'id' => 'module collection',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		//$info['menu'] = 'Collection';
		$info['roles'] = array(
			'access_collection_backend', 'view_all_collection', 'view_own_collection', 'edit_all_collection', 'edit_own_collection', 'delete_all_collection', 'delete_own_collection', 'create_collection',
		);
		
		if(group_has_role('collection', 'access_collection_backend')){
			$info['sections']['collection']['name'] = 'collection:collection:plural';
			$info['sections']['collection']['uri'] = 'admin/collection/collection/index';
			
			if(group_has_role('collection', 'create_collection')){
				$info['sections']['collection']['shortcuts']['create'] = array(
					'name' => 'collection:collection:new',
					'uri' => 'admin/collection/collection/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		
		$menu_items["lang:cp:nav_Collection"] = 'admin/collection/collection/index';
		
		// Menu ordering
		$menu_order[0] = 'lang:cp:nav_Collection';
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// collection
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '30',
				'null' => FALSE,
			),
			'slug' => array(
				'type' => 'varchar',
				'constraint' => '30',
				'null' => FALSE,
			),
			'kecil' => array(
				'type' => 'char',
				'constraint' => '15',
				'default' => '0',
				'null' => TRUE,
			),
			'besar' => array(
				'type' => 'char',
				'constraint' => '15',
				'default' => '0',
				'null' => TRUE,
			),
			'deskripsi' => array(
				'type' => 'text',
				'null' => TRUE,
			),
			'detail' => array(
				'type' => 'text',
				'null' => TRUE,
			),
			'show' => array(
				'type' => 'tinyint',
				'constraint' => '1',
				'default' => '0',
				'null' => TRUE,
			),
			'urutan' => array(
				'type' => 'int',
				'constraint' => '11',
				'default' => '0',
				'null' => TRUE,
			),
			'harga' => array(
				'type' => 'INT',
				'constraint' => 11,
				'default' => '0',
				'null' => TRUE,
			),
			'jual' => array(
				'type' => 'tinyint',
				'constraint' => '1',
				'default' => '0',
				'null' => TRUE,
			),
			'kode' => array(
				'type' => 'varchar',
				'constraint' => '10',
				'default' => NULL,
				'null' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('collection', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_collection(created_by)");

		//membuat folder untuk penyimpanan file
		$this->load->library('files/files');
		Files::create_folder(0,'Gambar List');
		Files::create_folder(0,'Gambar Detail');

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('collection');

        //menghapus folder
		$this->load->library('files/files');
		$id1 = Files::get_id_by_name('Gambar List');
		$id2 = Files::get_id_by_name('Gambar Detail');
		Files::delete_folder($id1);
		Files::delete_folder($id2);

        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}