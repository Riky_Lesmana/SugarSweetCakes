<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Collection model
 *
 * @author Aditya Satrya
 */
class Collection_m extends MY_Model {
	
	public function get_collection($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if($this->input->get('f-nama')){
			$this->db->like('nama', $this->input->get('f-nama'));
		}

		if($this->input->get('f-show')!=''){
			$this->db->where('show', $this->input->get('f-show'));
		}

		if($this->input->get('f-jual')!=''){
			$this->db->where('jual', $this->input->get('f-jual'));
		}

		$this->db->order_by('id desc');
		
		$query = $this->db->get('default_collection');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_collection_public($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->order_by('id desc');
		
		$query = $this->db->get('default_collection');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_last_n_collection($limit = 8)
	{
		$this->db->select('*');

		$this->db->order_by('id desc');
		
		$query = $this->db->get('default_collection', $limit);
		$result = $query->result_array();
		
        return $result;
	}

	public function get_collection_by_seo($seo)
	{
		$this->db->select('*');
		$this->db->like('slug', $seo);
		$query = $this->db->get('default_collection');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function get_collection_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_collection');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_collection()
	{

		if($this->input->get('f-nama')){
			$this->db->like('nama', $this->input->get('f-nama'));
		}

		if($this->input->get('f-show')!=''){
			$this->db->where('show', $this->input->get('f-show'));
		}

		if($this->input->get('f-jual')!=''){
			$this->db->where('jual', $this->input->get('f-jual'));
		}
		
		return $this->db->count_all_results('default_collection');
	}
	
	public function delete_collection_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_collection');
	}
	
	public function insert_collection($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_collection', $values);
	}
	
	public function update_collection($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_collection', $values); 
	}
	
}