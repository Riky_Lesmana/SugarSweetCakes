<div class="page-header">
	<h1><?php echo lang('collection:collection:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/collection/collection/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('collection:back') ?>
		</a>

		<?php if(group_has_role('collection', 'edit_all_collection')){ ?>
			<a href="<?php echo site_url('admin/collection/collection/edit/'.$collection['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('collection', 'edit_own_collection')){ ?>
			<?php if($collection->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/collection/collection/edit/'.$collection->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('collection', 'delete_all_collection')){ ?>
			<a href="<?php echo site_url('admin/collection/collection/delete/'.$collection['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('collection', 'delete_own_collection')){ ?>
			<?php if($collection->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/collection/collection/delete/'.$collection['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $collection['id']; ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:nama'); ?></div>
		<?php if(isset($collection['nama'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:slug'); ?></div>
		<?php if(isset($collection['slug'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['slug']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:kode'); ?></div>
		<?php if(isset($collection['kode'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['kode']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:harga'); ?></div>
		<?php if(isset($collection['harga'])){ ?>
		<div class="entry-detail-value">Rp. <?php echo number_format($collection['harga'], 0, ',', '.'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:kecil'); ?></div>
		<?php if(isset($collection['kecil'])){ ?>
		<div class="entry-detail-value"><img src="<?php echo base_url()."files/thumb/".$collection['kecil']."/200/"; ?>"></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:besar'); ?></div>
		<?php if(isset($collection['besar'])){ ?>
		<div class="entry-detail-value"><img src="<?php echo base_url()."files/thumb/".$collection['besar']."/250/"; ?>"></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:deskripsi'); ?></div>
		<?php if(isset($collection['deskripsi'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['deskripsi']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:detail'); ?></div>
		<?php if(isset($collection['detail'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['detail']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:show'); ?></div>
		<?php if(isset($collection['show'])){ ?>
		<div class="entry-detail-value"><?php echo $cari[$collection['show']]; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:jual'); ?></div>
		<?php if(isset($collection['jual'])){ ?>
		<div class="entry-detail-value"><?php echo $cari[$collection['jual']]; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:urutan'); ?></div>
		<?php if(isset($collection['urutan'])){ ?>
		<div class="entry-detail-value"><?php echo $collection['urutan']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:created'); ?></div>
		<?php if(isset($collection['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($collection['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:updated'); ?></div>
		<?php if(isset($collection['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($collection['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('collection:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($collection['created_by'], true); ?></div>
	</div>
</div>