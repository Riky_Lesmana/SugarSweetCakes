<div class="page-header">
	<h1><?php echo lang('collection:collection:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="nama"><?php echo lang('collection:nama'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" id="name" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" />
		</div>
	</div>
			<?php 
				$value = NULL;
				if($this->input->post('slug') != NULL){
					$value = $this->input->post('slug');
				}elseif($mode == 'edit'){
					$value = $fields['slug'];
				}
			?>
			<input name="slug" type="hidden" id="slug" value="<?php echo $value; ?>" />

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="harga"><?php echo lang('collection:harga'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = '0';
				if($this->input->post('harga') != NULL){
					$value = $this->input->post('harga');
				}elseif($mode == 'edit'){
					$value = $fields['harga'];
				}
			?>
			<input name="harga" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="kode"><?php echo lang('collection:kode'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('kode') != NULL){
					$value = $this->input->post('kode');
				}elseif($mode == 'edit'){
					$value = $fields['kode'];
				}
			?>
			<input name="kode" type="text" maxlength="10" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="kecil"><?php echo lang('collection:kecil'); ?></label>

		<div class="col-xs-3">
			<?php 
				$value = NULL;
				if($this->input->post('kecil') != NULL){
					$value = $this->input->post('kecil');
				}elseif($mode == 'edit'){
					$value = $fields['kecil'];
				}
			?>
			<input type="file" name="kecil" id="kecil" />

		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="besar"><?php echo lang('collection:besar'); ?></label>

		<div class="col-xs-3">
			<?php 
				$value = NULL;
				if($this->input->post('besar') != NULL){
					$value = $this->input->post('besar');
				}elseif($mode == 'edit'){
					$value = $fields['besar'];
				}
			?>
			<input type="file" name="besar" id="besar" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="deskripsi"><?php echo lang('collection:deskripsi'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('deskripsi') != NULL){
					$value = $this->input->post('deskripsi');
				}elseif($mode == 'edit'){
					$value = $fields['deskripsi'];
				}
			?>
			<textarea name="deskripsi" maxlength="250" class="limited col-xs-12 col-sm-3" rows="7" id=""><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="detail"><?php echo lang('collection:detail'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('detail') != NULL){
					$value = $this->input->post('detail');
				}elseif($mode == 'edit'){
					$value = $fields['detail'];
				}
			?>
			<textarea name="detail" maxlength="1000" class="limited col-xs-10 col-sm-6" rows="15"><?php echo $value; ?></textarea>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for=""><?php echo lang('collection:show'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('show') != NULL){
					$value = $this->input->post('show');
				}elseif($mode == 'edit'){
					$value = $fields['show'];
				}
			?>
			<label class="pull-left">
				<input <?php if($value!='0'){echo "checked ";} ?> name="show" class="ace ace-switch ace-switch-5" type="checkbox" value="1">
				<span class="lbl"></span>
			</label>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for=""><?php echo lang('collection:jual'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('jual') != NULL){
					$value = $this->input->post('jual');
				}elseif($mode == 'edit'){
					$value = $fields['jual'];
				}
			?>
			<label class="pull-left">
				<input <?php if($value!='0'){echo "checked ";} ?> name="jual" class="ace ace-switch ace-switch-5" type="checkbox" value="1">
				<span class="lbl"></span>
			</label>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>
<script type="text/javascript">
$(document).ready(function() {
  $('#name').keyup(function(event) {
  	TextGanti = $(this).val();
  	$('#slug').val(convertToSlug(TextGanti));
  });
			
				
	/**/$('#kecil, #besar').ace_file_input({
		no_file:'No File ...',
		btn_choose:'Choose',
		btn_change:'Change',
		droppable:true,
		onchange:null,
		thumbnail:'large', //| true | large
		whitelist:'gif|png|jpg|jpeg',
		blacklist:'exe|php'
		//onchange:''
		//
	});

	$('textarea.limited').inputlimiter({
		remText: 'Tersisa %n huruf...',
		limitText: '<br/>Max %n huruf.'
	});

});

function convertToSlug(TextGanti)
{
    return TextGanti.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'_');
}
</script>