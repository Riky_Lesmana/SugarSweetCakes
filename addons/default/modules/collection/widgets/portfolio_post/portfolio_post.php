<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Widget_Portfolio_post extends Widgets
{
	// The widget title,  this is displayed in the admin interface
	public $title = 'Portfolio Post';

	//The widget description, this is also displayed in the admin interface.  Keep it brief.
	public $description = 'Menampilkan beberapa portfolio diwidget.';

	//duh
	public $author = 'Riky H. Lesmana';

	//duh
	public $website = '';

	//current version of your widget
	public $version = '1.0';

	public function form()
	{
		$fields = $this->fields;

		return array('fields' => $fields);
	}

	public function run($options)
	{
		$this->load->model('collection/collection_m');

		$entries = $this->collection_m->get_last_n_collection();

		return array('entries' => $entries);
	}
}
/* End of file status_pelaporan_kegiatan.php */