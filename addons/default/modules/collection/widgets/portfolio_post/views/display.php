<h1>Portfolio Terbaru</h1>
	<div class="row">
		<?php foreach($entries as $entry){ ?>
		<div class="col-lg-3 col-md-4 col-xs-6 thumb">
			<a href="<?php echo BASE_URL.'portfolio/view/'.$entry['slug'] ?>" class="thumbnail">
				<img src="<?php echo BASE_URL.'files/thumb/'.$entry['besar'].'/200/200/fit/' ?>" title="<?php echo $entry['nama'] ?>" alt="<?php echo $entry['nama'] ?>" width="200" height="200">
				<div><h3 class="text-center"><?php echo $entry['nama'] ?></h3></div>
			</a>
		</div>
		<?php } ?>
	</div>