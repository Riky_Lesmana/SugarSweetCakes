<?php
foreach ($fields as $data) {
?>
	<div class="form-group">
		<label class="form-field-8"><?php echo $data['label']; ?></label>
		<br />
		<?php echo form_input($data['field'], set_value($data['field'], isset($options[$data['field']]) ? $options[$data['field']] : '0')) ?>
		<span class="required-icon tooltip"><?php echo lang($data['rules'].'_label') ?></span>
	</div>
<?php
}
?>