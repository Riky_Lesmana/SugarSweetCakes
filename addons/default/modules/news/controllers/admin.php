<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * News Module
 *
 * News Module override Blog
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/news/article/index');
    }

}