<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * News Module
 *
 * News Module override Blog
 *
 */
class Admin_article extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'article';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'access_article_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('news');		
		$this->load->model('article_m');
    }

    /**
	 * List all article
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'view_all_article') AND ! group_has_role('news', 'view_own_article')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/news/article/index';
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->article_m->count_all_article();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['article']['entries'] = $this->article_m->get_article($pagination_config);
		$data['article']['total'] = count($data['article']['entries']);
		$data['article']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('news:article:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('news:article:plural'))
			->build('admin/article_index', $data);
    }
	
	/**
     * Display one article
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'view_all_article') AND ! group_has_role('news', 'view_own_article')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['article'] = $this->article_m->get_article_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('news', 'view_all_article')){
			if($data['article']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('news:article:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('news:article:plural'), '/admin/news/article/index')
			->set_breadcrumb(lang('news:article:view'))
			->build('admin/article_entry', $data);
    }
	
	/**
     * Create a new article entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'create_article')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_article('new')){	
				$this->session->set_flashdata('success', lang('news:article:submit_success'));				
				redirect('admin/news/article/index');
			}else{
				$data['messages']['error'] = lang('news:article:submit_failure');
			}
		}
		
		$data['mode'] = 'new';
		$data['return'] = 'admin/news/article/index';
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('news:article:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('news:article:plural'), '/admin/news/article/index')
			->set_breadcrumb(lang('news:article:new'))
			->build('admin/article_form', $data);
    }
	
	/**
     * Edit a article entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the article to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'edit_all_article') AND ! group_has_role('news', 'edit_own_article')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('news', 'edit_all_article')){
			$entry = $this->article_m->get_article_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_article('edit', $id)){	
				$this->session->set_flashdata('success', lang('news:article:submit_success'));				
				redirect('admin/news/article/index');
			}else{
				$data['messages']['error'] = lang('news:article:submit_failure');
			}
		}
		
		$data['fields'] = $this->article_m->get_article_by_id($id);
		$data['mode'] = 'edit';
		$data['return'] = 'admin/news/article/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('news:article:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('news:article:plural'), '/admin/news/article/index')
			->set_breadcrumb(lang('news:article:view'), '/admin/news/article/view/'.$id)
			->set_breadcrumb(lang('news:article:edit'))
			->build('admin/article_form', $data);
    }
	
	/**
     * Delete a article entry
     * 
     * @param   int [$id] The id of article to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('news', 'delete_all_article') AND ! group_has_role('news', 'delete_own_article')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('news', 'delete_all_article')){
			$entry = $this->article_m->get_article_by_id($id);
			$created_by_user_id = $entry['created_by'];
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->article_m->delete_article_by_id($id);
        $this->session->set_flashdata('error', lang('news:article:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/news/article/index');
    }
	
	/**
     * Insert or update article entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_article($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('field_name', lang('news:article:field_name'), 'required');
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result = $this->article_m->insert_article($values);
				
			}
			else
			{
				$result = $this->article_m->update_article($values, $row_id);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}