<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * News Module
 *
 * News Module override Blog
 *
 */
class News extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'article';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('news');
		
		$this->load->model('article_m');
    }

    /**
	 * List all article
     *
     * @return	void
     */
    public function index()
    {
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'news/article/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->article_m->count_all_article();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['article']['entries'] = $this->article_m->get_article();
		$data['article']['total'] = count($data['article']['entries']);
		$data['article']['pagination'] = $this->pagination->create_links();
        $data['featured'] = $this->article_m->get_featured();
        $data['recent'] = $this->article_m->get_recent();
        $data['tags'] = $this->article_m->get_tags();
		$data['pageNow'] = 'news';

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title(lang('news:article:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('news:article:plural'))
			->build('article_index', $data);
    }

    /**
	 * List all article filtered by tags
     *
     * @return	void
     */
    public function tags($tags = '')
    {
		
        // -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'news/article/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->article_m->count_all_article();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------
		
        $data['article']['entries'] = $this->article_m->get_article_tags($tags);
		$data['article']['total'] = count($data['article']['entries']);
		$data['article']['pagination'] = $this->pagination->create_links();
        $data['featured'] = $this->article_m->get_featured();
        $data['recent'] = $this->article_m->get_recent();
        $data['tags'] = $this->article_m->get_tags();
		$data['pageNow'] = 'news';
        $data['tagsNow'] = $tags;

		// -------------------------------------
		// Build the page. 
		// -------------------------------------
		
        $this->template->title("Search by Tags (".$tags.")".lang('news:article:plural'))
			->set_breadcrumb('Home', '/')
			->set_breadcrumb(lang('news:article:plural'))
			->build('article_tags', $data);
    }
	
	/**
     * Display one article
     *
     * @return  void
     */
    public function view($seo = '', $month = 0, $year = 0)
    {
		// -------------------------------------
		// Get our entry.
		// -------------------------------------

		
        $data['article'] = $this->article_m->get_article_complete($seo,$month,$year);
        $data['featured'] = $this->article_m->get_featured();
        $data['recent'] = $this->article_m->get_recent();
        $data['tags'] = $this->article_m->get_tags();
		$data['pageNow'] = 'news';

		// -------------------------------------
		// Build the page.
		// -------------------------------------
		
        $this->template->title($data['article']['title']." | ".lang('news:article:view'))
			->set_breadcrumb('Home', '/home')
			->set_breadcrumb(lang('news:article:plural'), '/news/article/index')
			->set_breadcrumb(lang('news:article:view'))
			->build('article_entry', $data);
    }

	// --------------------------------------------------------------------------

}