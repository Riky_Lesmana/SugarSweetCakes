<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_News extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Tips',
			'id' => 'Tips',
		);
		$info['description'] = array(
			'en' => 'News Module override Blog',
			'id' => 'News Module override Blog',
		);
		$info['frontend'] = true;
		$info['backend'] = false;
		$info['menu'] = 'tips';
		$info['roles'] = array(
			'access_article_backend', 'view_all_article', 'view_own_article', 'edit_all_article', 'edit_own_article', 'delete_all_article', 'delete_own_article', 'create_article',
		);
		
		if(group_has_role('news', 'access_article_backend')){
			$info['sections']['article']['name'] = 'news:article:plural';
			$info['sections']['article']['uri'] = 'admin/news/article/index';
			
			if(group_has_role('news', 'create_article')){
				$info['sections']['article']['shortcuts']['create'] = array(
					'name' => 'news:article:new',
					'uri' => 'admin/news/article/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}