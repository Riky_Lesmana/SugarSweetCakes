<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Article model
 *
 * @author Aditya Satrya
 */
class Article_m extends MY_Model {
	
	public function get_article($pagination_config = NULL)
	{
		$this->db->select('default_blog.*,  GROUP_CONCAT( default_keywords.name )  tags_name');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->join('default_keywords_applied', 'default_blog.tags = default_keywords_applied.hash', 'left');
		$this->db->join('default_keywords', 'default_keywords_applied.keyword_id = default_keywords.id', 'left');
		$this->db->group_by('default_blog.id');
		$this->db->order_by('created_on desc');
		
		$query = $this->db->get('default_blog');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_article_tags($tags, $pagination_config = NULL)
	{
		$this->db->select('default_blog.*,  GROUP_CONCAT( default_keywords.name )  tags_name');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		$this->db->join('default_keywords_applied', 'default_blog.tags = default_keywords_applied.hash', 'left');
		$this->db->join('default_keywords', 'default_keywords_applied.keyword_id = default_keywords.id', 'left');
		$this->db->group_by('default_blog.id');
		$this->db->order_by('created_on desc');
		$this->db->where('default_keywords.name', $tags);
		
		$query = $this->db->get('default_blog');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_featured()
	{
		$this->db->select('default_blog.*,  GROUP_CONCAT( default_keywords.name )  tags_name');

		$this->db->join('default_keywords_applied', 'default_blog.tags = default_keywords_applied.hash', 'left');
		$this->db->join('default_keywords', 'default_keywords_applied.keyword_id = default_keywords.id', 'left');
		$this->db->group_by('default_blog.id');
		
		$query = $this->db->get('default_blog',2);
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_recent()
	{
		$this->db->select('default_blog.*,  GROUP_CONCAT( default_keywords.name )  tags_name');

		$this->db->join('default_keywords_applied', 'default_blog.tags = default_keywords_applied.hash', 'left');
		$this->db->join('default_keywords', 'default_keywords_applied.keyword_id = default_keywords.id', 'left');
		$this->db->group_by('default_blog.id');
		$this->db->order_by('created_on desc');
		
		$query = $this->db->get('default_blog',3);
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_article_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$this->db->group_by('default_blog.id');
		$query = $this->db->get('default_blog');
		$result = $query->row_array();
		
		return $result;
	}

	public function get_article_complete($seo, $month, $year)
	{
		$this->db->select('default_blog.*,  GROUP_CONCAT( default_keywords.name )  tags_name');

		$this->db->join('default_keywords_applied', 'default_blog.tags = default_keywords_applied.hash', 'left');
		$this->db->join('default_keywords', 'default_keywords_applied.keyword_id = default_keywords.id', 'left');
		$this->db->where('slug', $seo);
		$this->db->where('date_format(created,"%m")', $month);
		$this->db->where('date_format(created,"%Y")', $year);
		$query = $this->db->get('default_blog');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_article()
	{
		return $this->db->count_all('default_blog');
	}
	
	public function delete_article_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_blog');
	}
	
	public function insert_article($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_blog', $values);
	}
	
	public function update_article($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_blog', $values); 
	}
	
	public function get_tags()
	{
		
		$query = $this->db->get('default_keywords');
		$result = $query->result_array();
		
        return $result;
	}
	
}