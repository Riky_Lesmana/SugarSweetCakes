
      <div class="baseHome">
        <div class="col-md-9">
            <br/>
          <div class="details">
            <h3 class="pinkHead"><?php echo $article['title']; ?></h3>
            <p class="date"><?php echo date('M j, Y', $article['created_on']); ?></p>
				<?php echo $article['body']; ?>
			<br/><br/>
            <div class="clearfix">
              Tags : 
             <?php 
             $tagss = explode(',', $article['tags_name']);
             foreach ($tagss as $data) {
             ?>
              <a href="<?php echo base_url()."news/tags/".$data; ?>/"><?php echo $data; ?></a>
             <?php
             }
             ?>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-md-3 rightCon">
          <div class="col-xs-12">
            <h3 class="pinkHead">Featured Post</h3>
            <hr />
          <?php
            foreach ($featured as $data) {
          ?>
          <a href="<?php echo base_url()."news/view/".$data['slug']."/".date('m', $data['created_on'])."/".date('Y', $data['created_on'])."/";?>" >
            <img src="<?php echo base_url(); ?>/files/thumb/<?php echo $data['image']; ?>/170/">
            <h4><a href="<?php echo base_url()."news/view/".$data['slug']."/".date('m', $data['created_on'])."/".date('Y', $data['created_on'])."/";?>"><?php echo $data['title']; ?></a></h4>
            <p class="date"><?php echo date('M j, Y', $data['created_on']); ?></p>
            <div class="clearfix"></div>
          </a>
          <?
            }
          ?>
          </div>

          <div class="col-xs-12 recentPost">
            <h3 class="pinkHead">Recent Post</h3>
            <hr />

          <?php
            foreach ($recent as $data) {
          ?>
            <div class="col-xs-12 recentPost">
              <a href="<?php echo base_url()."news/view/".$data['slug']."/".date('m', $data['created_on'])."/".date('Y', $data['created_on'])."/";?>" >
                <img src="<?php echo base_url(); ?>/files/thumb/<?php echo $data['image']; ?>/70/50/" width="70" height="50" align="left">
              </a>
              <p><a href="<?php echo base_url()."news/view/".$data['slug']."/".date('m', $data['created_on'])."/".date('Y', $data['created_on'])."/";?>"><?php echo $data['title']; ?></a></p>
              <p class="date small"><?php echo date('M j, Y', $data['created_on']); ?></p>
            </div>
          <?
            }
          ?>
          </div>

          <div class="col-xs-12 searchTags">
            <h3 class="pinkHead">Search By Tags</h3>
            <hr />
            <?php
            foreach ($tags as $data) {
            ?>
            <a href="<?php echo base_url()."news/tags/".$data['name']; ?>/"><?php echo $data['name']; ?></a>
            <?php
            }
            ?>
          </div>

          <div class="col-xs-12 followus">
            <h3 class="pinkHead">Follow Us</h3>
            <hr />
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>"><img src="{{ theme:image_path  }}facebook.png" width="23"></a>
            <a target="_blank" href="https://twitter.com/home?status=<?php echo current_url(); ?>"><img src="{{ theme:image_path  }}twitter.png" width="23"></a>
            <a target="_blank" href="https://plus.google.com/share?url=<?php echo current_url(); ?>"><img src="{{ theme:image_path  }}Gplus.png" width="23"></a>
          </div>
        </div>

        <div class="clearfix"></div><br/>
      </div>