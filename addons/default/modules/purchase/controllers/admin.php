<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Purchase Module
 *
 * Modul untuk pemesanan
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/purchase/pemesanan/index');
    }

}