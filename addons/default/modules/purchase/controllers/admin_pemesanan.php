<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Purchase Module
 *
 * Modul untuk pemesanan
 *
 */
class Admin_pemesanan extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'pemesanan';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'access_pemesanan_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('purchase');		
		$this->load->model('pemesanan_m');
    }

    /**
	 * List all pemesanan
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'view_all_pemesanan') AND ! group_has_role('purchase', 'view_own_pemesanan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = base_url(). 'admin/purchase/index';
		$pagination_config['uri_segment'] = 4;
		$pagination_config['total_rows'] = $this->pemesanan_m->count_all_pemesanan();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get entries
		// -------------------------------------

		$data['status'][''] = '-- Status --';
		$data['status'][] = 'Baru';
		$data['status'][] = 'Dikonfirmasi';
		$data['status'][] = 'Dikirim';
		$data['status'][] = 'Diterima';
		$data['status'][] = 'Ditolak';
		
        $data['pemesanan']['entries'] = $this->pemesanan_m->get_pemesanan($pagination_config);
		$data['pemesanan']['total'] = count($data['pemesanan']['entries']);
		$data['pemesanan']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('purchase:pemesanan:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('purchase:pemesanan:plural'))
			->build('admin/pemesanan_index', $data);
    }
	
	/**
     * Delete one pemesanan
     *
     * @return  void
     */
    public function delete($id = 0)
    {
		
		if(! group_has_role('purchase', 'delete_all_pemesanan') AND ! group_has_role('purchase', 'delete_own_pemesanan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $data['pemesanan'] = $this->pemesanan_m->get_pemesanan_by_id($id);

		if(! group_has_role('purchase', 'delete_all_pemesanan')){
			if($data['pemesanan']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

        $this->pemesanan_m->delete_pemesanan_by_id($id);
		$this->session->set_flashdata('error', lang('configuration:config:deleted'));


		redirect('admin/purchase/index');
    }
	
	/**
     * Display one pemesanan
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'view_all_pemesanan') AND ! group_has_role('purchase', 'view_own_pemesanan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['pemesanan'] = $this->pemesanan_m->get_pemesanan_by_id($id);

		$data['status'][''] = '-- Status --';
		$data['status'][] = 'Baru';
		$data['status'][] = 'Dikonfirmasi';
		$data['status'][] = 'Dikirim';
		$data['status'][] = 'Diterima';
		$data['status'][] = 'Ditolak';
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'view_all_pemesanan')){
			if($data['pemesanan']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('purchase:pemesanan:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('purchase:pemesanan:plural'), '/admin/purchase/pemesanan/index')
			->set_breadcrumb(lang('purchase:pemesanan:view'))
			->build('admin/pemesanan_entry', $data);
    }
	
	/**
     * Display one pemesanan
     *
     * @return  void
     */
    public function download($id = 0)
    {
    	$this->load->helper('print');
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'view_all_pemesanan') AND ! group_has_role('purchase', 'view_own_pemesanan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry.
		// -------------------------------------
		
        $data['pemesanan'] = $this->pemesanan_m->get_pemesanan_by_id($id);

		$data['status'][''] = '-- Status --';
		$data['status'][] = 'Baru';
		$data['status'][] = 'Dikonfirmasi';
		$data['status'][] = 'Dikirim';
		$data['status'][] = 'Diterima';
		$data['status'][] = 'Ditolak';
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'view_all_pemesanan')){
			if($data['pemesanan']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        //$this->template->build('admin/pemesanan_entry', $data);
        print_html($data['pemesanan'], $data['status']);
    }
	
	/**
     * Edit a pemesanan entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the pemesanan to the be deleted.
     * @return	void
     */
    public function editStatus($id = 0, $status = NULL)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('purchase', 'edit_all_pemesanan') AND ! group_has_role('purchase', 'edit_own_pemesanan')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		if($id == 0 OR $status == NULL){
			$this->session->set_flashdata('error', 'Terjadi kesalahan saat perubahan data.');
			redirect('admin/purchase/');
		}
		
		// Check view all/own permission
		if(! group_has_role('purchase', 'edit_all_pemesanan')){
			$entry = $this->pemesanan_m->get_pemesanan_by_id($id);
		}
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		$values['status'] = $status;
		if($this->pemesanan_m->update_pemesanan($values, $id)){	
			$this->session->set_flashdata('success', 'Status pemesanan telah berhasil dirubah.');
			redirect('admin/purchase/index/');
		}else{
			$data['messages']['error'] = lang('purchase:pemesanan:submit_failure');
			redirect('admin/purchase/index/');
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
    }

	// --------------------------------------------------------------------------

}