<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Purchase Module
 *
 * Modul untuk pemesanan
 *
 */
class Purchase extends Public_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'pemesanan';
	
    public function __construct()
    {
        parent::__construct();

        // -------------------------------------
		// Load everything we need
		// -------------------------------------

		$this->lang->load('buttons');
        $this->lang->load('purchase');
		
		$this->load->model('pemesanan_m');
        $this->load->helper('recaptcha');
    }

    /**
	 * List all pemesanan
     *
     * @return	void
     */
    public function buy($kode = NULL)
    {
        $data['items'] = $this->pemesanan_m->get_items();
        if(isset($kode)){
       		$data['barang'] = $this->pemesanan_m->get_items_by_kode($kode);
        }else{
        	$data['barang'] = 0;
        }
		$data['pageNow'] = 'purchase';

		// -------------------------------------
		// Build the page. 
		// -------------------------------------

		
		if($_POST){
			if($this->_update_pemesanan('new')){	
				$this->session->set_flashdata('success', lang('purchase:pemesanan:submit_success'));			
				redirect('purchase/complete');
			}else{
				//$data['messages']['error'] = lang('purchase:pemesanan:submit_failure');
			}
		}
		
        $this->template->title('Purchase')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb('Purchase')
			->build('buy', $data);
    }

    /**
	 * List all pemesanan
     *
     * @return	void
     */
    public function index()
    {
		$this->buy();
    }

    /**
	 * List all pemesanan
     *
     * @return	void
     */
    public function yahoo($alamat)
    {
		$status = file_get_contents("http://opi.yahoo.com/online?u=$alamat&m=a&t=1");
		$gambar = "";
		$links = base_url().'addons/shared_addons/themes/iconic/img/';
		if($status == '00'){
			$gambar = file_get_contents($links.'zhan_ymoff.png');
		}elseif($status == '01'){
			$gambar = file_get_contents($links.'zhan_ymon.png');
		}
		echo $gambar;
    }

    /**
	 * List all pemesanan
     *
     * @return	void
     */
    public function complete()
    {
		$data['pageNow'] = 'purchase';

		$data['pesanan'] = $this->pemesanan_m->get_complete();

		unset($_SESSION['pesanan_id']);
		
        $this->template->title('Purchase Complete')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb('Purchase Complete')
			->build('complete', $data);
    }

    public function confirm(){

    	$data = '';
    	if($_POST){
    		$data = $_POST;
			$this->session->set_flashdata('success', 'Konfirmasi pemesanan telah dikirimkan.');
    		Events::trigger('email', $data, 'value');
    	}
		
        $this->template->title('Purchase Confirmation')
			->set_breadcrumb('Home', '/')
			->set_breadcrumb('Purchase Confirmation')
			->build('sendmail', $data);
    }
	
	/**
     * Insert or update pemesanan entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_pemesanan($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		$values = $this->input->post();

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set validation rules
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|min_length[3]');
		$this->form_validation->set_rules('email', 'E-Mail', 'required|valid_email');
		$this->form_validation->set_rules('alamat', 'Alamat Lengkap', 'required|min_length[20]');
		$this->form_validation->set_rules('telepon', 'Telepon', 'required|min_length[7]|numeric');
		$this->form_validation->set_rules('kode_pos', 'Kode Pos', 'required|min_length[4]|numeric');
        $this->form_validation->set_rules('recaptcha_response_field', 'ReCaptcha', 'required|recaptcha_matches');

		// Set Error Delimns
		$this->form_validation->set_error_delimiters('','<br/>');
		
		$result = false;

		if ($this->form_validation->run() === true)
		{

	        $ins1['nama'] = $this->input->post('nama');
	        $ins1['email'] = $this->input->post('email');
	        $ins1['alamat'] = $this->input->post('alamat');
	        $ins1['telepon'] = $this->input->post('telepon');
	        $ins1['kode_pos'] = $this->input->post('kode_pos');

	        $ins2 = array();
	        $kode 	=	$this->input->post('kode');
	        $ukuran =	$this->input->post('ukuran');
	        $jumlah =	$this->input->post('jumlah');
	        $harga =	$this->input->post('harga');
	        $y = count($kode);
			
			/*echo "<pre>";
			print_r($ins1);
			echo "<br/>";
			print_r($ins2);
			echo "</pre>";*/

			if ($method == 'new')
			{
                unset($values['recaptcha_challenge_field']);
                unset($values['recaptcha_response_field']);
				$result = $this->pemesanan_m->insert_pemesanan($ins1);

				$_SESSION['pesanan_id'] = $result;

				for($x=0;$x<$y;$x++){
					$ins2[$x]['id_pemesanan'] = $result;
					$ins2[$x]['kode_barang'] = $kode[$x];
					$ins2[$x]['ukuran'] = $ukuran[$x];
					$ins2[$x]['jumlah'] = $jumlah[$x];
					$ins2[$x]['harga'] = $harga[$x];
					$ins2[$x]['created'] = date("Y-m-d H:i:s");
				}
				$result = $this->pemesanan_m->insert_barang($ins2);

				unset($_POST);
			}
		}
		
		return $result;
	}

	// --------------------------------------------------------------------------

}