<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Purchase extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Purchase',
			'id' => 'Purchase',
		);
		$info['description'] = array(
			'en' => 'Modul untuk pemesanan',
			'id' => 'Modul untuk pemesanan',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'Purchase';
		$info['roles'] = array(
			'access_pemesanan_backend', 'view_all_pemesanan', 'view_own_pemesanan', 'edit_all_pemesanan', 'edit_own_pemesanan', 'delete_all_pemesanan', 'delete_own_pemesanan', 'create_pemesanan',
			'access_barang_backend', 'view_all_barang', 'view_own_barang', 'edit_all_barang', 'edit_own_barang', 'delete_all_barang', 'delete_own_barang', 'create_barang',
		);
		
		if(group_has_role('purchase', 'access_pemesanan_backend')){
			$info['sections']['pemesanan']['name'] = 'purchase:pemesanan:plural';
			$info['sections']['pemesanan']['uri'] = 'admin/purchase/index';
			
			if(group_has_role('purchase', 'create_pemesanan')){
				$info['sections']['pemesanan']['shortcuts']['create'] = array(
					'name' => 'purchase:pemesanan:new',
					'uri' => 'admin/purchase/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items, &$menu_order){
		unset($menu_items['lang:cp:nav_Purchase']);
		$menu_items['Pemesanan'] = array(
			'Semua' => 'admin/purchase/index/',
			'Baru' => 'admin/purchase/index/?f-status=0',
			'Dikonfirmasi' => 'admin/purchase/index/?f-status=1',
			'Dikirim' => 'admin/purchase/index/?f-status=2',
			'Diterima' => 'admin/purchase/index/?f-status=3',
			'Ditolak' => 'admin/purchase/index/?f-status=4',
		);
		$menu_order[1] = 'Pemesanan';
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        $this->load->dbforge();

		// pemesanan
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'nama' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'email' => array(
				'type' => 'varchar',
				'constraint' => '50',
				'null' => FALSE,
			),
			'alamat' => array(
				'type' => 'text',
				'null' => FALSE,
			),
			'kode_pos' => array(
				'type' => 'varchar',
				'constraint' => '8',
				'null' => FALSE,
			),
			'telepon' => array(
				'type' => 'varchar',
				'constraint' => '20',
				'null' => FALSE,
			),
			'status' => array(
				'type' => 'varchar',
				'constraint' => '10',
				'default' => '0',
				'null' => FALSE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('pemesanan', TRUE);


		// barang
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'id_pemesanan' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => FALSE,
			),
			'kode_barang' => array(
				'type' => 'varchar',
				'constraint' => '10',
				'null' => FALSE,
			),
			'ukuran' => array(
				'type' => 'varchar',
				'constraint' => '2',
				'default' => 'S',
				'null' => FALSE,
			),
			'harga' => array(
				'type' => 'INT',
				'constraint' => '11',
				'default' => '0',
				'null' => FALSE,
			),
			'jumlah' => array(
				'type' => 'INT',
				'constraint' => '3',
				'default' => '1',
				'null' => FALSE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('barang', TRUE);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
		$this->load->dbforge();
        $this->dbforge->drop_table('pemesanan');
        $this->dbforge->drop_table('barang');
        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}