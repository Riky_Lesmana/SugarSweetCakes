<?php
function print_html($pemesanan, $status){


?>
<html>
<head>
	<title>Order #<?php echo format_date($pemesanan[0]['created'], 'Ym'); ?><?php echo str_pad($pemesanan[0]['id'], 3, "0", STR_PAD_LEFT); ?> oleh <?php echo $pemesanan[0]['nama']; ?></title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/ace-fonts.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/ace.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/chosen.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/ace-rtl.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/ace-skins.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/customscroll.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>system/cms/themes/ace/css/jquery-ui-1.10.3.full.min.css" />
</head>
<body>
<div style="width:900px;">

	<div class="entry-detail-row">
		<div class="entry-detail-name">No. Order</div>
		<div class="entry-detail-value"><?php echo format_date($pemesanan[0]['created'], 'Ym'); ?><?php echo str_pad($pemesanan[0]['id'], 3, "0", STR_PAD_LEFT); ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:nama'); ?></div>
		<?php if(isset($pemesanan[0]['nama'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">E-mail</div>
		<?php if(isset($pemesanan[0]['email'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['email']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:alamat'); ?></div>
		<?php if(isset($pemesanan[0]['alamat'])){ ?>
		<div class="entry-detail-value" style="pre-warp"><?php echo $pemesanan[0]['alamat']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:kode_pos'); ?></div>
		<?php if(isset($pemesanan[0]['kode_pos'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['kode_pos']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:telepon'); ?></div>
		<?php if(isset($pemesanan[0]['telepon'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['telepon']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:status'); ?></div>
		<?php if(isset($pemesanan[0]['status'])){ ?>
		<div class="entry-detail-value"><?php echo $status[$pemesanan[0]['status']]; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">Tanggal Pemesanan</div>
		<?php if(isset($pemesanan[0]['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($pemesanan[0]['created'], 'd M Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">Pemesanan</div>
		<div class="entry-detail-value">
                <table class="table table-hover tableAdd">
                    <thead>
                        <tr>
                            <th>Barang</th>
                            <th>Ukuran</th>
                            <th>Jumlah</th>
                            <th class="text-center">Harga (Rp)</th>
                            <th class="text-center">Total (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                        $tots = 0;
                        foreach ($pemesanan as $data) {
                      ?>
                        <tr>
                            <td class="col-md-4 text-left">
                              <img src="<?php echo base_url()."files/thumb/".$data['gambar']."/100"; ?>"><br/>
                              <?php echo $data['kode_barang']; ?><br/>
                              <?php echo $data['nama_barang']; ?>
                            </td>
                            <td class="col-md-2" style="text-align: left">
                              <?php echo $data['ukuran']; ?>

                            </td>
                            <td class="col-md-2" style="text-align: left">
                              <?php echo $data['jumlah']; ?>

                            </td>
                            <td class="col-md-2 text-center harga">
                              Rp. <?php echo number_format($data['harga'],0,',','.'); ?>

                            </td>
                            <td class="col-md-2 text-center total">
                              Rp. <?php echo number_format($data['total'],0,',','.'); $tots += $data['total']; ?>

                            </td>
                        </tr>
                      <?php
                        }
                      ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>Total Keseluruhan</td>
                            <td class="text-center"><strong class="totalAll">Rp. <?php echo number_format($tots,0,',','.'); ?></strong></td>
                        </tr>
                    </tbody>
                </table>
		</div>
	</div>
</div>
</body>
</html>
<script type="text/javascript">
print();
window.close();
</script>
<?php
}
?>
