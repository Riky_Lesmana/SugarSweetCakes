<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Barang model
 *
 * @author Aditya Satrya
 */
class Barang_m extends MY_Model {
	
	public function get_barang($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_barang');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_barang_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_barang');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_barang()
	{
		return $this->db->count_all('default_barang');
	}
	
	public function delete_barang_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_barang');
	}
	
	public function insert_barang($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_barang', $values);
	}
	
	public function update_barang($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_barang', $values); 
	}
	
}