<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Pemesanan model
 *
 * @author Aditya Satrya
 */
class Pemesanan_m extends MY_Model {
	
	public function get_pemesanan($pagination_config = NULL)
	{
		$this->db->select('default_pemesanan.*');
		$this->db->select('sum(default_barang.harga*default_barang.jumlah) total');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);

		if($this->input->get('f-nama')){
			$this->db->like('default_pemesanan.nama', $this->input->get('f-nama'));
		}

		if($this->input->get('f-id')){
			$ids = $this->input->get('f-id');
			$yearmonth = substr($ids, 0,6);
			$idnya = substr($ids, 6,strlen($ids));
			//echo "$yearmonth : $idnya";

			$this->db->where("date_format(default_pemesanan.created, '%Y%m') = '$yearmonth'", null, false);
			$this->db->where('default_pemesanan.id', $idnya);
		}

		if($this->input->get('f-status')>-1){
			$this->db->like('default_pemesanan.status', $this->input->get('f-status'));
		}

		$this->db->join('default_barang', 'default_pemesanan.id = default_barang.id_pemesanan', 'left');
		
		$this->db->order_by('created desc');
		$this->db->group_by('default_pemesanan.id');
		$query = $this->db->get('default_pemesanan');
		$result = $query->result_array();

		//echo $this->db->last_query();
		
        return $result;
	}
	
	public function get_pemesanan_by_id($id)
	{
		$this->db->select('default_pemesanan.id id, default_pemesanan.created created, default_pemesanan.nama nama, email, alamat, kode_pos, telepon, status');
		$this->db->select('kode_barang, default_collection.nama nama_barang, default_barang.harga, jumlah, (default_barang.harga*jumlah) total, kecil gambar');
		$this->db->select('case(ukuran)
			when 1 then "S"
			when 2 then "M"
			when 3 then "L"
			end ukuran
			', false);
		$this->db->where('default_pemesanan.id', $id);
		$this->db->join('default_barang', 'default_pemesanan.id = default_barang.id_pemesanan','left');
		$this->db->join('default_collection', 'default_barang.kode_barang = default_collection.kode','left');
		$query = $this->db->get('default_pemesanan');
		$result = $query->result_array();
		
		return $result;
	}
	
	public function count_all_pemesanan()
	{

		if($this->input->get('f-id')){
			$this->db->like('default_pemesanan.id', $this->input->get('f-id')-1);
		}

		if($this->input->get('f-nama')){
			$this->db->like('default_pemesanan.nama', $this->input->get('f-nama'));
		}

		if($this->input->get('f-status')){
			$this->db->like('default_pemesanan.status', $this->input->get('f-status'));
		}
		return $this->db->count_all_results('default_pemesanan');
	}
	
	public function delete_pemesanan_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_pemesanan');
	}
	
	public function insert_pemesanan($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$this->db->insert('default_pemesanan', $values);

		return $this->db->insert_id();
	}
	
	public function insert_barang($values)
	{
		return $this->db->insert_batch('default_barang', $values);
	}
	
	public function update_pemesanan($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_pemesanan', $values); 
	}

	public function get_complete(){
		$this->db->select('default_pemesanan.id id, default_pemesanan.nama nama, email, alamat, kode_pos, telepon');
		$this->db->select('kode_barang, default_collection.nama nama_barang, default_barang.harga, jumlah, (default_barang.harga*jumlah) total, kecil gambar');
		$this->db->select('case(ukuran)
			when 1 then "S"
			when 2 then "M"
			when 3 then "L"
			end ukuran
			', false);
		$this->db->where('default_pemesanan.id', $_SESSION['pesanan_id']);
		$this->db->join('default_barang', 'default_pemesanan.id = default_barang.id_pemesanan','left');
		$this->db->join('default_collection', 'default_barang.kode_barang = default_collection.kode','left');
		$query = $this->db->get('default_pemesanan');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_items(){
		$this->db->where('show', 1);
		$this->db->where('jual', 1);
		$query = $this->db->get('default_collection');
		$result = $query->result_array();
		
        return $result;
	}

	public function get_items_by_kode($kode){
		$this->db->like('kode', $kode);
		$query = $this->db->get('default_collection');
		$result = $query->row_array();
		
        return $result;
	}
	
}