<div class="page-header">
	<h1><?php echo lang('purchase:barang:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/purchase/barang/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('purchase:back') ?>
		</a>

		<?php if(group_has_role('purchase', 'edit_all_barang')){ ?>
			<a href="<?php echo site_url('admin/purchase/barang/edit/'.$barang['id']); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('purchase', 'edit_own_barang')){ ?>
			<?php if($barang->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/purchase/barang/edit/'.$barang->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('purchase', 'delete_all_barang')){ ?>
			<a href="<?php echo site_url('admin/purchase/barang/delete/'.$barang['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('purchase', 'delete_own_barang')){ ?>
			<?php if($barang->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/purchase/barang/delete/'.$barang['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $barang['id']; ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:id_pemesanan'); ?></div>
		<?php if(isset($barang['id_pemesanan'])){ ?>
		<div class="entry-detail-value"><?php echo $barang['id_pemesanan']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:kode_barang'); ?></div>
		<?php if(isset($barang['kode_barang'])){ ?>
		<div class="entry-detail-value"><?php echo $barang['kode_barang']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:ukuran'); ?></div>
		<?php if(isset($barang['ukuran'])){ ?>
		<div class="entry-detail-value"><?php echo $barang['ukuran']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:harga'); ?></div>
		<?php if(isset($barang['harga'])){ ?>
		<div class="entry-detail-value"><?php echo $barang['harga']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:jumlah'); ?></div>
		<?php if(isset($barang['jumlah'])){ ?>
		<div class="entry-detail-value"><?php echo $barang['jumlah']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:created'); ?></div>
		<?php if(isset($barang['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($barang['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:updated'); ?></div>
		<?php if(isset($barang['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($barang['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($barang['created_by'], true); ?></div>
	</div>
</div>