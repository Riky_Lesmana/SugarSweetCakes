<div class="page-header">
	<h1><?php echo lang('purchase:barang:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="id_pemesanan"><?php echo lang('purchase:id_pemesanan'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('id_pemesanan') != NULL){
					$value = $this->input->post('id_pemesanan');
				}elseif($mode == 'edit'){
					$value = $fields['id_pemesanan'];
				}
			?>
			<input name="id_pemesanan" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="kode_barang"><?php echo lang('purchase:kode_barang'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('kode_barang') != NULL){
					$value = $this->input->post('kode_barang');
				}elseif($mode == 'edit'){
					$value = $fields['kode_barang'];
				}
			?>
			<input name="kode_barang" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="ukuran"><?php echo lang('purchase:ukuran'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('ukuran') != NULL){
					$value = $this->input->post('ukuran');
				}elseif($mode == 'edit'){
					$value = $fields['ukuran'];
				}
			?>
			<input name="ukuran" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="harga"><?php echo lang('purchase:harga'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('harga') != NULL){
					$value = $this->input->post('harga');
				}elseif($mode == 'edit'){
					$value = $fields['harga'];
				}
			?>
			<input name="harga" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="jumlah"><?php echo lang('purchase:jumlah'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('jumlah') != NULL){
					$value = $this->input->post('jumlah');
				}elseif($mode == 'edit'){
					$value = $fields['jumlah'];
				}
			?>
			<input name="jumlah" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>