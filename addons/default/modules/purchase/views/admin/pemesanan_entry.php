<div class="page-header">
	<h1><?php echo lang('purchase:pemesanan:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/purchase/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('purchase:back') ?>
		</a>
		
		<a href="<?php echo site_url('admin/purchase/download/'.$pemesanan[0]['id']); ?>" target="_blank" class="btn btn-sm btn-info">
			<i class="icon-print"></i>
			Print
		</a>
		
	</div>
</div>

<div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">No. Order</div>
		<div class="entry-detail-value"><?php echo format_date($pemesanan[0]['created'], 'Ym'); ?><?php echo str_pad($pemesanan[0]['id'], 3, "0", STR_PAD_LEFT); ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:nama'); ?></div>
		<?php if(isset($pemesanan[0]['nama'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['nama']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">E-Mail</div>
		<?php if(isset($pemesanan[0]['email'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['email']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:alamat'); ?></div>
		<?php if(isset($pemesanan[0]['alamat'])){ ?>
		<div class="entry-detail-value" style="pre-warp"><?php echo $pemesanan[0]['alamat']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:kode_pos'); ?></div>
		<?php if(isset($pemesanan[0]['kode_pos'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['kode_pos']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:telepon'); ?></div>
		<?php if(isset($pemesanan[0]['telepon'])){ ?>
		<div class="entry-detail-value"><?php echo $pemesanan[0]['telepon']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('purchase:status'); ?></div>
		<?php if(isset($pemesanan[0]['status'])){ ?>
		<div class="entry-detail-value"><?php echo $status[$pemesanan[0]['status']]; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">Tanggal Pemesanan</div>
		<?php if(isset($pemesanan[0]['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($pemesanan[0]['created'], 'd M Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name">Pemesanan</div>
		<div class="entry-detail-value">
                <table class="table table-hover tableAdd">
                    <thead>
                        <tr>
                            <th>Barang</th>
                            <th>Ukuran</th>
                            <th>Jumlah</th>
                            <th class="text-center">Harga (Rp)</th>
                            <th class="text-center">Total (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                        $tots = 0;
                        foreach ($pemesanan as $data) {
                      ?>
                        <tr>
                            <td class="col-md-4 text-left">
                              <img src="<?php echo base_url()."files/thumb/".$data['gambar']."/100"; ?>"><br/>
                              <?php echo $data['kode_barang']; ?><br/>
                              <?php echo $data['nama_barang']; ?>
                            </td>
                            <td class="col-md-2" style="text-align: left">
                              <?php echo $data['ukuran']; ?>

                            </td>
                            <td class="col-md-2" style="text-align: left">
                              <?php echo $data['jumlah']; ?>

                            </td>
                            <td class="col-md-2 text-center harga">
                              Rp. <?php echo number_format($data['harga'],0,',','.'); ?>

                            </td>
                            <td class="col-md-2 text-center total">
                              Rp. <?php echo number_format($data['total'],0,',','.'); $tots += $data['total']; ?>

                            </td>
                        </tr>
                      <?php
                        }
                      ?>
                        <tr>
                            <td>   </td>
                            <td>   </td>
                            <td>   </td>
                            <td>Total Keseluruhan</td>
                            <td class="text-center"><strong class="totalAll">Rp. <?php echo number_format($tots,0,',','.'); ?></strong></td>
                        </tr>
                    </tbody>
                </table>
		</div>
	</div>
</div>