<div class="page-header">
	<h1><?php echo lang('purchase:pemesanan:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="nama"><?php echo lang('purchase:nama'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('nama') != NULL){
					$value = $this->input->post('nama');
				}elseif($mode == 'edit'){
					$value = $fields['nama'];
				}
			?>
			<input name="nama" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="alamat"><?php echo lang('purchase:alamat'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('alamat') != NULL){
					$value = $this->input->post('alamat');
				}elseif($mode == 'edit'){
					$value = $fields['alamat'];
				}
			?>
			<textarea name="alamat" class="col-xs-10 col-sm-5" id=""><?php echo $value; ?></textarea>
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="kode_pos"><?php echo lang('purchase:kode_pos'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('kode_pos') != NULL){
					$value = $this->input->post('kode_pos');
				}elseif($mode == 'edit'){
					$value = $fields['kode_pos'];
				}
			?>
			<input name="kode_pos" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="telepon"><?php echo lang('purchase:telepon'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('telepon') != NULL){
					$value = $this->input->post('telepon');
				}elseif($mode == 'edit'){
					$value = $fields['telepon'];
				}
			?>
			<input name="telepon" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="status"><?php echo lang('purchase:status'); ?></label>

		<div class="col-xs-10">
			<?php 
				$value = NULL;
				if($this->input->post('status') != NULL){
					$value = $this->input->post('status');
				}elseif($mode == 'edit'){
					$value = $fields['status'];
				}
			?>
			<input name="status" type="text" value="<?php echo $value; ?>" class="col-xs-10 col-sm-5" id="" />
			
			<span class="help-inline col-xs-12 col-sm-7">
				<span class="middle">Inline help text</span>
			</span>
		</div>
	</div>

</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>