      <div class="baseHome">
        <h3>Purchase Confirmation</h3>

        <div class="row">
            <div class="col-xs-12">
                <?php
                    if($this->session->flashdata('success')){
                        $msg = $this->session->flashdata('success');
                        echo '
                        <div class="cols-xs-12"><div class="alert alert-success text-center" role="alert">
                        '.$msg[0].'
                        </div></div>';
                    }
                ?>
            {{ contact:form
                name = "text|required"
                email = "text|required|valid_email"
                order_id = "text|required"
                message = "textarea|required"
                attachment = "file|jpg|png|zip"
                max_size = "10000"
                button = "send"
                template = "confirm"
                lang = "en"
                to = "iconicmoviestars@gmail.com"
                from = "iconicmoviestars@gmail.com"
                sent = "Your confirmation has been sent. Thank you for contacting us"
                error = "Sorry. Your message could not be sent. Please call us at 0812-1447-4818"
                success-redirect = "purchase/confirm"
            }}
                {{ name }}
                {{ email }}
                {{ order_id }}
                {{ attachment }}
                {{ message }}
            {{ /contact:form }}

            </div>
        </div>

      <br/>
      </div>