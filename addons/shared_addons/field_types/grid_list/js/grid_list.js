(function($){
  $.fn.extend( {
    grid_list_field: function( options ) {
      this.defaults = {};
      var settings = $.extend( {}, this.defaults, options );
      function makeTemplate(element) {
		var templateElem = element.clone();
		templateElem.find('input').each(function(){
			//$(this).attr('value') = '';
		});
		return '<tr>' + templateElem.html() + '</tr>';
      }
      function removeItem() {
        var $tr = $(this).parent().parent();
		var $tbody = $tr.parent();
		if($tbody.children('tr').length > 2){
			$tr.remove();
		}else{
			alert('This field must have one row minimum.');
		}
      }
      function addItem(callback) {
        var $parent = $(this).parent().parent();
		var str = makeTemplate($parent);
        $parent.after(str);
		$parent.next().find('input').each(function(){
			$(this).val('');
		});
      }
      function textareaEnter($parent, callback) {
        var str = makeTemplate($(this));
        $parent.append(str);
        if (callback && typeof(callback) === "function") {
          callback(newname);
        }
      }
      function textareaHandle(e) {
        var $parent = $(this).parent().parent();
        var code = (e.keyCode ? e.keyCode : e.which);
        if(code === 13) {
          return false;
        }
      }
      return this.each( function() {
        var $this = $(this);
        $this.on('click', '.add', addItem);
        $this.on('click', '.remove', removeItem);
        $this.on('keypress', 'input', textareaHandle);
      });
    }
  });
  $(function() {
    $('.grid_list_field').grid_list_field();
  });
})( jQuery );