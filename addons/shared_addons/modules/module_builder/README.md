# PyroCMS "Module Builder" Module

This module creates codebase for a new PyroCMS module. It creates basic files to run CRUD functionlity.

## Installation

* Install Pyro Grid List Field first. See https://bitbucket.org/asatrya/pyro-grid-list-field/overview.

* Unzip the folder, rename it to 'module_builder', and drop it into your addons/_site\_ref_/modules or addons/shared\_addons/modules folder. Install from the admin panel.