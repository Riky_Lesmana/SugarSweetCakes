<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Module Builder 2 Module
 *
 * New Module Builder
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/module_builder/generator/index');
    }

}