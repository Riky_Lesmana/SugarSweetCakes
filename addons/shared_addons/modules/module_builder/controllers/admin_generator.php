<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Module Builder 2 Module
 *
 * New Module Builder
 *
 */
class Admin_generator extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'generator';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('module_builder', 'access_generator_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('module_builder');
        $this->load->driver('Streams');
    }

    /**
	 * List all Generator using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the generator database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('module_builder', 'view_all_generator') AND ! group_has_role('module_builder', 'view_own_generator')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'generator';
		$params['namespace'] = 'module_builder';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		//$params['order_by'] = 'created';
		//$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		//$params['where'] = ''; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 4;
		$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		if(! group_has_role('module_builder', 'view_all_generator')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// Get stream entries
        $data['generator'] = $this->streams->entries->get_entries($params);

		// Get multiple relationship
		/*foreach($data['generator']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', 'module_builder');
			$attributes = array(
				'stream_slug' => 'generator', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['generator']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('module_builder:generator:plural'));
		$this->template->build('admin/generator_index', $data);
    }
	
	/**
     * Display one Generator
     *
     * We are using the Streams API to grab
     * data from the generator database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('module_builder', 'view_all_generator') AND ! group_has_role('module_builder', 'view_own_generator')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['generator'] = $this->streams->entries->get_entry($id, 'generator', 'module_builder', true);
		
		// Check view all/own permission
		if(! group_has_role('module_builder', 'view_all_generator')){
			if($data['generator']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', 'module_builder');
		$attributes = array(
			'stream_slug' => 'generator', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['generator']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('module_builder:generator:view'));
        $this->template->build('admin/generator_entry', $data);
    }
	
	/**
     * Create a new Generator entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('module_builder', 'create_generator')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
        $stream = $this->streams->streams->get_stream('generator', 'module_builder');
		$namespace = 'module_builder';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/module_builder/generator/index',
            'success_message' => lang('module_builder:generator:submit_success'),
            'failure_message' => lang('module_builder:generator:submit_failure'),
            'title' => 'lang:module_builder:generator:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('module_builder:generator:new'));
        $this->template->build('admin/generator_form', $data);
    }
	
	/**
     * Edit a Generator entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Generator to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('module_builder', 'edit_all_generator') AND ! group_has_role('module_builder', 'edit_own_generator')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('module_builder', 'edit_all_generator')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'generator', 'module_builder', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
		$stream = $this->streams->streams->get_stream('generator', 'module_builder');
		$namespace = 'module_builder';
		$mode = 'edit'; //'new', 'edit'
		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/module_builder/generator/index',
            'success_message' => lang('module_builder:generator:submit_success'),
            'failure_message' => lang('module_builder:generator:submit_failure'),
            'title' => 'lang:module_builder:generator:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('module_builder:generator:edit'));
        $this->template->build('admin/generator_form', $data);
    }
	
	/**
     * Delete a Generator entry
     * 
     * @param   int [$id] The id of Generator to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('module_builder', 'delete_all_generator') AND ! group_has_role('module_builder', 'delete_own_generator')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('module_builder', 'delete_all_generator')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'generator', 'module_builder', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
        $this->streams->entries->delete_entry($id, 'generator', 'module_builder');
        $this->session->set_flashdata('error', lang('module_builder:generator:deleted'));
 
        redirect('admin/module_builder/generator/index');
    }

	/**
     * Create a new Module
     *
     * @return	void
     */
    public function build($id)
    {
		$this->load->helper('url');

		$module = $this->streams->entries->get_entry($id, 'generator', 'module_builder', true);
		
		$input['module_name'] = $module->module_name;
		$input['module_slug'] = $module->module_slug;
		$input['module_class_name'] = ucfirst($module->module_slug);
		$input['module_description'] = $module->module_description;
		$input['module_has_frontend'] = $module->module_has_frontend['key'];
		$input['module_has_backend'] = $module->module_has_backend['key'];
		$input['module_menu'] = $module->module_menu;
		foreach($module->module_streams as $stream){
			$input['module_streams'][] = $stream;
		}
		$input['module_fields'] = $module->module_fields;

		// Path to template files
		$file_templates_dir = FCPATH . $this->module_details['path'] . '/file_templates/';

		// Make module directories
		$module_dir = FCPATH . ADDONPATH . 'modules/' .$input['module_slug'];
		if(!file_exists($module_dir) OR !is_dir($module_dir)){
			mkdir($module_dir);
		}
		$config_dir = $module_dir . '/config';
		if(!file_exists($config_dir) OR !is_dir($config_dir)){
			mkdir($config_dir);
		}
		$controllers_dir = $module_dir . '/controllers';
		if(!file_exists($controllers_dir) OR !is_dir($controllers_dir)){
			mkdir($controllers_dir);
		}
		$models_dir = $module_dir . '/models';
		if(!file_exists($models_dir) OR !is_dir($models_dir)){
			mkdir($models_dir);
		}
		$css_dir = $module_dir . '/css';
		if(!file_exists($css_dir) OR !is_dir($css_dir)){
			mkdir($css_dir);
		}
		$language_dir = $module_dir . '/language';
		if(!file_exists($language_dir) OR !is_dir($language_dir)){
			mkdir($language_dir);
		}
		$views_dir = $module_dir . '/views';
		if(!file_exists($views_dir) OR !is_dir($views_dir)){
			mkdir($views_dir);
		}

		// Generate details.php
		$this->_generate_details($module_dir, $file_templates_dir, $input);

		// Generate routes.php
		$this->_generate_routes($config_dir, $file_templates_dir, $input);

		// Generate css file
		$this->_generate_css($css_dir, $file_templates_dir, $input);

		// Generate lang file
		$this->_generate_languages($language_dir, $file_templates_dir, $input, 'english');
		$this->_generate_languages($language_dir, $file_templates_dir, $input, 'indonesian');

		// Generate controllers
		$this->_generate_controllers($controllers_dir, $file_templates_dir, $input);
		
		// Generate models
		$this->_generate_models($models_dir, $file_templates_dir, $input);

		// Generate views
		$this->_generate_admin_views($views_dir, $file_templates_dir, $input);
		$this->_generate_public_views($views_dir, $file_templates_dir, $input);

		$this->session->set_flashdata('success', lang('module_builder:module_created_success_message'));
		redirect('admin/module_builder/generator/view/'.$id);
    }

	function _generate_details($module_dir, $file_templates_dir, $input)
	{
		// Load everything we need
		$this->load->library('streams_core/Fields');

		// Part 01
		$str_template = file_get_contents($file_templates_dir . 'details.part01.php');
		$str_result = $str_template;
		foreach($input as $key => $value){
			if(! is_array($value)){
				$str_result = str_replace('%'.$key.'%', $value, $str_result);
			}
		}
		$fp = fopen($module_dir . '/details.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 02
		$str_template = file_get_contents($file_templates_dir . 'details.part02.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$str_result .= str_replace('%section_slug%', $value_slug, $str_template);
		}
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 03
		$str_template = file_get_contents($file_templates_dir . 'details.part03.php');
		$str_result = $str_template;
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 04
		$str_template = file_get_contents($file_templates_dir . 'details.part04.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%');
			$replacement_arr = array($input['module_slug'], $value_slug);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 05
		$str_template = file_get_contents($file_templates_dir . 'details.part05.php');
		$str_result = '';
		$fp = fopen($module_dir . '/details.php', 'a');
		$str_result = str_replace('%module_slug%', $input['module_slug'], $str_template);
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 06
		$str_template = file_get_contents($file_templates_dir . 'details.part06.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%');
			$replacement_arr = array($input['module_slug'], $value_slug);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 07
		$str_template = file_get_contents($file_templates_dir . 'details.part07.php');
		$str_result = $str_template;
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 08 (ALL sub-part)
		$str_result = '';
		foreach($input['module_fields'] as $value){
			$field_slug = url_title($value['name'], '_', TRUE);
			$needle_arr = array(
				'%field_name%',
				'%field_slug%',
				'%field_type%',
				'%field_assign%',
				'%field_title_column%',
				'%field_required%',
				'%field_unique%',
				'%module_slug%',
			);
			$replacement_arr = array(
				$value['name'],
				$field_slug,
				$value['type'],
				$value['assign'],
				$value['title_column'],
				$value['required'],
				$value['unique'],
				$input['module_slug'],
			);

			// Part 0
			$str_template = file_get_contents($file_templates_dir . 'details.part08.php');
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			// Part 01
			$field_type = $this->type->load_single_type($value['type']);
			if(isset($field_type->custom_parameters) AND is_array($field_type->custom_parameters)){
				foreach($field_type->custom_parameters as $value){
					$str_template = file_get_contents($file_templates_dir . 'details.part08_1.php');
					$str_result .= str_replace('%field_custom_parameters%', $value, $str_template);
				}
			}

			// Part 02
			$str_template = file_get_contents($file_templates_dir . 'details.part08_2.php');
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($module_dir . '/details.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 09
		$str_template = file_get_contents($file_templates_dir . 'details.part09.php');
		$str_result = '';
		$fp = fopen($module_dir . '/details.php', 'a');
		$str_result = str_replace('%module_slug%', $input['module_slug'], $str_template);
		fwrite($fp, $str_result);
		fclose($fp);
	}

	function _generate_routes($config_dir, $file_templates_dir, $input)
	{
		// Part 01
		$str_template = file_get_contents($file_templates_dir . 'routes.part01.php');
		$str_result = $str_template;
		$fp = fopen($config_dir . '/routes.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 02
		$str_template = file_get_contents($file_templates_dir . 'routes.part02.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%');
			$replacement_arr = array($input['module_slug'], $value_slug);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($config_dir . '/routes.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 03
		$str_template = file_get_contents($file_templates_dir . 'routes.part03.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%');
			$replacement_arr = array($input['module_slug'], $value_slug);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($config_dir . '/routes.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);
	}

	function _generate_css($css_dir, $file_templates_dir, $input)
	{
		$fp = fopen($css_dir . '/'.$input['module_slug'].'.css', 'w');
		fwrite($fp, '');
		fclose($fp);
	}

	function _generate_languages($language_dir, $file_templates_dir, $input, $language = 'english')
	{
		// Create directory for specific language if doesn't exist yet
		if(!file_exists($language_dir . '/' . $language) OR !is_dir($language_dir . '/' . $language)){
			mkdir($language_dir . '/' . $language);
		}

		// Module Lang
		// Part 01
		$str_template = file_get_contents($file_templates_dir . 'lang.'.$language.'.part01.php');
		$str_result = $str_template;
		$needle_arr = array('%module_menu%', '%module_slug%');
		$replacement_arr = array($input['module_menu'], $input['module_slug']);
		$str_result = str_replace($needle_arr, $replacement_arr, $str_template);
		$fp = fopen($language_dir . '/' . $language . '/' . $input['module_slug'] . '_lang.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 02
		$str_template = file_get_contents($file_templates_dir . 'lang.'.$language.'.part02.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%', '%section_name%');
			$replacement_arr = array($input['module_slug'], $value_slug, $value);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($language_dir . '/' . $language . '/' . $input['module_slug'] . '_lang.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 03
		$str_template = file_get_contents($file_templates_dir . 'lang.'.$language.'.part03.php');
		$str_result = $str_template;
		$fp = fopen($language_dir . '/' . $language . '/' . $input['module_slug'] . '_lang.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 03_1
		$str_template = file_get_contents($file_templates_dir . 'lang.english.part03_1.php');
		$str_result = '';
		foreach($input['module_fields'] as $value){
			$field_slug = url_title($value['name'], '_', TRUE);
			$needle_arr = array(
				'%field_name%',
				'%field_slug%',
				'%field_type%',
				'%field_assign%',
				'%field_title_column%',
				'%field_required%',
				'%field_unique%',
				'%module_slug%',
			);
			$replacement_arr = array(
				$value['name'],
				$field_slug,
				$value['type'],
				$value['assign'],
				$value['title_column'],
				$value['required'],
				$value['unique'],
				$input['module_slug'],
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($language_dir . '/' . $language . '/' . $input['module_slug'] . '_lang.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 04
		$str_template = file_get_contents($file_templates_dir . 'lang.'.$language.'.part04.php');
		$str_result = '';
		$str_result = str_replace('%module_slug%', $input['module_slug'], $str_template);
		$fp = fopen($language_dir . '/' . $language . '/' . $input['module_slug'] . '_lang.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);

		// Permission Lang
		// Part 01
		$str_template = file_get_contents($file_templates_dir . 'permission_lang.'.$language.'.part01.php');
		$str_result = $str_template;
		$fp = fopen($language_dir . '/' . $language . '/permission_lang.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// Part 02
		$str_template = file_get_contents($file_templates_dir . 'permission_lang.'.$language.'.part02.php');
		$str_result = '';
		foreach($input['module_streams'] as $value){
			$value_slug = url_title($value, '_', TRUE);
			$needle_arr = array('%module_slug%', '%section_slug%', '%section_name%');
			$replacement_arr = array($input['module_slug'], $value_slug, $value);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
		}
		$fp = fopen($language_dir . '/' . $language . '/permission_lang.php', 'a');
		fwrite($fp, $str_result);
		fclose($fp);
	}

	function _generate_controllers($controllers_dir, $file_templates_dir, $input)
	{
		// Admin.php
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'controllers.admin.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
			break;
		}
		$fp = fopen($controllers_dir . '/admin.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// admin_{section}.php
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'controllers.admin_section.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($controllers_dir . '/admin_'.$section_slug.'.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Public controller
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'controllers.public.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$module_class = ucfirst($input['module_slug']);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%',
				'%module_class%',
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name,
				$module_class,
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);
			break;
		}
		$fp = fopen($controllers_dir . '/'.$input['module_slug'].'.php', 'w');
		fwrite($fp, $str_result);
		fclose($fp);

		// admin_{section}.php
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'controllers.public_section.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$module_class = ucfirst($input['module_slug']);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%',
				'%module_class%',
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name,
				$module_class,
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($controllers_dir . '/'.$input['module_slug'].'_'.$section_slug.'.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}
	}
	
	function _generate_models($models_dir, $file_templates_dir, $input)
	{
		// admin_{section}.php
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'models.section.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_class%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				ucfirst($section_slug),
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($models_dir . '/'.$section_slug.'_m.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}
	}

	function _generate_admin_views($views_dir, $file_templates_dir, $input)
	{
		// Create directory for admin views
		if(!file_exists($views_dir . '/admin') OR !is_dir($views_dir . '/admin')){
			mkdir($views_dir . '/admin');
		}

		// index file
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part03.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 03_1
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part03_1.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
						'%field_name%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
						$fields['name'],
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 04
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part04.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 04_1
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part04_1.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
						'%field_name%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
						$fields['name'],
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 05
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.index.part05.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Form files
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.form.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_form.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.admin.form.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/admin/'.$section_slug.'_form.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.form.part03.php');
			$section_slug = url_title($section_name, '_', TRUE);
			$str_result = $str_template;
			$fp = fopen($views_dir . '/admin/'.$section_slug.'_form.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Entry file
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.entry.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_entry.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.admin.entry.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/admin/'.$section_slug.'_entry.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.admin.entry.part03.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/admin/'.$section_slug.'_entry.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}
	}

	function _generate_public_views($views_dir, $file_templates_dir, $input)
	{
		// index file
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.index.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.public.index.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.index.part03.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 03_1
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.public.index.part03_1.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
						'%field_name%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
						$fields['name'],
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 04
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.index.part04.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 04_1
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.public.index.part04_1.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
						'%field_name%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
						$fields['name'],
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 05
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.index.part05.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_index.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Form files
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.form.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_form.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.public.form.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/'.$section_slug.'_form.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.form.part03.php');
			$section_slug = url_title($section_name, '_', TRUE);
			$str_result = $str_template;
			$fp = fopen($views_dir . '/'.$section_slug.'_form.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Entry file
		// Part 01
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.entry.part01.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_entry.php', 'w');
			fwrite($fp, $str_result);
			fclose($fp);
		}

		// Part 02
		foreach($input['module_streams'] as $section_name){
			foreach($input['module_fields'] as $fields){
				$str_template = file_get_contents($file_templates_dir . 'views.public.entry.part02.php');
				$str_result = '';

				$section_slug = url_title($section_name, '_', TRUE);
				$field_slug = url_title($fields['name'], '_', TRUE);

				if($fields['assign'] == $section_slug){
					$needle_arr = array(
						'%module_name%',
						'%module_description%',
						'%module_slug%',
						'%section_slug%',
						'%section_name%',
						'%field_slug%',
					);
					$replacement_arr = array(
						$input['module_name'],
						$input['module_description'],
						$input['module_slug'],
						$section_slug,
						$section_name,
						$field_slug,
					);
					$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

					$fp = fopen($views_dir . '/'.$section_slug.'_entry.php', 'a');
					fwrite($fp, $str_result);
					fclose($fp);
				}
			}
		}

		// Part 03
		foreach($input['module_streams'] as $section_name){
			$str_template = file_get_contents($file_templates_dir . 'views.public.entry.part03.php');
			$str_result = '';

			$section_slug = url_title($section_name, '_', TRUE);
			$needle_arr = array(
				'%module_name%',
				'%module_description%',
				'%module_slug%',
				'%section_slug%',
				'%section_name%'
			);
			$replacement_arr = array(
				$input['module_name'],
				$input['module_description'],
				$input['module_slug'],
				$section_slug,
				$section_name
			);
			$str_result .= str_replace($needle_arr, $replacement_arr, $str_template);

			$fp = fopen($views_dir . '/'.$section_slug.'_entry.php', 'a');
			fwrite($fp, $str_result);
			fclose($fp);
		}
	}

}