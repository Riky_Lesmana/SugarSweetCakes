<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Module_builder extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Module Builder',
			'id' => 'Module Builder',
		);
		$info['description'] = array(
			'en' => 'Module to generate module codebase automatically.',
			'id' => 'Modul untuk menhasilkan codebase module secara otomatis.',
		);
		$info['frontend'] = false;
		$info['backend'] = true;
		$info['menu'] = 'addons';
		$info['roles'] = array(
			'access_generator_backend', 'view_all_generator', 'view_own_generator', 'edit_all_generator', 'edit_own_generator', 'delete_all_generator', 'delete_own_generator', 'create_generator',
		);
		
		if(group_has_role('module_builder', 'access_generator_backend')){
			$info['sections']['generator']['name'] = 'module_builder:generator:plural';
			$info['sections']['generator']['uri'] = 'admin/module_builder/generator/index';
			
			if(group_has_role('module_builder', 'create_generator')){
				$info['sections']['generator']['shortcuts']['create'] = array(
					'name' => 'module_builder:generator:new',
					'uri' => 'admin/module_builder/generator/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        // We're using the streams API to
        // do data setup.
        $this->load->driver('Streams');

        $this->load->language('module_builder/module_builder');

        // Add streams
        if ( ! $generator_stream_id = $this->streams->streams->add_stream('lang:module_builder:generator:plural', 'generator', 'module_builder', 'module_builder_', null)) return false;

        // Add some fields
        $fields = array(
            array(
                'name' => 'lang:module_builder:module_name',
                'slug' => 'module_name',
                'namespace' => 'module_builder',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'generator',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:module_builder:module_slug',
                'slug' => 'module_slug',
                'namespace' => 'module_builder',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '_',
					'slug_field' => 'module_name',
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:module_builder:module_description',
                'slug' => 'module_description',
                'namespace' => 'module_builder',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:module_builder:module_has_frontend',
                'slug' => 'module_has_frontend',
                'namespace' => 'module_builder',
                'type' => 'choice',
                'extra' => array(
					'choice_data' => "true : Yes\nfalse : No",
					'choice_type' => 'dropdown',
					//'default_value' => '',
					//'min_choices' => '',
					//'max_choices' => '',
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:module_builder:module_has_backend',
                'slug' => 'module_has_backend',
                'namespace' => 'module_builder',
                'type' => 'choice',
                'extra' => array(
					'choice_data' => "true : Yes\nfalse : No",
					'choice_type' => 'dropdown',
					//'default_value' => '',
					//'min_choices' => '',
					//'max_choices' => '',
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:module_builder:module_menu',
                'slug' => 'module_menu',
                'namespace' => 'module_builder',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:module_builder:module_streams',
                'slug' => 'module_streams',
                'namespace' => 'module_builder',
                'type' => 'list',
                'extra' => array(
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:module_builder:module_fields',
                'slug' => 'module_fields',
                'namespace' => 'module_builder',
                'type' => 'grid_list',
                'extra' => array(
					'columns' => array(
						array(
							'label' => 'Name',
							'slug' => 'name',
							'type' => 'text',
						),
						array(
							'label' => 'Type',
							'slug' => 'type',
							'type' => 'text',
						),
						array(
							'label' => 'Assign',
							'slug' => 'assign',
							'type' => 'text',
						),
						array(
							'label' => 'Title column?',
							'slug' => 'title_column',
							'type' => 'text',
						),
						array(
							'label' => 'Required?',
							'slug' => 'required',
							'type' => 'text',
						),
						array(
							'label' => 'Unique?',
							'slug' => 'unique',
							'type' => 'text',
						),
					),
				),
                'assign' => 'generator',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
        );

        $this->streams->fields->add_fields($fields);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('module_builder');

        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}