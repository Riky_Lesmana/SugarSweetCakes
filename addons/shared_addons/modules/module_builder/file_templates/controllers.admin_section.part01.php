<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * %module_name% Module
 *
 * %module_description%
 *
 */
class Admin_%section_slug% extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = '%section_slug%';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'access_%section_slug%_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------

        $this->lang->load('%module_slug%');
        $this->load->driver('Streams');
		
		$this->load->model('%section_slug%_m');
    }

    /**
	 * List all %section_name% using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the %section_slug% database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Pagination
		// -------------------------------------

		$pagination_config['base_url'] = current_url();
		$pagination_config['uri_segment'] = 5;
		$pagination_config['total_rows'] = $this->%section_slug%_m->count_all_%section_slug%();
		$pagination_config['per_page'] = Settings::get('records_per_page');
		$this->pagination->initialize($pagination_config);
		$data['pagination_config'] = $pagination_config;
		
        // -------------------------------------
		// Get stream entries
		// -------------------------------------
		
        $data['%section_slug%']['entries'] = $this->%section_slug%_m->get_%section_slug%($pagination_config);
		$data['%section_slug%']['total'] = count($data['%section_slug%']['entries']);
		$data['%section_slug%']['pagination'] = $this->pagination->create_links();

		// -------------------------------------
		// Get multiple relationship
		// -------------------------------------
		
		/*foreach($data['%section_slug%']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', '%module_slug%');
			$attributes = array(
				'stream_slug' => '%section_slug%', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['%section_slug%']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'))
			->build('admin/%section_slug%_index', $data);
    }
	
	/**
     * Display one %section_name%
     *
     * We are using the Streams API to grab
     * data from the %section_slug% database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%') AND ! group_has_role('%module_slug%', 'view_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry. We are simply specifying
        // the stream/namespace.
		// -------------------------------------
		
        $data['%section_slug%'] = $this->%section_slug%_m->get_%section_slug%_by_id($id);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%')){
			if($data['%section_slug%']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
		// Get multiple relationship
		// -------------------------------------
		
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', '%module_slug%');
		$attributes = array(
			'stream_slug' => '%section_slug%', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['%section_slug%']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/admin/%module_slug%/%section_slug%/index')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'))
			->build('admin/%section_slug%_entry', $data);
    }
	
	/**
     * Create a new %section_name% entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'create_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
        // -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('new')){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('admin/%module_slug%/%section_slug%/index');
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('%section_slug%', '%module_slug%');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, null, 'new');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, null, 'new');
		$data['mode'] = 'new';
		$data['return'] = 'admin/%module_slug%/%section_slug%/index';
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/admin/%module_slug%/%section_slug%/index')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:new'))
			->build('admin/%section_slug%_form', $data);
    }
	
	/**
     * Edit a %section_name% entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the %section_name% to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%') AND ! group_has_role('%module_slug%', 'edit_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'edit_all_%section_slug%')){
			$created_by_user_id = $this->streams->entries->get_entry($id, '%section_slug%', '%module_slug%', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_%section_slug%('edit', $id)){	
				$this->session->set_flashdata('success', lang('%module_slug%:%section_slug%:submit_success'));				
				redirect('admin/%module_slug%/%section_slug%/index');
			}else{
				$data['messages']['error'] = lang('%module_slug%:%section_slug%:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('%section_slug%', '%module_slug%');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		if( ! $row = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$values = $this->fields->set_values($stream_fields, $row, 'edit');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, $row, 'edit');
		$data['mode'] = 'edit';
		$data['return'] = 'admin/%module_slug%/%section_slug%/view/'.$id;
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('%module_slug%:%section_slug%:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:plural'), '/admin/%module_slug%/%section_slug%/index')
			->set_breadcrumb(lang('%module_slug%:%section_slug%:view'), '/admin/%module_slug%/%section_slug%/view/'.$id)
			->set_breadcrumb(lang('%module_slug%:%section_slug%:edit'))
			->build('admin/%section_slug%_form', $data);
    }
	
	/**
     * Delete a %section_name% entry
     * 
     * @param   int [$id] The id of %section_name% to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%') AND ! group_has_role('%module_slug%', 'delete_own_%section_slug%')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('%module_slug%', 'delete_all_%section_slug%')){
			$created_by_user_id = $this->streams->entries->get_entry($id, '%section_slug%', '%module_slug%', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->%section_slug%_m->delete_%section_slug%_by_id($id);
        $this->session->set_flashdata('error', lang('%module_slug%:%section_slug%:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/%module_slug%/%section_slug%/index');
    }
	
	/**
     * Insert or update %section_name% entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_%section_slug%($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
		
		$stream = $this->streams->streams->get_stream('%section_slug%', '%module_slug%');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// Can't do nothing if we don't have any fields		
		if ($stream_fields === false)
		{
			return null;
		}
		
		// -------------------------------------
		// Set Validation Rules
		// -------------------------------------
		
		$this->form_validation->reset_validation();
		$validation_rules = $this->fields->set_rules($stream_fields, $method, array(), true, $row_id);
		
		// You can edit validation rules here by modifying $validation_rules
		// $validation_rules = ...
		
		$this->form_validation->set_rules($validation_rules);

		// -------------------------------------
		// Set Values
		// -------------------------------------
		
		// You can edit values here by modifying $values
		// $_POST[...] = ...

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result_id = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result_id = $this->%section_slug%_m->insert_%section_slug%($this->input->post(), $stream_fields, $stream);
				
			}
			else
			{
				$result_id = $this->%section_slug%_m->update_%section_slug%($this->input->post(), $stream_fields, $stream, $row_id);
			}
		}
		
		return $result_id;
	}

	// --------------------------------------------------------------------------

}