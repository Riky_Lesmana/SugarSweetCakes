		if(group_has_role('%module_slug%', 'access_%section_slug%_backend')){
			$info['sections']['%section_slug%']['name'] = '%module_slug%:%section_slug%:plural';
			$info['sections']['%section_slug%']['uri'] = 'admin/%module_slug%/%section_slug%/index';
			
			if(group_has_role('%module_slug%', 'create_%section_slug%')){
				$info['sections']['%section_slug%']['shortcuts']['create'] = array(
					'name' => '%module_slug%:%section_slug%:new',
					'uri' => 'admin/%module_slug%/%section_slug%/create',
					'class' => 'add'
				);
			}
		}
		
