<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * %section_class% model
 *
 * @author Aditya Satrya
 */
class %section_class%_m extends MY_Model {
	
	public function get_%section_slug%($pagination_config = NULL)
	{
		// -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		
		$params = array();
		$params['stream'] = '%section_slug%';
		$params['namespace'] = '%module_slug%';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		
		$params['limit'] = $pagination_config['per_page'];
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		
		//$params['order_by'] = 'created';
		//$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		
		//$params['where'] = ''; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		if($pagination_config != NULL){
			$params['paginate'] = 'yes';
			$params['pag_segment'] = $pagination_config['uri_segment'];
			$params['pag_base'] = $pagination_config['base_url'];
		}
		
		// -------------------------------------
		// Users
		// -------------------------------------
		
		if(! group_has_role('%module_slug%', 'view_all_%section_slug%')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// -------------------------------------
		// Send $params to viewer
		// -------------------------------------
		
        $data['params'] = $params;
		
		// -------------------------------------
		// Get stream entries
		// -------------------------------------
		
        $entries = $this->streams->entries->get_entries($params);
		return $entries['entries'];
	}
	
	public function get_%section_slug%_by_id($id)
	{
		return (array) $this->streams->entries->get_entry($id, '%section_slug%', '%module_slug%', true, true);
	}
	
	public function count_all_%section_slug%()
	{
		return $this->db->count_all('%module_slug%_%section_slug%');
	}
	
	public function delete_%section_slug%_by_id($id)
	{
		$this->streams->entries->delete_entry($id, '%section_slug%', '%module_slug%');
	}
	
	public function insert_%section_slug%($values, $stream_fields, $stream)
	{
		$result_id = $this->row_m->insert_entry($values, $stream_fields, $stream);
		return $result_id;
	}
	
	public function update_%section_slug%($values, $stream_fields, $stream, $row_id)
	{
		$result_id = $this->row_m->update_entry($stream_fields, $stream, $row_id, $values);
		return $result_id;
	}
	
}