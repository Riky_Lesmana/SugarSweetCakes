	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['%field_slug%']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['%field_slug%']['input_title']);?> <?php echo $fields['%field_slug%']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['%field_slug%']['input']; ?>
			
			<?php if( $fields['%field_slug%']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['%field_slug%']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

