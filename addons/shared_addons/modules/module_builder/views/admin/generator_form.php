<div class="page-header">
	<h1><?php echo lang('module_builder:generator:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="form-horizontal"'); ?>

<div class="form_inputs">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_name']['input_title']);?> <?php echo $fields['module_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_name']['input']; ?>
			<?php if( $fields['module_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_slug']['input_title']);?> <?php echo $fields['module_slug']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_slug']['input']; ?>
			<?php if( $fields['module_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_slug']['instructions']); ?></small>
			<?php endif; ?>		
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_description']['input_title']);?> <?php echo $fields['module_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_description']['input']; ?>
			<?php if( $fields['module_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_description']['instructions']); ?></small>
			<?php endif; ?>			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_has_frontend']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_has_frontend']['input_title']);?> <?php echo $fields['module_has_frontend']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_has_frontend']['input']; ?>
			<?php if( $fields['module_has_frontend']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_has_frontend']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_has_backend']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_has_backend']['input_title']);?> <?php echo $fields['module_has_backend']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_has_backend']['input']; ?>
			<?php if( $fields['module_has_backend']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_has_backend']['instructions']); ?></small>
			<?php endif; ?>			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_menu']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_menu']['input_title']);?> <?php echo $fields['module_menu']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_menu']['input']; ?>
			<?php if( $fields['module_menu']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_menu']['instructions']); ?></small>
			<?php endif; ?>			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_streams']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_streams']['input_title']);?> <?php echo $fields['module_streams']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_streams']['input']; ?>
			<?php if( $fields['module_streams']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_streams']['instructions']); ?></small>
			<?php endif; ?>			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['module_fields']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['module_fields']['input_title']);?> <?php echo $fields['module_fields']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['module_fields']['input']; ?>
			<?php if( $fields['module_fields']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['module_fields']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>