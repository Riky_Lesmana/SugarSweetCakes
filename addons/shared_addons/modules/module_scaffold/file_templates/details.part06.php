
		// %section_slug%
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE,
			),
			'' => array(
				'type' => '',
				'constraint' => '',
				'unsigned' => TRUE,
				'default' => '',
				'null' => TRUE,
				'auto_increment' => TRUE,
			),
			'created' => array(
				'type' => 'DATETIME',
			),
			'updated' => array(
				'type' => 'DATETIME',
				'null' => TRUE,
			),
			'created_by' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
			'ordering_count' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'null' => TRUE,
			),
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('%module_slug%_%section_slug%', TRUE);
		$this->db->query("CREATE INDEX author_index ON default_%module_slug%_%section_slug%(created_by)");

