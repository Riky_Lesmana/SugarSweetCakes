<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * %section_class% model
 *
 * @author Aditya Satrya
 */
class %section_class%_m extends MY_Model {
	
	public function get_%section_slug%($pagination_config = NULL)
	{
		$this->db->select('*');
		
		$start = ($this->uri->segment($pagination_config['uri_segment'])) ? $this->uri->segment($pagination_config['uri_segment']) : 0;
		$this->db->limit($pagination_config['per_page'], $start);
		
		$query = $this->db->get('default_%module_slug%_%section_slug%');
		$result = $query->result_array();
		
        return $result;
	}
	
	public function get_%section_slug%_by_id($id)
	{
		$this->db->select('*');
		$this->db->where('id', $id);
		$query = $this->db->get('default_%module_slug%_%section_slug%');
		$result = $query->row_array();
		
		return $result;
	}
	
	public function count_all_%section_slug%()
	{
		return $this->db->count_all('%module_slug%_%section_slug%');
	}
	
	public function delete_%section_slug%_by_id($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('default_%module_slug%_%section_slug%');
	}
	
	public function insert_%section_slug%($values)
	{
		$values['created'] = date("Y-m-d H:i:s");
		$values['created_by'] = $this->current_user->id;
		
		return $this->db->insert('default_%module_slug%_%section_slug%', $values);
	}
	
	public function update_%section_slug%($values, $row_id)
	{
		$values['updated'] = date("Y-m-d H:i:s");
		
		$this->db->where('id', $row_id);
		return $this->db->update('default_%module_slug%_%section_slug%', $values); 
	}
	
}