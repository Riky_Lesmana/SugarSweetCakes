	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('%module_slug%:created'); ?></div>
		<?php if(isset($%section_slug%['created'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($%section_slug%['created'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('%module_slug%:updated'); ?></div>
		<?php if(isset($%section_slug%['updated'])){ ?>
		<div class="entry-detail-value"><?php echo format_date($%section_slug%['updated'], 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('%module_slug%:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($%section_slug%['created_by'], true); ?></div>
	</div>
</div>