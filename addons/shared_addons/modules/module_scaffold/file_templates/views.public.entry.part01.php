<div class="page-header">
	<h1>
		<span><?php echo lang('%module_slug%:%section_slug%:view'); ?></span>
	</h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('%module_slug%/%section_slug%/index'); ?>" class="btn btn-sm btn-default">
			<i class="icon-arrow-left"></i>
			<?php echo lang('%module_slug%:back') ?>
		</a>

		<?php if(group_has_role('%module_slug%', 'edit_all_%section_slug%')){ ?>
			<a href="<?php echo site_url('%module_slug%/%section_slug%/edit/'.$%section_slug%['id']); ?>" class="btn btn-sm btn-default">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('%module_slug%', 'edit_own_%section_slug%')){ ?>
			<?php if($%section_slug%->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('%module_slug%/%section_slug%/edit/'.$%section_slug%['id']); ?>" class="btn btn-sm btn-default">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('%module_slug%', 'delete_all_%section_slug%')){ ?>
			<a href="<?php echo site_url('%module_slug%/%section_slug%/delete/'.$%section_slug%['id']); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('%module_slug%', 'delete_own_%section_slug%')){ ?>
			<?php if($%section_slug%->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('%module_slug%/%section_slug%/delete/'.$%section_slug%['id']); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $%section_slug%['id']; ?></div>
	</div>

