<div class="page-header">
	<h1><?php echo lang('module_scaffold:generator:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/module_scaffold/generator/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('module_scaffold:back') ?>
		</a>
		
		<?php if(group_has_role('module_scaffold', 'edit_all_generator')){ ?>
			<a href="<?php echo site_url('admin/module_scaffold/generator/edit/'.$generator->id); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('module_scaffold', 'edit_own_generator')){ ?>
			<?php if($generator->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/module_scaffold/generator/edit/'.$generator->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('module_scaffold', 'edit_all_generator')){ ?>
			<a href="<?php echo site_url('admin/module_scaffold/generator/build/'.$generator->id); ?>" class="btn btn-sm btn-success">
				<i class="icon-gear"></i>
				Build
			</a>
		<?php }elseif(group_has_role('module_scaffold', 'edit_own_generator')){ ?>
			<?php if($generator->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/module_scaffold/generator/build/'.$generator->id); ?>" class="btn btn-sm btn-success">
					<i class="icon-gear"></i>
					Build
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('module_scaffold', 'delete_all_generator')){ ?>
			<a href="<?php echo site_url('admin/module_scaffold/generator/delete/'.$generator->id); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('module_scaffold', 'delete_own_generator')){ ?>
			<?php if($generator->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/module_scaffold/generator/delete/'.$generator->id); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name">ID</div>
		<div class="entry-detail-value"><?php echo $generator->id; ?></div>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_name'); ?></div>
		<?php if($generator->module_name OR $generator->module_name == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_name; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_slug'); ?></div>
		<?php if($generator->module_slug OR $generator->module_slug == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_slug; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_description'); ?></div>
		<?php if($generator->module_description OR $generator->module_description == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_description; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_has_frontend'); ?></div>
		<?php if($generator->module_has_frontend OR $generator->module_has_frontend == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_has_frontend['value']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_has_backend'); ?></div>
		<?php if($generator->module_has_backend OR $generator->module_has_backend == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_has_backend['value']; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_menu'); ?></div>
		<?php if($generator->module_menu OR $generator->module_menu == ''){ ?>
		<div class="entry-detail-value"><?php echo $generator->module_menu; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_streams'); ?></div>
		<?php if($generator->module_streams OR $generator->module_streams == ''){ ?>
		<div class="entry-detail-value"><?php echo '<pre>'.print_r($generator->module_streams, TRUE).'</pre>'; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:module_fields'); ?></div>
		<?php if($generator->module_fields OR $generator->module_fields == ''){ ?>
		<div class="entry-detail-value"><?php echo '<pre>'.print_r($generator->module_fields, TRUE).'</pre>'; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:created'); ?></div>
		<?php if($generator->created){ ?>
		<div class="entry-detail-value"><?php echo format_date($generator->created, 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:updated'); ?></div>
		<?php if($generator->updated){ ?>
		<div class="entry-detail-value"><?php echo format_date($generator->updated, 'd-m-Y G:i'); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('module_scaffold:created_by'); ?></div>
		<div class="entry-detail-value"><?php echo user_displayname($generator->created_by, true); ?></div>
	</div>
</div>