<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['organization/admin/units(:any)'] = 'admin_units$1';
$route['organization/admin/types(:any)'] = 'admin_types$1';
$route['organization/admin/memberships(:any)'] = 'admin_memberships$1';
$route['organization/admin/titles(:any)'] = 'admin_titles$1';
$route['organization/units(:any)'] = 'organization_units$1';
$route['organization/types(:any)'] = 'organization_types$1';
$route['organization/memberships(:any)'] = 'organization_memberships$1';
$route['organization/titles(:any)'] = 'organization_titles$1';
