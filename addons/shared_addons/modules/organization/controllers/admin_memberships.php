<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_memberships extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'units';

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('organization');
		$this->lang->load('users/user');
        $this->load->driver('Streams');
		$this->load->library('Organization');

		$this->load->model('memberships_m');
    }

    /**
	 * List all Memberships using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the memberships database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index($unit_id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_memberships') AND ! group_has_role('organization', 'view_own_memberships')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		if(! group_has_role('organization', 'view_all_memberships')){
			if(! $this->organization->is_member($this->current_user->id, $unit_id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get Unit entry
		$data['unit'] = $this->streams->entries->get_entry($unit_id, 'units', 'organization', true);
		if( ! $data['unit']){
			die();
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'memberships';
		$params['namespace'] = 'organization';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		$params['order_by'] = 'membership_is_head';
		$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		$params['where'] = 'membership_unit = '.$unit_id;
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = 'unit_parents'; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 4;
		$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		//if(! group_has_role('organization', 'view_all_memberships')){
		//	$params['restrict_user'] = 'current'; //'current', user id, username
		//}
		
		// Get stream entries
        $data['memberships'] = $this->streams->entries->get_entries($params);

		// Get multiple relationship
		/*foreach($data['memberships']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', 'organization');
			$attributes = array(
				'stream_slug' => 'memberships', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['memberships']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

		// Get non-member users
		// - limit groups
		$limit_groups = array();

		// - skip groups
		$skip_groups = array();
		if($this->current_user->group != 'admin'){
			$skip_groups[] = 'admin';
		}
		if($this->current_user->group != 'site_admin'){
			$skip_groups[] = 'site_admin';
		}

		// - skip users
		$skip_user_ids = array();

		$data['non_member_users'] = $this->memberships_m->get_non_member_users($unit_id, $limit_groups, $skip_groups, $skip_user_ids);

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:memberships:plural'));
		$this->template->build('admin/memberships_index', $data);
    }
	
	/**
     * Create a new Memberships entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($unit_id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'create_all_units_memberships') AND ! group_has_role('organization', 'create_own_units_memberships')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		if(! group_has_role('organization', 'create_all_units_memberships')){
			if(! $this->organization->is_member($this->current_user->id, $unit_id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');

		// Get Unit entry
		$data['unit'] = $this->streams->entries->get_entry($unit_id, 'units', 'organization', true);
		if( ! $data['unit']){
			die();
		}

		if($_POST){
			$user_id = $this->input->post('membership_user');

			$params = array();
			$params['stream'] = 'memberships';
			$params['namespace'] = 'organization';
			$params['where'] = 'membership_unit = '.$unit_id.' AND membership_user = '.$user_id;
			$members = $this->streams->entries->get_entries($params);
			
			if(count($members['entries']) > 0){
				$this->session->set_flashdata('error', lang('organization:membership_user_already_registered'));
				redirect('admin/organization/memberships/create/'.$unit_id);
			}
		}
		
        $stream = $this->streams->streams->get_stream('memberships', 'organization');
		$namespace = 'organization';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => $this->input->post('return'),
            'success_message' => lang('organization:memberships:submit_success'),
            'failure_message' => lang('organization:memberships:submit_failure'),
            'title' => 'lang:organization:memberships:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}

		// Get ralationship
		$data['fields']['membership_unit']['entry_value'] = $this->streams->entries->get_entry($data['fields']['membership_unit']['value'], 'units', 'organization', true);

		// Set return page
		$data['return'] = 'admin/organization/memberships/index/'.$unit_id;
		
		// Build the form page.
        $this->template->title(lang('organization:memberships:new'));
        $this->template->build('admin/memberships_form', $data);
    }

	public function create_process($unit_id)
	{
		$user_id = $this->input->post('membership_user');

		// check membership
		$is_member = $this->memberships_m->check_membership($user_id, $unit_id);
		if($is_member){
			// Set the flashdata message and redirect
			$this->session->set_flashdata('error', lang('organization:membership_user_already_registered'));

			redirect('admin/organization/memberships/index/'.$unit_id);
		}

		// create membership
		$this->memberships_m->create_membership($user_id, $unit_id);

		// Set the flashdata message and redirect
		$this->session->set_flashdata('success', lang('organization:memberships:submit_success'));
		redirect('admin/organization/memberships/index/'.$unit_id);
	}
	
	/**
     * Delete a Memberships entry
     * 
     * @param   int [$id] The id of Memberships to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_memberships') AND ! group_has_role('organization', 'delete_own_memberships')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_memberships')){
			$membership_entry = $this->streams->entries->get_entry($id, 'memberships', 'organization', true);
			$unit_id = $membership_entry->membership_unit;
			if(! $this->organization->is_member($this->current_user->id, $unit_id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get will-be-deleted entry
		$membership = $this->streams->entries->get_entry($id, 'memberships', 'organization', true);

		$this->streams->entries->delete_entry($id, 'memberships', 'organization');
        $this->session->set_flashdata('success', lang('organization:memberships:deleted'));

        if(! group_has_role('organization', 'view_all_memberships')){
			redirect('admin/organization/units/index/');
		}else{
			redirect('admin/organization/memberships/index/'.$membership->membership_unit['id']);
		}
    }

}