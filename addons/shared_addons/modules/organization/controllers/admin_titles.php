<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_titles extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'titles';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('organization', 'access_titles_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('organization');
        $this->load->driver('Streams');
    }

    /**
	 * List all Titles using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the titles database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_titles') AND ! group_has_role('organization', 'view_own_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'titles';
		$params['namespace'] = 'organization';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		$params['order_by'] = 'title_name';
		$params['sort'] = 'asc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		//$params['where'] = ''; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['paginate'] = 'no';
		//$params['pag_segment'] = 4;
		//$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		if(! group_has_role('organization', 'view_all_titles')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// Get stream entries
        $data['titles'] = $this->streams->entries->get_entries($params);

		// Get multiple relationship
		/*foreach($data['titles']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', 'organization');
			$attributes = array(
				'stream_slug' => 'titles', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['titles']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:titles:plural'));
		$this->template->build('admin/titles_index', $data);
    }
	
	/**
     * Display one Titles
     *
     * We are using the Streams API to grab
     * data from the titles database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'view_all_titles') AND ! group_has_role('organization', 'view_own_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['titles'] = $this->streams->entries->get_entry($id, 'titles', 'organization', true);
		
		// Check view all/own permission
		if(! group_has_role('organization', 'view_all_titles')){
			if($data['titles']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:titles:view'));
        $this->template->build('admin/titles_entry', $data);
    }
	
	/**
     * Create a new Titles entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('organization', 'create_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
        $stream = $this->streams->streams->get_stream('titles', 'organization');
		$namespace = 'organization';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/organization/titles/index',
            'success_message' => lang('organization:titles:submit_success'),
            'failure_message' => lang('organization:titles:submit_failure'),
            'title' => 'lang:organization:titles:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('organization:titles:new'));
        $this->template->build('admin/titles_form', $data);
    }
	
	/**
     * Create a new Titles entry
     *
     * We're using the entry_form function
     * to generate the form. This is similar to create(), but we are
     * building the entry form automatically using method entry_form()
	 * from Streams CP Driver
     *
     * @return	void
     */
    public function create_alt()
    {
        // Check permission
		if(! group_has_role('organization', 'create_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		$streams = 'titles';
		$namespace = 'organization';
		$mode = 'new'; //'new', 'edit'
		$entry_id = null;
		$view_override = true;
		$extra = array(
            'return' => 'admin/organization/titles/index',
            'success_message' => lang('organization:titles:submit_success'),
            'failure_message' => lang('organization:titles:submit_failure'),
            'title' => 'lang:organization:titles:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$tabs = false;
		$hidden = array();
		$defaults = array();
		
		$this->streams->cp->entry_form($streams, $namespace, $mode, $entry_id, $view_override, $extra, $skips, $tabs, $hidden, $defaults);
    }
	
	/**
     * Edit a Titles entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Titles to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_titles') AND ! group_has_role('organization', 'edit_own_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_titles')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'titles', 'organization', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
		$stream = $this->streams->streams->get_stream('titles', 'organization');
		$namespace = 'organization';
		$mode = 'edit'; //'new', 'edit'
		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/organization/titles/index',
            'success_message' => lang('organization:titles:submit_success'),
            'failure_message' => lang('organization:titles:submit_failure'),
            'title' => 'lang:organization:titles:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('organization:titles:new'));
        $this->template->build('admin/titles_form', $data);
    }
	
	/**
     * Edit a Titles entry
     *
     * We're using the entry_form function
     * to generate the edit form. We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
     *
     * @param   int [$id] The id of the Titles to the be deleted.
     * @return	void
     */
    public function edit_alt($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_titles') AND ! group_has_role('organization', 'edit_own_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_titles')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'titles', 'organization', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$streams = 'titles';
		$namespace = 'organization';
		$mode = 'edit'; //'new', 'edit'
		$entry_id = $id;
		$view_override = true;
		$extra = array(
            'return' => 'admin/organization/titles/index',
            'success_message' => lang('organization:titles:submit_success'),
            'failure_message' => lang('organization:titles:submit_failure'),
            'title' => 'lang:organization:titles:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$tabs = false;
		$hidden = array();
		$defaults = array();
		
		$this->streams->cp->entry_form($streams, $namespace, $mode, $entry_id, $view_override, $extra, $skips, $tabs, $hidden, $defaults);
    }
    
    /**
     * Delete a Titles entry
     * 
     * @param   int [$id] The id of Titles to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_titles') AND ! group_has_role('organization', 'delete_own_titles')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_titles')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'titles', 'organization', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
        $this->streams->entries->delete_entry($id, 'titles', 'organization');
        $this->session->set_flashdata('error', lang('organization:titles:deleted'));
 
        redirect('admin/organization/titles/index');
    }

}