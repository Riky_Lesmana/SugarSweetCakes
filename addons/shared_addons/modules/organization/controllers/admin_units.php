<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Organization Module
 *
 * Module to manage organization
 *
 */
class Admin_units extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'units';

	private $units_arr = array();
	private $sorted_units_arr = array();

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('organization', 'access_units_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('organization');
        $this->load->driver('Streams');
		$this->load->library('Organization');
		
		$this->load->model('units_m');
		$this->load->model('types_m');
    }

    /**
	 * List all Units using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the units database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        $root_units = $this->units_m->get_units_by_level(0);
		$units = $this->organization->build_serial_tree(array(), $root_units);
		
		$data['units']['total'] = count($units);
		$data['units']['entries'] = $units;
		$data['units']['pagination'] = '';

		// -------------------------------------
		// Restric users
		// -------------------------------------
		// Eliminate not owned unit
		if(! group_has_role('organization', 'view_all_units')){
			$temp_arr = array();
			foreach($data['units']['entries'] as $unit){
				if($this->organization->is_member($this->current_user->id, $unit['id'])){
					$temp_arr[] = $unit;
				}
			}
			$data['units']['entries'] = $temp_arr;
			$data['units']['total'] = count($temp_arr);
		}

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:units:plural'));
		$this->template->build('admin/units_index', $data);
    }
	
	/**
     * Display one Units
     *
     * We are using the Streams API to grab
     * data from the units database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'view_all_units') AND ! group_has_role('organization', 'view_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['units'] = $this->streams->entries->get_entry($id, 'units', 'organization', true);
		$data['units']->unit_type = $this->streams->entries->get_entry($data['units']->id, 'types', 'organization', true);
		
		// Check view all/own permission
		if(! group_has_role('organization', 'view_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		$field = $this->fields_m->get_field_by_slug('unit_parents', 'organization');
		$attributes = array(
			'stream_slug' => 'units', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['units']->unit_parents = $this->type->types->multiple->plugin_override($field, $attributes);

		// Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('organization:units:view'));
        $this->template->build('admin/units_entry', $data);
    }
	
	/**
     * Create a new Units entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('organization', 'create_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
        $stream = $this->streams->streams->get_stream('units', 'organization');
		$namespace = 'organization';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/organization/units/index',
            'success_message' => lang('organization:units:submit_success'),
            'failure_message' => lang('organization:units:submit_failure'),
            'title' => 'lang:organization:units:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('organization:units:new'));
        $this->template->build('admin/units_form', $data);
    }
	
	/**
     * Edit a Units entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Units to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('organization', 'edit_all_units') AND ! group_has_role('organization', 'edit_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'edit_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
		$stream = $this->streams->streams->get_stream('units', 'organization');
		$namespace = 'organization';
		$mode = 'edit'; //'new', 'edit'
		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/organization/units/index',
            'success_message' => lang('organization:units:submit_success'),
            'failure_message' => lang('organization:units:submit_failure'),
            'title' => 'lang:organization:units:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('organization:units:edit'));
        $this->template->build('admin/units_form', $data);
    }
	
	/**
     * Delete a Units entry
     * 
     * @param   int [$id] The id of Units to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('organization', 'delete_all_units') AND ! group_has_role('organization', 'delete_own_units')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('organization', 'delete_all_units')){
			if(! $this->organization->is_member($this->current_user->id, $id)){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$children = $this->units_m->get_units_by_parent($id);
		if(count($children) > 0){
			$this->session->set_flashdata('error', lang('organization:units:delete_fail_has_child'));
		}else{
			$this->streams->entries->delete_entry($id, 'units', 'organization');
			$this->session->set_flashdata('success', lang('organization:units:deleted'));
		}
		
		redirect('admin/organization/units/index');
    }
	
	public function ajax_unit_parents_checkbox($type_id = 0){
		$type = $this->types_m->get_type_by_id($type_id);
		
		if($type['type_level'] != NULL AND $type['type_level'] > 0){
			$parent_level = $type['type_level'] - 1;			
			$units = $this->units_m->get_units_by_level($parent_level);
			
			foreach($units as $unit){
				echo '<input type="checkbox" name="unit_parents[]" id="unit_parents" value="'.$unit['id'].'" /> '.$unit['unit_name'].'<br />';
			}
		}else{
			echo '<input name="unit_parents" value="" type="hidden" />';
			echo '<p id="unit_parents" class="form-control-static" />-</p>';
		}
	}

}