<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Organization extends Module
{
    public $version = '1.1';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Organization',
			'id' => 'Organization',
		);
		$info['description'] = array(
			'en' => 'Module to manage organization',
			'id' => 'Module to manage organization',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'users';
		$info['roles'] = array(
			'access_units_backend', 'view_all_units', 'view_own_units', 'edit_all_units', 'edit_own_units', 'delete_all_units', 'delete_own_units', 'create_units',
			'access_types_backend', 'view_all_types', 'view_own_types', 'edit_all_types', 'edit_own_types', 'delete_all_types', 'delete_own_types', 'create_types',
			'view_all_memberships', 'view_own_memberships', 'edit_all_memberships', 'edit_own_memberships', 'delete_all_memberships', 'delete_own_memberships', 'create_all_units_memberships', 'create_own_units_memberships',
			'access_titles_backend', 'view_all_titles', 'view_own_titles', 'edit_all_titles', 'edit_own_titles', 'delete_all_titles', 'delete_own_titles', 'create_titles',
		);
		
		if(group_has_role('organization', 'access_units_backend')){
			$info['sections']['units']['name'] = 'organization:units:plural';
			$info['sections']['units']['uri'] = 'admin/organization/units/index';
			
			if(group_has_role('organization', 'create_units')){
				$info['sections']['units']['shortcuts']['create'] = array(
					'name' => 'organization:units:new',
					'uri' => 'admin/organization/units/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('organization', 'access_types_backend')){
			$info['sections']['types']['name'] = 'organization:types:plural';
			$info['sections']['types']['uri'] = 'admin/organization/types/index';
			
			if(group_has_role('organization', 'create_types')){
				$info['sections']['types']['shortcuts']['create'] = array(
					'name' => 'organization:types:new',
					'uri' => 'admin/organization/types/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('organization', 'access_titles_backend')){
			$info['sections']['titles']['name'] = 'organization:titles:plural';
			$info['sections']['titles']['uri'] = 'admin/organization/titles/index';
			
			if(group_has_role('organization', 'create_titles')){
				$info['sections']['titles']['shortcuts']['create'] = array(
					'name' => 'organization:titles:new',
					'uri' => 'admin/organization/titles/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        // We're using the streams API to
        // do data setup.
        $this->load->driver('Streams');

        $this->load->language('organization/organization');

        // Add streams
        if ( ! $units_stream_id = $this->streams->streams->add_stream('lang:organization:units:plural', 'units', 'organization', 'organization_', null)) return false;
        if ( ! $types_stream_id = $this->streams->streams->add_stream('lang:organization:types:plural', 'types', 'organization', 'organization_', null)) return false;
        if ( ! $memberships_stream_id = $this->streams->streams->add_stream('lang:organization:memberships:plural', 'memberships', 'organization', 'organization_', null)) return false;
        if ( ! $titles_stream_id = $this->streams->streams->add_stream('lang:organization:titles:plural', 'titles', 'organization', 'organization_', null)) return false;

        // Add some fields
        $fields = array(
            array(
                'name' => 'lang:organization:unit_name',
                'slug' => 'unit_name',
                'namespace' => 'organization',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'units',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:organization:unit_abbrevation',
                'slug' => 'unit_abbrevation',
                'namespace' => 'organization',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
			array(
                'name' => 'lang:organization:unit_slug',
                'slug' => 'unit_slug',
                'namespace' => 'organization',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '-',
					'slug_field' => 'unit_name',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:organization:unit_description',
                'slug' => 'unit_description',
                'namespace' => 'organization',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:unit_type',
                'slug' => 'unit_type',
                'namespace' => 'organization',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $types_stream_id,
					'link_uri' => 'admin/organization/types/view/-id-',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:unit_sort_order',
                'slug' => 'unit_sort_order',
                'namespace' => 'organization',
                'type' => 'integer',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
			array(
                'name' => 'lang:organization:unit_parents',
                'slug' => 'unit_parents',
                'namespace' => 'organization',
                'type' => 'multiple',
                'extra' => array(
					'choose_stream' => $units_stream_id,
					//'choose_ui' => '',
				),
                'assign' => 'units',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:type_name',
                'slug' => 'type_name',
                'namespace' => 'organization',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'types',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
			array(
                'name' => 'lang:organization:type_slug',
                'slug' => 'type_slug',
                'namespace' => 'organization',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '-',
					'slug_field' => 'type_name',
				),
                'assign' => 'types',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:organization:type_description',
                'slug' => 'type_description',
                'namespace' => 'organization',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
				),
                'assign' => 'types',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
			array(
                'name' => 'lang:organization:type_level',
                'slug' => 'type_level',
                'namespace' => 'organization',
                'type' => 'integer',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'types',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:title_name',
                'slug' => 'title_name',
                'namespace' => 'organization',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'titles',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:organization:title_description',
                'slug' => 'title_description',
                'namespace' => 'organization',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
				),
                'assign' => 'titles',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:membership_unit',
                'slug' => 'membership_unit',
                'namespace' => 'organization',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $units_stream_id,
					'link_uri' => 'admin/organization/units/view/-id-',
				),
                'assign' => 'memberships',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:membership_user',
                'slug' => 'membership_user',
                'namespace' => 'organization',
                'type' => 'user',
                'extra' => array(
					//'restrict_group' => '',
				),
                'assign' => 'memberships',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:membership_title',
                'slug' => 'membership_title',
                'namespace' => 'organization',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $titles_stream_id,
					'link_uri' => 'admin/organization/titles/view/-id-',
				),
                'assign' => 'memberships',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:organization:membership_is_head',
                'slug' => 'membership_is_head',
                'namespace' => 'organization',
                'type' => 'choice',
                'extra' => array(
					'choice_data' => "true : Yes\nfalse : No",
					'choice_type' => 'dropdown',
				),
                'assign' => 'memberships',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
        );

        $this->streams->fields->add_fields($fields);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('organization');

        return true;
    }

    public function upgrade($old_version)
    {
        switch($old_version){
			case '1.0':
				
				// add fields
				$fields = array(
					array(
						'name' => 'lang:organization:type_slug',
						'slug' => 'type_slug',
						'namespace' => 'organization',
						'type' => 'slug',
						'extra' => array(
							'space_type' => '-',
							'slug_field' => 'type_name',
						),
						'assign' => 'types',
						'title_column' => false,
						'required' => true,
						'unique' => true,
					),
					array(
						'name' => 'lang:organization:type_level',
						'slug' => 'type_level',
						'namespace' => 'organization',
						'type' => 'integer',
						'extra' => array(
							//'max_length' => '',
							//'default_value' => '',
						),
						'assign' => 'types',
						'title_column' => false,
						'required' => true,
						'unique' => false,
					),
					array(
						'name' => 'lang:organization:unit_slug',
						'slug' => 'unit_slug',
						'namespace' => 'organization',
						'type' => 'slug',
						'extra' => array(
							'space_type' => '-',
							'slug_field' => 'unit_name',
						),
						'assign' => 'units',
						'title_column' => false,
						'required' => true,
						'unique' => true,
					),
				);
				$this->streams->fields->add_fields($fields);
				
				// delete fields
				$this->streams->fields->delete_field('unit_level', 'organization');
		}
		
		return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}