<?php

/*****************************************************
 * STREAMS: units
 *****************************************************/
$lang['organization:units:singular'] = 'Units';
$lang['organization:units:plural'] = 'Units';
$lang['organization:units:no_entry'] = 'There are currently no Units';
$lang['organization:units:new'] = 'New Units';
$lang['organization:units:edit'] = 'Edit Units';
$lang['organization:units:view'] = 'Units Detail';
$lang['organization:units:submit_success'] = 'Units submitted successfully.';
$lang['organization:units:submit_failure'] = 'There was a problem submitting your Units.';
$lang['organization:units:deleted'] = 'The Units was deleted.';
$lang['organization:units:delete_fail_has_child'] = 'This unit can\'t be deleted because has child.';

/*****************************************************
 * STREAMS: types
 *****************************************************/
$lang['organization:types:singular'] = 'Types';
$lang['organization:types:plural'] = 'Types';
$lang['organization:types:no_entry'] = 'There are currently no Types';
$lang['organization:types:new'] = 'New Types';
$lang['organization:types:edit'] = 'Edit Types';
$lang['organization:types:view'] = 'Types Detail';
$lang['organization:types:submit_success'] = 'Types submitted successfully.';
$lang['organization:types:submit_failure'] = 'There was a problem submitting your Types.';
$lang['organization:types:deleted'] = 'The Types was deleted.';

/*****************************************************
 * STREAMS: memberships
 *****************************************************/
$lang['organization:memberships:singular'] = 'Memberships';
$lang['organization:memberships:plural'] = 'Memberships';
$lang['organization:memberships:no_entry'] = 'There are currently no member';
$lang['organization:memberships:new'] = 'Add member';
$lang['organization:memberships:edit'] = 'Edit Memberships';
$lang['organization:memberships:view'] = 'Memberships Detail';
$lang['organization:memberships:submit_success'] = 'Member added successfully.';
$lang['organization:memberships:submit_failure'] = 'There was a problem submitting new member.';
$lang['organization:memberships:deleted'] = 'The Memberships was deleted.';

/*****************************************************
 * STREAMS: titles
 *****************************************************/
$lang['organization:titles:singular'] = 'Titles';
$lang['organization:titles:plural'] = 'Titles';
$lang['organization:titles:no_entry'] = 'There are currently no Titles';
$lang['organization:titles:new'] = 'New Titles';
$lang['organization:titles:edit'] = 'Edit Titles';
$lang['organization:titles:view'] = 'Titles Detail';
$lang['organization:titles:submit_success'] = 'Titles submitted successfully.';
$lang['organization:titles:submit_failure'] = 'There was a problem submitting your Titles.';
$lang['organization:titles:deleted'] = 'The Titles was deleted.';


/*****************************************************
 * FIELDS
 *****************************************************/
$lang['organization:unit_name'] = 'Name';
$lang['organization:unit_abbrevation'] = 'Abbrevation';
$lang['organization:unit_slug'] = 'Slug';
$lang['organization:unit_description'] = 'Description';
$lang['organization:unit_type'] = 'Type';
$lang['organization:unit_sort_order'] = 'Sort Order';
$lang['organization:unit_parents'] = 'Parents';
$lang['organization:type_name'] = 'Name';
$lang['organization:type_slug'] = 'Slug';
$lang['organization:type_level'] = 'Level';
$lang['organization:type_description'] = 'Description';
$lang['organization:title_name'] = 'Title Name';
$lang['organization:title_description'] = 'Description';
$lang['organization:membership_unit'] = 'Unit';
$lang['organization:membership_user'] = 'User';
$lang['organization:membership_title'] = 'Title';
$lang['organization:membership_is_head'] = 'Is Head?';

/*****************************************************
 * DEFAULT FIELDS
 *****************************************************/
$lang['organization:created'] = 'Created';
$lang['organization:updated'] = 'Updated';
$lang['organization:created_by'] = 'Created by';

/*****************************************************
 * STREAMS (GENERAL)
 *****************************************************/
$lang['organization:streams'] = 'Streams';
$lang['organization:view_options'] = 'View Options';
$lang['organization:field_assignments'] = 'Field Assignments';
$lang['organization:new_assignment'] = 'New Field Assignment';
$lang['organization:edit_assignment'] = 'Edit Field Assignment';

/*****************************************************
 * GLOBAL
 *****************************************************/
$lang['organization:back'] = 'Back';
$lang['organization:confirm_delete'] = 'Are you sure?';
$lang['organization:detail'] = 'Detail';

$lang['organization:membership_user_already_registered'] = 'User is already become member in this Unit.';
$lang['organization:count_member'] = '# Members';
$lang['organization:you'] = '+you';
$lang['organization:loading'] = 'Loading...';