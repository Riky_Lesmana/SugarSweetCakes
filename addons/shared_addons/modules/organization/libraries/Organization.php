<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Organization Library
 *
 * @author Aditya Satrya
 */

class Organization
{
	/**
	 * The Construct
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->driver('Streams');
	}

	public function is_member($user_id, $unit_id)
	{
		// Get our entries
		$params = array();
		$params['stream'] = 'memberships';
		$params['namespace'] = 'organization';
		$params['where'] = 'membership_unit = '.$unit_id;

		// Get stream entries
        $members = $this->CI->streams->entries->get_entries($params);

		// Check if member
		foreach($members['entries'] as $member){
			if($member['membership_user']['user_id'] == $user_id){
				return TRUE;
			}
		}

		return FALSE;
	}

	public function get_membership_by_unit($unit_id)
	{
		// Get our entries
		$params = array();
		$params['stream'] = 'memberships';
		$params['namespace'] = 'organization';
		$params['where'] = 'membership_unit = '.$unit_id;

		// Get stream entries
        $members = $this->CI->streams->entries->get_entries($params);

		return $members;
	}

	public function get_membership_by_user($user_id)
	{
		// Get our entries
		$params = array();
		$params['stream'] = 'memberships';
		$params['namespace'] = 'organization';
		$params['where'] = 'membership_user = '.$user_id;

		// Get stream entries
        $units = $this->CI->streams->entries->get_entries($params);
		
		return $units;
	}

	public function build_serial_tree($serial_tree, $units, $level = 0)
	{
		foreach($units as $unit){
			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '-- ';
			}

			$unit['unit_name'] = $prefix.$unit['unit_name'];

			$serial_tree[] = $unit;
			$children = $this->CI->units_m->get_units_by_parent($unit['id']);

			if(count($children) == 0){
				continue;
			}else{
				$serial_tree = $this->build_serial_tree($serial_tree, $children, $level+1);
			}
		}

		return $serial_tree;
	}
}