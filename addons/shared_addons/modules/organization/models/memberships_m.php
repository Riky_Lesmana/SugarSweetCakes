<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Memberships model
 *
 * @author Aditya Satrya
 */
class Memberships_m extends MY_Model {
	
	public function get_membership_by_unit($unit_id)
	{
		$this->db->select('organization_memberships.id AS membership_id');
		$this->db->select('organization_memberships.membership_unit AS unit_id');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_types.id AS unit_type_id');
		$this->db->select('organization_types.type_name AS unit_type_name');
		$this->db->select('organization_memberships.membership_user AS user_id');
		$this->db->select('users.email AS user_email');
		$this->db->select('users.username AS user_username');
		$this->db->select('profiles.display_name AS user_display_name');
		$this->db->select('groups.id AS user_group_id');
		$this->db->select('groups.name AS user_group_name');
		$this->db->select('groups.description AS user_group_description');

		$this->db->where('organization_memberships.membership_unit', $unit_id);
		$this->db->where('users.active', 1);

		$this->db->join('organization_units', 'organization_units.id = organization_memberships.membership_unit', 'left');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		$this->db->join('users', 'users.id = organization_memberships.membership_user', 'left');
		$this->db->join('groups', 'users.group_id = groups.id', 'left');
		$this->db->join('profiles', 'users.id = profiles.user_id', 'left');

		$res = $this->db->get('organization_memberships')->result_array();

		return $res;
	}

	public function get_memberships_by_user($user_id)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'memberships';
		$params['namespace'] = 'organization';
		$params['where'] = array(
			$this->db->dbprefix(NULL).'organization_memberships.membership_user = '.$user_id,
		);
		
		return $this->streams->entries->get_entries($params);
	}

	public function delete_membership_by_user($user_id)
	{
		$this->db->where('membership_user', $user_id);
		$this->db->delete($this->db->dbprefix(NULL).'organization_memberships');
	}

	public function check_membership($user_id, $unit_id)
	{
		$this->where('organization_memberships.membership_user', $user_id);
		$this->where('organization_memberships.membership_unit', $unit_id);

		$res = $this->db->get('organization_memberships')->result_array();

		if(count($res) > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function create_membership($user_id, $unit_id)
	{
		$data = array(
			'membership_user' => $user_id,
			'membership_unit' => $unit_id,
			'created' => date("Y-m-d H:i:s"),
		);
		$this->db->insert($this->db->dbprefix(NULL).'organization_memberships', $data);
	}

	public function get_non_member_users($unit_id, $limit_groups = array(), $skip_groups = array(), $skip_user_ids = array())
	{
		// First, get all already-member user
		$unit_members = $this->get_membership_by_unit($unit_id);
		$unit_member_ids = array();
		foreach($unit_members as $unit_member){
			$unit_member_ids[] = $unit_member['user_id'];
		}

		// Second, make query
		$this->db->select('users.id AS id');
		$this->db->select('users.email AS email');
		$this->db->select('users.username AS username');
		$this->db->select('users.group_id AS group_id');
		$this->db->select('groups.name AS group_name');
		$this->db->select('groups.description AS group_description');
		$this->db->select('users.created_on AS created_on');
		$this->db->select('users.last_login AS last_login');
		$this->db->select('profiles.*');

		$this->db->where('users.active', 1);

		// remove already-member users
		foreach($unit_member_ids as $unit_member_id){
			$this->db->where('users.id !=', $unit_member_id);
		}

		// skip users (also add from)
		if(is_array($skip_user_ids) AND count($skip_user_ids) > 0){
			foreach($skip_user_ids as $skip_user_id){
				$this->db->where('users.id !=', $skip_user_id);
			}
		}

		// skip groups
		if(is_array($skip_groups) AND count($skip_groups) > 0){
			foreach($skip_groups as $skip_group){
				$this->db->where('groups.name !=', $skip_group);
			}
		}

		// limit only from certain groups
		if(is_array($limit_groups) AND count($limit_groups) > 0){
			$query_arr = array();
			foreach($limit_groups as $limit_group){
				$query_arr[] = $this->db->dbprefix(NULL)."groups.name = '".$limit_group."'";
			}
			$this->db->where('('.implode(' OR ', $query_arr).')');
		}

		$this->db->join('groups', 'users.group_id = groups.id', 'left');
		$this->db->join('profiles', 'users.id = profiles.user_id', 'left');

		$this->db->order_by('group_description', 'ASC');
		$this->db->order_by('profiles.display_name', 'ASC');

		$res = $this->db->get('users')->result_array();

		return $res;
	}
}