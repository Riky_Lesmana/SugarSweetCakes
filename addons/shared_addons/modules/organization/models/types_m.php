<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Types model
 *
 * @author Aditya Satrya
 */
class Types_m extends MY_Model {
	
	public function get_type_by_id($type_id)
	{
		$this->db->where('id', $type_id);
		$res = $this->db->get('organization_types')->result_array();
		
		if(count($res) >= 1){
			return $res[0];
		}else{
			return NULL;
		}
	}
	
}