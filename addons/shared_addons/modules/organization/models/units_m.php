<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Units model
 *
 * @author Aditya Satrya
 */
class Units_m extends MY_Model {
	
	public function get_all_units()
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'units';
		$params['namespace'] = 'organization';
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_units_by_level($level)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');
		
		$this->db->where('organization_types.type_level', $level);
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		
		$res = $this->db->get('organization_units')->result_array();
		return $res;
	}
	
	public function get_units_by_parent($parent_id)
	{
		$this->db->select('organization_units.id AS id');
		$this->db->select('organization_units.created AS created');
		$this->db->select('organization_units.updated AS updated');
		$this->db->select('organization_units.created_by AS created_by');
		$this->db->select('organization_units.ordering_count AS ordering_count');
		$this->db->select('organization_units.unit_name AS unit_name');
		$this->db->select('organization_units.unit_abbrevation AS unit_abbrevation');
		$this->db->select('organization_units.unit_description AS unit_description');
		$this->db->select('organization_units.unit_type AS unit_type');
		$this->db->select('organization_units.unit_sort_order AS unit_sort_order');
		$this->db->select('organization_units.unit_slug AS unit_slug');
		$this->db->select('organization_types.type_name AS type_name');
		$this->db->select('organization_types.type_description AS type_description');
		$this->db->select('organization_types.type_slug AS type_slug');
		$this->db->select('organization_types.type_level AS type_level');

		$this->db->where('organization_units_units.units_id', $parent_id);
		
		$this->db->join('organization_units', 'organization_units.id = organization_units_units.row_id', 'left');
		$this->db->join('organization_types', 'organization_types.id = organization_units.unit_type', 'left');
		
		$res = $this->db->get('organization_units_units')->result_array();
		
		return $res;
	}
	
}