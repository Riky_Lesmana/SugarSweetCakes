<div class="page-header">
	<h1><?php echo $unit->unit_name; ?></h1>

	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/organization/units/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>
		
	</div>
</div>

<div class="tabbable tab-link">
	<ul class="nav nav-tabs">
		<li class="">
			<a href="<?php echo site_url('admin/organization/units/view/'.$unit->id); ?>"><?php echo lang('organization:detail'); ?></a>
		</li>

		<?php if(group_has_role('organization', 'view_all_memberships')){ ?>
		<li class="active">
			<a href="<?php echo site_url('admin/organization/memberships/index/'.$unit->id); ?>">
				<?php echo lang('organization:memberships:singular'); ?>

				<?php $members = $this->organization->get_membership_by_unit($unit->id); ?>
				<span class="badge badge-danger"><?php echo $members['total']; ?></span>
			</a>
		</li>
		<?php } ?>
	</ul>
</div>

<?php if(group_has_role('organization', 'create_all_units_memberships')){ ?>
<form method="post" action="<?php echo site_url('admin/organization/memberships/create_process').'/'.$unit->id; ?>">
	<div class="form-inline">
		<select name="membership_user" class="chosen col-sm-5">
			<option>--- Pilih Pengguna ---</option>
			<optgroup label="Grup">
				<?php
				$count = 0;
				$last_group = '';
				foreach($non_member_users as $non_member_user){
					if($last_group == '' OR $non_member_user['group_description'] != $last_group){
						if($count > 0){
							echo '</optgroup>';
						}
						echo '<optgroup label="'.$non_member_user['group_description'].'">';
					}
					echo '<option value="'.$non_member_user['id'].'">'.$non_member_user['display_name'].' ('.$non_member_user['email'].')</option>';

					$count++;
					$last_group = $non_member_user['group_description'];
				}

				echo '</optgroup>';
				?>
			</optgroup>
		</select>

		<button type="submit" class="btn btn-xs btn-yellow">
			<i class="icon-plus"></i>
			<?php echo lang('organization:memberships:new') ?>
		</button>
	</div>
</form>
<?php } ?>

<div class="space-8"></div>
	
<?php if ($memberships['total'] > 0): ?>

	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>No</th>
				<th><?php echo lang('organization:membership_user'); ?></th>
				<th><?php echo lang('user:group_label'); ?></th>
				<th><?php echo lang('organization:created').'/'.lang('organization:updated'); ?></th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php $count = 1; ?>
			<?php foreach ($memberships['entries'] as $memberships_entry): ?>
			<tr>
				<td><?php echo $count++; ?></td>
				<td><?php echo user_displayname($memberships_entry['membership_user']['user_id'], true, true); ?></td>

				<td><?php user_groups($memberships_entry['membership_user']['user_id']); ?></td>

				<?php if($memberships_entry['created']){ ?>
				<td><?php echo format_date($memberships_entry['created'], 'd-m-Y G:i'); ?></td>
				<?php }else{ ?>
				<td>-</td>
				<?php } ?>

				<td class="actions">
				<?php
				if(group_has_role('organization', 'delete_all_memberships')){
					echo anchor('admin/organization/memberships/delete/' . $memberships_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}elseif(group_has_role('organization', 'delete_own_memberships')){
					if($memberships_entry['membership_user']['user_id'] == $this->current_user->id){
						echo anchor('admin/organization/memberships/delete/' . $memberships_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
					}
				}
				?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php echo $memberships['pagination']; ?>

<?php else: ?>
	<div class="well"><?php echo lang('organization:memberships:no_entry'); ?></div>
<?php endif;?>

<script>
	$(".chosen").chosen();
</script>
