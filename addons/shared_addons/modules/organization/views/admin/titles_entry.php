<div class="page-header">
	<h1><?php echo lang('organization:titles:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/organization/titles/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>

		<?php if(group_has_role('organization', 'edit_all_titles')){ ?>
			<a href="<?php echo site_url('admin/organization/titles/edit/'.$titles->id); ?>" class="btn btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'edit_own_titles')){ ?>
			<?php if($titles->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/organization/titles/edit/'.$titles->id); ?>" class="btn btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('organization', 'delete_all_titles')){ ?>
			<a href="<?php echo site_url('admin/organization/titles/delete/'.$titles->id); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-danger"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'delete_own_titles')){ ?>
			<?php if($titles->created_by_user_id == $this->current_user->id){ ?>
				<a href="<?php echo site_url('admin/organization/titles/delete/'.$titles->id); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-danger"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:title_name'); ?></div>
		<?php if($titles->title_name){ ?>
		<div class="entry-detail-value"><?php echo $titles->title_name; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:title_description'); ?></div>
		<?php if($titles->title_description){ ?>
		<div class="entry-detail-value"><?php echo $titles->title_description; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
</div>