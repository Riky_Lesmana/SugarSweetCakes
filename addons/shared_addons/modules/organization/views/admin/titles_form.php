<div class="page-header">
	<h1><?php echo lang('organization:titles:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['title_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['title_name']['input_title']);?> <?php echo $fields['title_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['title_name']['input']; ?>
			
			<?php if( $fields['title_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['title_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['title_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['title_description']['input_title']);?> <?php echo $fields['title_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['title_description']['input']; ?>
			<?php if( $fields['title_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['title_description']['instructions']); ?></small>
			<?php endif; ?>
			
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>