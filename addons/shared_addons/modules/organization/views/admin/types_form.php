<div class="page-header">
	<h1><?php echo lang('organization:types:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['type_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['type_name']['input_title']);?> <?php echo $fields['type_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['type_name']['input']; ?>
			<?php if( $fields['type_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['type_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['type_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['type_slug']['input_title']);?> <?php echo $fields['type_slug']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['type_slug']['input']; ?>
			<?php if( $fields['type_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['type_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['type_level']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['type_level']['input_title']);?> <?php echo $fields['type_level']['required'];?></label>

		<div class="col-sm-1">
			<select class="form-control" id="type_level" name="type_level">
				<?php for($i = 0; $i <= 10; $i++){ ?>
				<option <?php if($fields['type_level']['value'] == $i){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			
			<?php if( $fields['type_level']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['type_level']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['type_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['type_description']['input_title']);?> <?php echo $fields['type_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['type_description']['input']; ?>
			<?php if( $fields['type_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['type_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>