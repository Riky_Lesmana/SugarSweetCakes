<div class="page-header">
	<h1><?php echo lang('organization:units:plural'); ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($units['total'] > 0): ?>

<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>No</th>
			<th><?php echo lang('organization:unit_name'); ?></th>
			<th><?php echo lang('organization:unit_slug'); ?></th>
			<th><?php echo lang('organization:unit_type'); ?></th>
			<th><?php echo lang('organization:count_member'); ?></th>
			<th width="280"></th>
		</tr>
	</thead>
	<tbody>
		<?php $count = 1; ?>
		<?php foreach ($units['entries'] as $units_entry): ?>
		<tr>
			<td><?php echo $count++; ?></td>
			<td>
			<?php 
			if($units_entry['unit_abbrevation'] != NULL AND $units_entry['unit_abbrevation'] != ''){
				$units_entry['unit_name'] .= ' ('.$units_entry['unit_abbrevation'].')';
			}
			echo $units_entry['unit_name'];
			?>
			</td>
			<td><?php echo $units_entry['unit_slug']; ?></td>
			<td><?php echo $units_entry['type_name']; ?></td>

			<?php $members = $this->organization->get_membership_by_unit($units_entry['id']); ?>
			<td>
			<?php echo $members['total']; ?>
			<?php
			if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
				echo '('.lang('organization:you').')';
			}
			?>
			</td>
			
			<td class="actions">
			<?php 
			if(group_has_role('organization', 'view_all_units')){
				echo anchor('admin/organization/units/view/' . $units_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
			}elseif(group_has_role('organization', 'view_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/view/' . $units_entry['id'], lang('global:view'), 'class="btn btn-xs btn-info view"');
				}
			}
			?>
			<?php 
			if(group_has_role('organization', 'edit_all_units')){
				echo anchor('admin/organization/units/edit/' . $units_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
			}elseif(group_has_role('organization', 'edit_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/edit/' . $units_entry['id'], lang('global:edit'), 'class="btn btn-xs btn-info edit"');
				}
			}
			?>
			<?php
			if(group_has_role('organization', 'view_all_memberships')){
				echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular'), 'class="btn btn-xs btn-info view"');
			}elseif(group_has_role('organization', 'view_own_memberships')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/memberships/index/' . $units_entry['id'], lang('organization:memberships:singular'), 'class="btn btn-xs btn-info view"');
				}
			}
			?>
			<?php 
			if(group_has_role('organization', 'delete_all_units')){
				echo anchor('admin/organization/units/delete/' . $units_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
			}elseif(group_has_role('organization', 'delete_own_units')){
				if($this->organization->is_member($this->current_user->id, $units_entry['id'])){
					echo anchor('admin/organization/units/delete/' . $units_entry['id'], lang('global:delete'), array('class' => 'confirm btn btn-xs btn-danger delete'));
				}
			}
			?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>

<?php echo $units['pagination']; ?>

<?php else: ?>
<div class="well"><?php echo lang('organization:units:no_entry'); ?></div>
<?php endif;?>