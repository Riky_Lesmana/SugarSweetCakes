<h2><?php echo lang('organization:memberships:view'); ?></h2>

<p>
	<strong>ID:</strong>&nbsp;
	<?php echo $memberships->id; ?>
</p>

<p>
	<strong><?php echo lang('organization:membership_unit'); ?>:</strong>&nbsp;
	<?php if($memberships->membership_unit){ ?>
	<?php echo $memberships->membership_unit; ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:membership_user'); ?>:</strong>&nbsp;
	<?php if($memberships->membership_user){ ?>
	<?php echo $memberships->membership_user; ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:membership_title'); ?>:</strong>&nbsp;
	<?php if($memberships->membership_title){ ?>
	<?php echo $memberships->membership_title; ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:membership_is_head'); ?>:</strong>&nbsp;
	<?php if($memberships->membership_is_head){ ?>
	<?php echo $memberships->membership_is_head; ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:created'); ?>:</strong>&nbsp;
	<?php if($memberships->created){ ?>
	<?php echo format_date($memberships->created, 'd-m-Y G:i'); ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:updated'); ?>:</strong>&nbsp;
	<?php if($memberships->updated){ ?>
	<?php echo format_date($memberships->updated, 'd-m-Y G:i'); ?>
	<?php }else{ ?>
	-
	<?php } ?>
</p>

<p>
	<strong><?php echo lang('organization:created_by'); ?>:</strong>&nbsp;
	<?php echo user_displayname($memberships->created_by, true); ?>
</p>

<div class="float-right buttons">
	<a href="<?php echo site_url('organization/memberships/index'); ?>" class="btn btn-gray">&laquo; <?php echo lang('organization:back') ?></a>

	<?php if(group_has_role('organization', 'edit_all_memberships')){ ?>
		<a href="<?php echo site_url('organization/memberships/edit/'.$memberships->id); ?>" class="btn btn-color"><?php echo lang('global:edit') ?></a>
	<?php }elseif(group_has_role('organization', 'edit_own_memberships')){ ?>
		<?php if($memberships->created_by_user_id == $this->current_user->id){ ?>
			<a href="<?php echo site_url('organization/memberships/edit/'.$memberships->id); ?>" class="btn btn-color"><?php echo lang('global:edit') ?></a>
		<?php } ?>
	<?php } ?>

	<?php if(group_has_role('organization', 'delete_all_memberships')){ ?>
		<a href="<?php echo site_url('organization/memberships/delete/'.$memberships->id); ?>" class="confirm btn" onclick="return confirm('<?php echo lang('organization:confirm_delete') ?>')"><?php echo lang('global:delete') ?></a>
	<?php }elseif(group_has_role('organization', 'delete_own_memberships')){ ?>
		<?php if($memberships->created_by_user_id == $this->current_user->id){ ?>
			<a href="<?php echo site_url('organization/memberships/delete/'.$memberships->id); ?>" class="confirm btn" onclick="return confirm('<?php echo lang('organization:confirm_delete') ?>')"><?php echo lang('global:delete') ?></a>
		<?php } ?>
	<?php } ?>
</div>