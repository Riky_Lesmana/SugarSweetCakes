<h2><?php echo lang('organization:memberships:'.$mode); ?></h2>

<?php if (validation_errors()): ?>
<div class="alert alert-error animated fadeIn">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<?php echo validation_errors(); ?>
</div>
<?php endif; ?>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form_inputs">

	<label for="<?php echo $fields['membership_unit']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_unit']['input_title']);?> <?php echo $fields['membership_unit']['required'];?></label>
	<div class="input">
		<?php if($mode == 'edit'){ ?>

		<?php echo $fields['membership_unit']['entry_value']->unit_name; ?>
		<input type="hidden" name="<?php echo $fields['membership_unit']['input_slug'];?>" value="<?php echo $fields['membership_unit']['value'];?>" />

		<?php }else{ ?>

		<?php echo $unit->unit_name; ?>
		<input type="hidden" name="<?php echo $fields['membership_unit']['input_slug'];?>" value="<?php echo $unit->id; ?>" />

		<?php } ?>
	</div>
	
	<?php if( $fields['membership_unit']['instructions'] != '' ): ?>
		<span class="help-block"><?php echo $this->fields->translate_label($fields['membership_unit']['instructions']); ?></span>
	<?php endif; ?>

	<label for="<?php echo $fields['membership_user']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_user']['input_title']);?> <?php echo $fields['membership_user']['required'];?></label>
	<div class="input">
	
		<?php if($mode == 'edit'){ ?>

		<?php echo user_displayname($fields['membership_user']['value'], true); ?>
		<input type="hidden" name="<?php echo $fields['membership_user']['input_slug']; ?>" value="<?php echo $fields['membership_user']['value']; ?>" />

		<?php }else{ ?>

		<?php echo $fields['membership_user']['input']; ?>

		<?php } ?>

	</div>

	<?php if( $fields['membership_user']['instructions'] != '' ): ?>
		<span class="help-block"><?php echo $this->fields->translate_label($fields['membership_user']['instructions']); ?></span>
	<?php endif; ?>

	<label for="<?php echo $fields['membership_title']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_title']['input_title']);?> <?php echo $fields['membership_title']['required'];?></label>
	<div class="input"><?php echo $fields['membership_title']['input']; ?></div>
	<?php if( $fields['membership_title']['instructions'] != '' ): ?>
		<span class="help-block"><?php echo $this->fields->translate_label($fields['membership_title']['instructions']); ?></span>
	<?php endif; ?>

	<label for="<?php echo $fields['membership_is_head']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['membership_is_head']['input_title']);?> <?php echo $fields['membership_is_head']['required'];?></label>
	<div class="input"><?php echo $fields['membership_is_head']['input']; ?></div>
	<?php if( $fields['membership_is_head']['instructions'] != '' ): ?>
		<span class="help-block"><?php echo $this->fields->translate_label($fields['membership_is_head']['instructions']); ?></span>
	<?php endif; ?>


</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>
<input type="hidden" value="<?php echo $return;?>" name="return" />

<div class="form-actions">
	<button type="submit" name="btnAction" value="save" class="btn btn-color"><span><?php echo lang('buttons:save'); ?></span></button>
	<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-gray"><?php echo lang('buttons:cancel'); ?></a>
</div>

<?php echo form_close();?>