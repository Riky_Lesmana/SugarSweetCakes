<div class="page-header">
	<h1><?php echo lang('organization:units:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('organization/units/index'); ?>" class="btn btn-default btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>

		<?php if(group_has_role('organization', 'edit_all_units')){ ?>
			<a href="<?php echo site_url('organization/units/edit/'.$units->id); ?>" class="btn btn-default btn-sm btn-yellow">
				<i class="icon-edit"></i>
				<?php echo lang('global:edit') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'edit_own_units')){ ?>
			<?php if($this->organization->is_member($this->current_user->id, $units->id)){ ?>
				<a href="<?php echo site_url('organization/units/edit/'.$units->id); ?>" class="btn btn-default btn-sm btn-yellow">
					<i class="icon-edit"></i>
					<?php echo lang('global:edit') ?>
				</a>
			<?php } ?>
		<?php } ?>

		<?php if(group_has_role('organization', 'delete_all_units')){ ?>
			<a href="<?php echo site_url('organization/units/delete/'.$units->id); ?>" class="confirm btn btn-sm btn-danger">
				<i class="icon-trash"></i>
				<?php echo lang('global:delete') ?>
			</a>
		<?php }elseif(group_has_role('organization', 'delete_own_units')){ ?>
			<?php if($this->organization->is_member($this->current_user->id, $units->id)){ ?>
				<a href="<?php echo site_url('organization/units/delete/'.$units->id); ?>" class="confirm btn btn-sm btn-danger">
					<i class="icon-trash"></i>
					<?php echo lang('global:delete') ?>
				</a>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_name'); ?></div>
		<?php if($units->unit_name){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_name; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_abbrevation'); ?></div>
		<?php if($units->unit_abbrevation){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_abbrevation; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
	
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_slug'); ?></div>
		<?php if($units->unit_slug){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_slug; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_description'); ?></div>
		<?php if($units->unit_description){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_description; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_type'); ?></div>
		<?php if($units->unit_type){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_type->type_name; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_sort_order'); ?></div>
		<?php if($units->unit_sort_order){ ?>
		<div class="entry-detail-value"><?php echo $units->unit_sort_order; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</li>

	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('organization:unit_parents'); ?></div>
		<?php if($units->unit_parents){ ?>
		<?php
		$parent_str_arr = array();
		foreach($units->unit_parents as $unit_parent){
			$parent_str_arr[] = '<a href="'.site_url('organization/units/view/'.$unit_parent['id']).'">'.$unit_parent['unit_name'].'</a>';
		}
		?>
		<div class="entry-detail-value"><?php echo implode(', ', $parent_str_arr); ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
</div>

<section class="item">

	<div class="content">

	<div class="form_inputs">

	<ul>
		
	</ul>

	</div>

	<div class="float-right buttons">
		
	</div>

	</div>
</section>