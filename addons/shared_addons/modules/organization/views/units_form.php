<div class="page-header">
	<h1><?php echo lang('organization:units:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_name']['input_title']);?> <?php echo $fields['unit_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['unit_name']['input']; ?>
			
			<?php if( $fields['unit_name']['instructions'] != '' ): ?>
			<br /><small><?php echo $this->fields->translate_label($fields['unit_name']['instructions']); ?></small>
		<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_abbrevation']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_abbrevation']['input_title']);?> <?php echo $fields['unit_abbrevation']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['unit_abbrevation']['input']; ?>
			
			<?php if( $fields['unit_abbrevation']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['unit_abbrevation']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_slug']['input_title']);?> <?php echo $fields['unit_slug']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['unit_slug']['input']; ?>
			
			<?php if( $fields['unit_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['unit_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_description']['input_title']);?> <?php echo $fields['unit_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['unit_description']['input']; ?>
			
			<?php if( $fields['unit_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['unit_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_type']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_type']['input_title']);?> <?php echo $fields['unit_type']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['unit_type']['input']; ?>
			
			<?php if( $fields['unit_type']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['unit_type']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_sort_order']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_sort_order']['input_title']);?> <?php echo $fields['unit_sort_order']['required'];?></label>

		<div class="col-sm-2">
			<select class="form-control" id="unit_sort_order" name="unit_sort_order">
				<?php for($i = -20; $i <= 20; $i++){ ?>
				<option <?php if($i == 0){echo 'selected';} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
				<?php } ?>
			</select>
			
			<?php if( $fields['unit_sort_order']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['unit_sort_order']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['unit_parents']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['unit_parents']['input_title']);?> <?php echo $fields['unit_parents']['required'];?>		</label>

		<div id="unit_parents_container" class="col-sm-10">
			<p id="unit-parents" class="form-control-static"><?php echo lang('organization:loading'); ?></p>
		</div>
		
		<script>
			$(document).ready(function(){
				// make get request
				var type_id = $('#unit_type').val();
				$("#unit-parents").load("<?php echo site_url('organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id, function(){
					//$("#unit_parents").html('Loading..');
				});
			});
			
			$('#unit_type').change(function(){
				var type_id = $('#unit_type').val();
				$("#unit-parents").load("<?php echo site_url('organization/units/ajax_unit_parents_checkbox') ?>" + '/' + type_id, function(){
					//$("#unit_parents").html('Loading..');
				});
			});
		</script>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>