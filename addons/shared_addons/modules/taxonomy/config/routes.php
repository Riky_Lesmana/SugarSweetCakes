<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['taxonomy/admin/taxonomies(:any)'] = 'admin_taxonomies$1';
$route['taxonomy/admin/vocabularies(:any)'] = 'admin_vocabularies$1';
$route['taxonomy/admin/entries(:any)'] = 'admin_entries$1';
