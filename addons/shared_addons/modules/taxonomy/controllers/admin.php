<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy Module
 *
 * Add general taxonomy functionality
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/taxonomy/taxonomies/index');
    }

}