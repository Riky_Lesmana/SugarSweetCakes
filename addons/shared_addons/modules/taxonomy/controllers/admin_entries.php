<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy Module
 *
 * Add general taxonomy functionality
 *
 */
class Admin_entries extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'entries';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'access_entries_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('taxonomy');
        $this->load->driver('Streams');
    }

    /**
	 * List all Entries using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the entries database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_entries') AND ! group_has_role('taxonomy', 'view_own_entries')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		
		$params = array();
		$params['stream'] = 'entries';
		$params['namespace'] = 'taxonomy';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		
		//$params['order_by'] = 'created';
		//$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		
		//$params['where'] = ''; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 5;
		$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_entries')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// -------------------------------------
		// Send $params to viewer
		// -------------------------------------
		
        $data['params'] = $params;
		
		// -------------------------------------
		// Get stream entries
		// -------------------------------------
		
        $data['entries'] = $this->streams->entries->get_entries($params);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:entries:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('taxonomy:entries:plural'))
			->build('admin/entries_index', $data);
    }
	
	/**
     * Delete a Entries entry
     * 
     * @param   int [$id] The id of Entries to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'delete_all_entries') AND ! group_has_role('taxonomy', 'delete_own_entries')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('taxonomy', 'delete_all_entries')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'entries', 'taxonomy', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->streams->entries->delete_entry($id, 'entries', 'taxonomy');
        $this->session->set_flashdata('error', lang('taxonomy:entries:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/taxonomy/entries/index');
    }
	
	/**
     * Insert or update Entries entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_entries($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
		
		$stream = $this->streams->streams->get_stream('entries', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// Can't do nothing if we don't have any fields		
		if ($stream_fields === false)
		{
			return null;
		}
		
		// -------------------------------------
		// Get row, if applicable
		// -------------------------------------
		
		if($method == 'edit'){
			if( ! $row = $this->row_m->get_row($row_id, $stream, false)){
				$this->log_error('invalid_row', 'form');
			}
		}else{
			$row = null;
		}
			
		// -------------------------------------
		// Set Validation Rules
		// -------------------------------------
		
		$this->form_validation->reset_validation();
		$validation_rules = $this->fields->set_rules($stream_fields, $method, array(), true, $row_id);
		
		// You can edit validation rules here by modifying $validation_rules
		// $validation_rules = ...
		
		$this->form_validation->set_rules($validation_rules);

		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, $row, $method);
		
		// You can edit values here by modifying $values
		// $values = ...

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result_id = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result_id = $this->row_m->insert_entry($_POST, $stream_fields, $stream);
				
			}
			else
			{
				$result_id = $this->row_m->update_entry($stream_fields, $stream, $row_id, $this->input->post());
			}
		}
		
		return $result_id;
	}

	// --------------------------------------------------------------------------

}