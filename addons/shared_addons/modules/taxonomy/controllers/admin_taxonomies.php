<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy Module
 *
 * Add general taxonomy functionality
 *
 */
class Admin_taxonomies extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'taxonomies';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'access_taxonomies_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('taxonomy');
        $this->load->driver('Streams');
		
		$this->load->model('taxonomy/vocabularies_m');
    }

    /**
	 * List all Taxonomies using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the taxonomies database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_taxonomies') AND ! group_has_role('taxonomy', 'view_own_taxonomies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		
		$params = array();
		$params['stream'] = 'taxonomies';
		$params['namespace'] = 'taxonomy';
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		
		$params['order_by'] = 'taxonomy_name';
		
		// -------------------------------------
		// Users
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_taxonomies')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// -------------------------------------
		// Send $params to viewer
		// -------------------------------------
		
        $data['params'] = $params;
		
		// -------------------------------------
		// Get stream entries
		// -------------------------------------
		
        $data['taxonomies'] = $this->streams->entries->get_entries($params);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:taxonomies:plural'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('taxonomy:taxonomies:plural'))
			->build('admin/taxonomies_index', $data);
    }
	
	/**
     * Display one Taxonomies
     *
     * We are using the Streams API to grab
     * data from the taxonomies database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_taxonomies') AND ! group_has_role('taxonomy', 'view_own_taxonomies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get our entry. We are simply specifying
        // the stream/namespace.
		// -------------------------------------
		
        $data['taxonomies'] = $this->streams->entries->get_entry($id, 'taxonomies', 'taxonomy', true, true);
		
		// -------------------------------------
		// Check view all/own permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_taxonomies')){
			if($data['taxonomies']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:taxonomies:view'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('taxonomy:taxonomies:plural'), '/admin/taxonomy/taxonomies/index')
			->set_breadcrumb(lang('taxonomy:taxonomies:view'))
			->build('admin/taxonomies_entry', $data);
    }
	
	/**
     * Create a new Taxonomies entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'create_taxonomies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
        // -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_taxonomies('new')){	
				$this->session->set_flashdata('success', lang('taxonomy:taxonomies:submit_success'));				
				redirect('admin/taxonomy/taxonomies/index');
			}else{
				$data['messages']['error'] = lang('taxonomy:taxonomies:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('taxonomies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, null, 'new');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, null, 'new');
		$data['mode'] = 'new';
		$data['return'] = 'admin/taxonomy/taxonomies/index';
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:taxonomies:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('taxonomy:taxonomies:plural'), '/admin/taxonomy/taxonomies/index')
			->set_breadcrumb(lang('taxonomy:taxonomies:new'))
			->build('admin/taxonomies_form', $data);
    }
	
	/**
     * Edit a Taxonomies entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Taxonomies to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'edit_all_taxonomies') AND ! group_has_role('taxonomy', 'edit_own_taxonomies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('taxonomy', 'edit_all_taxonomies')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'taxonomies', 'taxonomy', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_taxonomies('edit', $id)){	
				$this->session->set_flashdata('success', lang('taxonomy:taxonomies:submit_success'));				
				redirect('admin/taxonomy/taxonomies/index');
			}else{
				$data['messages']['error'] = lang('taxonomy:taxonomies:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('taxonomies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		if( ! $row = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$values = $this->fields->set_values($stream_fields, $row, 'edit');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, $row, 'edit');
		$data['mode'] = 'edit';
		$data['return'] = 'admin/taxonomy/taxonomies/index';
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:taxonomies:edit'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb(lang('taxonomy:taxonomies:plural'), '/admin/taxonomy/taxonomies/index')
			->set_breadcrumb(lang('taxonomy:taxonomies:view'), '/admin/taxonomy/taxonomies/view/'.$id)
			->set_breadcrumb(lang('taxonomy:taxonomies:edit'))
			->build('admin/taxonomies_form', $data);
    }
	
	/**
     * Delete a Taxonomies entry
     * 
     * @param   int [$id] The id of Taxonomies to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'delete_all_taxonomies') AND ! group_has_role('taxonomy', 'delete_own_taxonomies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('taxonomy', 'delete_all_taxonomies')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'taxonomies', 'taxonomy', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        // Delete vocabularies under
		$vocabularies = $this->vocabularies_m->get_vocabularies_by_taxonomy_id($id);
		foreach($vocabularies['entries'] as $vocabulary){
			$this->vocabularies_m->delete_vocabularies_recursive($vocabulary['id']);
		}
		
		// Delete taxonomy
		$this->streams->entries->delete_entry($id, 'taxonomies', 'taxonomy');
		
        $this->session->set_flashdata('error', lang('taxonomy:taxonomies:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/taxonomy/taxonomies/index');
    }
	
	/**
     * Insert or update Taxonomies entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_taxonomies($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
		
		$stream = $this->streams->streams->get_stream('taxonomies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// Can't do nothing if we don't have any fields		
		if ($stream_fields === false)
		{
			return null;
		}
		
		// -------------------------------------
		// Get row, if applicable
		// -------------------------------------
		
		if($method == 'edit'){
			if( ! $row = $this->row_m->get_row($row_id, $stream, false)){
				$this->log_error('invalid_row', 'form');
			}
		}else{
			$row = null;
		}
			
		// -------------------------------------
		// Set Validation Rules
		// -------------------------------------
		
		$this->form_validation->reset_validation();
		$validation_rules = $this->fields->set_rules($stream_fields, $method, array(), true, $row_id);
		
		// You can edit validation rules here by modifying $validation_rules
		// $validation_rules = ...
		
		$this->form_validation->set_rules($validation_rules);

		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, $row, $method);
		
		// You can edit values here by modifying $values
		// $values = ...

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result_id = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result_id = $this->row_m->insert_entry($_POST, $stream_fields, $stream);
				
			}
			else
			{
				$result_id = $this->row_m->update_entry($stream_fields, $stream, $row_id, $this->input->post());
			}
		}
		
		return $result_id;
	}

	// --------------------------------------------------------------------------

}