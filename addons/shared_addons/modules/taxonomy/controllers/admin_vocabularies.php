<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Taxonomy Module
 *
 * Add general taxonomy functionality
 *
 */
class Admin_vocabularies extends Admin_Controller
{
	// -------------------------------------
    // This will set the active section tab
	// -------------------------------------
	
    protected $section = 'taxonomies';

    public function __construct()
    {
        parent::__construct();

		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'access_vocabularies_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('taxonomy');
        $this->load->driver('Streams');
		
		$this->load->model('taxonomy/taxonomies_m');
		$this->load->model('taxonomy/vocabularies_m');
    }

    /**
	 * List all Vocabularies using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the vocabularies database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index($taxonomy_id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'view_all_vocabularies') AND ! group_has_role('taxonomy', 'view_own_vocabularies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get taxonomy details
		// -------------------------------------
		
		$taxonomies = $this->taxonomies_m->get_taxonomy_by_id($taxonomy_id);
		$data['taxonomy'] = $taxonomies['entries'][0];
		
        // -------------------------------------
		// Get stream entries
		// -------------------------------------
		
		$root_vocabularies = $this->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy_id, NULL);
		$vocabularies = $this->_build_serial_tree($taxonomy_id, array(), $root_vocabularies);
		
		$data['vocabularies']['total'] = count($vocabularies);
		$data['vocabularies']['entries'] = $this->_build_serial_tree($taxonomy_id, array(), $root_vocabularies);

		// -------------------------------------
        // Build the page. See views/admin/index.php
        // for the view code.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:vocabularies:plural').' '.lang('taxonomy:of').' "'.$data['taxonomy']['taxonomy_name'].'"')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb($data['taxonomy']['taxonomy_name'], '/admin/taxonomy/taxonomies/index')
			->set_breadcrumb(lang('taxonomy:vocabularies:plural'))
			->build('admin/vocabularies_index', $data);
    }
	
	private function _build_serial_tree($taxonomy_id, $serial_tree, $vocabularies, $level = 0)
	{
		foreach($vocabularies['entries'] as $vocabulary){
			$prefix = '';
			for($i = 0; $i < $level; $i++){
				$prefix .= '-- ';
			}
			
			$vocabulary['vocabulary_name'] = $prefix.$vocabulary['vocabulary_name'];
		
			$serial_tree[] = $vocabulary;
			$children = $this->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy_id, $vocabulary['id']);
			
			if($children['total'] == 0){
				continue;
			}else{
				$serial_tree = $this->_build_serial_tree($taxonomy_id, $serial_tree, $children, $level+1);
			}
		}
		
		return $serial_tree;
	}
	
	/**
     * Create a new Vocabularies entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($taxonomy_id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'create_vocabularies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// -------------------------------------
		// Get taxonomy details
		// -------------------------------------
		
		$taxonomies = $this->taxonomies_m->get_taxonomy_by_id($taxonomy_id);
		$data['taxonomy'] = $taxonomies['entries'][0];
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
        // -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_vocabularies('new')){	
				$this->session->set_flashdata('success', lang('taxonomy:vocabularies:submit_success'));				
				redirect('admin/taxonomy/vocabularies/index/'.$taxonomy_id);
			}else{
				$data['messages']['error'] = lang('taxonomy:vocabularies:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('vocabularies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, null, 'new');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, null, 'new');
		$data['mode'] = 'new';
		$data['return'] = 'admin/taxonomy/vocabularies/index/'.$taxonomy_id;
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Get vocabularies of this taxonomy
		// -------------------------------------
		
		$root_vocabularies = $this->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy_id, NULL);
		$vocabularies['entries'] = $this->_build_serial_tree($taxonomy_id, array(), $root_vocabularies);
		$data['vocabularies']['total'] = count($vocabularies['entries']);
		
		$data['vocabulary_choices'][''] = '----';
		foreach($vocabularies['entries'] as $vocabulary){
			$data['vocabulary_choices'][$vocabulary['id']] = $vocabulary['vocabulary_name'];
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('taxonomy:vocabularies:new'))
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb($data['taxonomy']['taxonomy_name'], '/admin/taxonomy/taxonomies/view/'.$taxonomy_id)
			->set_breadcrumb(lang('taxonomy:vocabularies:plural'), '/admin/taxonomy/vocabularies/index/'.$taxonomy_id)
			->set_breadcrumb(lang('taxonomy:vocabularies:new'))
			->build('admin/vocabularies_form', $data);
    }
	
	/**
     * Edit a Vocabularies entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Vocabularies to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'edit_all_vocabularies') AND ! group_has_role('taxonomy', 'edit_own_vocabularies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Check view all/own permission
		if(! group_has_role('taxonomy', 'edit_all_vocabularies')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'vocabularies', 'taxonomy', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Get taxonomy of this vocabulary
		// -------------------------------------
		
		$taxonomy = $this->taxonomies_m->get_taxonomy_by_vocabulary_id($id);
		$data['taxonomy'] = $taxonomy;
		
		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		
		// -------------------------------------
		// Process POST input
		// -------------------------------------
		
		if($_POST){
			if($this->_update_vocabularies('edit', $id)){	
				$this->session->set_flashdata('success', lang('taxonomy:vocabularies:submit_success'));				
				redirect('admin/taxonomy/vocabularies/index/'.$taxonomy['id']);
			}else{
				$data['messages']['error'] = lang('taxonomy:vocabularies:submit_failure');
			}
		}
		
		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
	
		$stream = $this->streams->streams->get_stream('vocabularies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// -------------------------------------
		// Set Values
		// -------------------------------------
		if( ! $row = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$values = $this->fields->set_values($stream_fields, $row, 'edit');
		
		// -------------------------------------
		// Get our field form elements.
		// -------------------------------------
		
		$data['fields'] = $this->fields->build_fields($stream_fields, $values, $row, 'edit');
		$data['mode'] = 'edit';
		$data['return'] = 'admin/taxonomy/vocabularies/index/'.$taxonomy['id'];
		$data['entry_id'] = $id;
		
		// -------------------------------------
		// Make input_slug as the array key
		// in order to ease form templating.
		// -------------------------------------
		
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// -------------------------------------
		// Get vocabularies of this taxonomy
		// -------------------------------------
		
		$root_vocabularies = $this->vocabularies_m->get_vocabularies_by_taxonomy_and_parent($taxonomy['id'], NULL);
		$vocabularies['entries'] = $this->_build_serial_tree($taxonomy['id'], array(), $root_vocabularies);
		$data['vocabularies']['total'] = count($vocabularies['entries']);
		
		$data['vocabulary_choices'][''] = '----';
		foreach($vocabularies['entries'] as $vocabulary){
			if($vocabulary['id'] != $id){
				$data['vocabulary_choices'][$vocabulary['id']] = $vocabulary['vocabulary_name'];
			}
		}
		
		// -------------------------------------
		// Build the form page.
		// -------------------------------------
		
        $this->template->title(lang('global:edit').' "'.$data['fields']['vocabulary_name']['value'].'"')
			->set_breadcrumb('Home', '/admin')
			->set_breadcrumb($taxonomy['taxonomy_name'], '/admin/taxonomy/vocabularies/index/'.$taxonomy['id'])
			->set_breadcrumb(lang('global:edit').' "'.$data['fields']['vocabulary_name']['value'].'"')
			->build('admin/vocabularies_form', $data);
    }
	
	/**
     * Delete a Vocabularies entry
     * 
     * @param   int [$id] The id of Vocabularies to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// -------------------------------------
		// Check permission
		// -------------------------------------
		
		if(! group_has_role('taxonomy', 'delete_all_vocabularies') AND ! group_has_role('taxonomy', 'delete_own_vocabularies')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('taxonomy', 'delete_all_vocabularies')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'vocabularies', 'taxonomy', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// -------------------------------------
		// Get taxonomy of this vocabulary
		// -------------------------------------
		
		$taxonomy = $this->taxonomies_m->get_taxonomy_by_vocabulary_id($id);
		
		// -------------------------------------
		// Delete entry
		// -------------------------------------
		
        $this->vocabularies_m->delete_vocabularies_recursive($id, 'vocabularies', 'taxonomy');
		
        $this->session->set_flashdata('error', lang('taxonomy:vocabularies:deleted'));
 
		// -------------------------------------
		// Redirect
		// -------------------------------------
		
        redirect('admin/taxonomy/vocabularies/index/'.$taxonomy['id']);
    }
	
	/**
     * Insert or update Vocabularies entry in database
     *
     * @param   string [$method] The method of database update ('new' or 'edit').
	 * @param   int [$row_id] The entry id (if in edit mode).
     * @return	boolean
     */
	private function _update_vocabularies($method, $row_id = null)
 	{
 		// -------------------------------------
		// Load everything we need
		// -------------------------------------
		
		$this->load->library('streams_core/Fields');
		$this->load->helper(array('form', 'url'));
		
 		// -------------------------------------
		// Get Stream Fields
		// -------------------------------------
		
		$stream = $this->streams->streams->get_stream('vocabularies', 'taxonomy');
		$stream_fields = $this->streams_m->get_stream_fields($stream->id);
		
		// Can't do nothing if we don't have any fields		
		if ($stream_fields === false)
		{
			return null;
		}
		
		// -------------------------------------
		// Get row, if applicable
		// -------------------------------------
		
		if($method == 'edit'){
			if( ! $row = $this->row_m->get_row($row_id, $stream, false)){
				$this->log_error('invalid_row', 'form');
			}
		}else{
			$row = null;
		}
			
		// -------------------------------------
		// Set Validation Rules
		// -------------------------------------
		
		$this->form_validation->reset_validation();
		$validation_rules = $this->fields->set_rules($stream_fields, $method, array(), true, $row_id);
		
		// You can edit validation rules here by modifying $validation_rules
		// $validation_rules = ...
		
		$this->form_validation->set_rules($validation_rules);

		// -------------------------------------
		// Set Values
		// -------------------------------------
		$values = $this->fields->set_values($stream_fields, $row, $method);
		
		// You can edit values here by modifying $values
		// $values = ...

		// -------------------------------------
		// Validation
		// -------------------------------------
		
		// Set Error Delimns
		$this->form_validation->set_error_delimiters('<div>', '</div>');
		
		$result_id = false;

		if ($this->form_validation->run() === true)
		{
			if ($method == 'new')
			{
				$result_id = $this->row_m->insert_entry($_POST, $stream_fields, $stream);
				
			}
			else
			{
				$result_id = $this->row_m->update_entry($stream_fields, $stream, $row_id, $this->input->post());
			}
		}
		
		return $result_id;
	}

	// --------------------------------------------------------------------------

}