<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Taxonomy extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Taxonomy',
			'id' => 'Taxonomy',
		);
		$info['description'] = array(
			'en' => 'Add general taxonomy functionality',
			'id' => 'Add general taxonomy functionality',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'structure';
		$info['roles'] = array(
			'access_taxonomies_backend', 'view_all_taxonomies', 'view_own_taxonomies', 'edit_all_taxonomies', 'edit_own_taxonomies', 'delete_all_taxonomies', 'delete_own_taxonomies', 'create_taxonomies',
			'access_vocabularies_backend', 'view_all_vocabularies', 'view_own_vocabularies', 'edit_all_vocabularies', 'edit_own_vocabularies', 'delete_all_vocabularies', 'delete_own_vocabularies', 'create_vocabularies',
			'access_entries_backend', 'view_all_entries', 'view_own_entries', 'edit_all_entries', 'edit_own_entries', 'delete_all_entries', 'delete_own_entries', 'create_entries',
		);
		
		if(group_has_role('taxonomy', 'access_taxonomies_backend')){
			$info['sections']['taxonomies']['name'] = 'taxonomy:taxonomies:plural';
			$info['sections']['taxonomies']['uri'] = 'admin/taxonomy/taxonomies/index';
			
			if(group_has_role('taxonomy', 'create_taxonomies')){
				$info['sections']['taxonomies']['shortcuts']['create'] = array(
					'name' => 'taxonomy:taxonomies:new',
					'uri' => 'admin/taxonomy/taxonomies/create',
					'class' => 'add'
				);
			}
		}
		
		if(group_has_role('taxonomy', 'access_entries_backend')){
			$info['sections']['entries']['name'] = 'taxonomy:entries:plural';
			$info['sections']['entries']['uri'] = 'admin/taxonomy/entries/index';
			
			if(group_has_role('taxonomy', 'create_entries')){
				$info['sections']['entries']['shortcuts']['create'] = array(
					'name' => 'taxonomy:entries:new',
					'uri' => 'admin/taxonomy/entries/create',
					'class' => 'add'
				);
			}
		}
		
		return $info;
    }
	
	/**
	 * Admin menu
	 *
	 * If a module has an admin_menu function, then
	 * we simply run that and allow it to manipulate the
	 * menu array
	 */
	public function admin_menu(&$menu_items){
		
	}

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        // We're using the streams API to
        // do data setup.
        $this->load->driver('Streams');

        $this->load->language('taxonomy/taxonomy');

        // Add streams
        if ( ! $taxonomies_stream_id = $this->streams->streams->add_stream('lang:taxonomy:taxonomies:plural', 'taxonomies', 'taxonomy', 'taxonomy_', null)) return false;
        if ( ! $vocabularies_stream_id = $this->streams->streams->add_stream('lang:taxonomy:vocabularies:plural', 'vocabularies', 'taxonomy', 'taxonomy_', null)) return false;
        if ( ! $entries_stream_id = $this->streams->streams->add_stream('lang:taxonomy:entries:plural', 'entries', 'taxonomy', 'taxonomy_', null)) return false;

        // Add some fields
        $fields = array(
            array(
                'name' => 'lang:taxonomy:taxonomy_name',
                'slug' => 'taxonomy_name',
                'namespace' => 'taxonomy',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'taxonomies',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:taxonomy:taxonomy_slug',
                'slug' => 'taxonomy_slug',
                'namespace' => 'taxonomy',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '-',
					'slug_field' => 'taxonomy_name',
				),
                'assign' => 'taxonomies',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:taxonomy:taxonomy_description',
                'slug' => 'taxonomy_description',
                'namespace' => 'taxonomy',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'taxonomies',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:taxonomy_multiselect',
                'slug' => 'taxonomy_multiselect',
                'namespace' => 'taxonomy',
                'type' => 'choice',
                'extra' => array(
					'choice_data' => "yes : Yes\nno : No",
					'choice_type' => 'dropdown',
					//'default_value' => '',
					//'min_choices' => '',
					//'max_choices' => '',
				),
                'assign' => 'taxonomies',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:vocabulary_name',
                'slug' => 'vocabulary_name',
                'namespace' => 'taxonomy',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'vocabularies',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:taxonomy:vocabulary_slug',
                'slug' => 'vocabulary_slug',
                'namespace' => 'taxonomy',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '-',
					'slug_field' => 'vocabulary_name',
				),
                'assign' => 'vocabularies',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:taxonomy:vocabulary_description',
                'slug' => 'vocabulary_description',
                'namespace' => 'taxonomy',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'vocabularies',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:vocabulary_taxonomy',
                'slug' => 'vocabulary_taxonomy',
                'namespace' => 'taxonomy',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $taxonomies_stream_id,
					'link_uri' => 'admin/taxonomy/taxonomies/view/-id-',
				),
                'assign' => 'vocabularies',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:vocabulary_parent',
                'slug' => 'vocabulary_parent',
                'namespace' => 'taxonomy',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $vocabularies_stream_id,
					'link_uri' => 'admin/taxonomy/vocabularies/view/-id-',
				),
                'assign' => 'vocabularies',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:entry_taxonomy',
                'slug' => 'entry_taxonomy',
                'namespace' => 'taxonomy',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $taxonomies_stream_id,
					'link_uri' => 'admin/taxonomy/taxonomies/view/-id-',
				),
                'assign' => 'entries',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:entry_vocabulary',
                'slug' => 'entry_vocabulary',
                'namespace' => 'taxonomy',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $vocabularies_stream_id,
					'link_uri' => 'admin/taxonomy/vocabularies/view/-id-',
				),
                'assign' => 'entries',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:entry_namespace_slug',
                'slug' => 'entry_namespace_slug',
                'namespace' => 'taxonomy',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'entries',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:entry_stream_slug',
                'slug' => 'entry_stream_slug',
                'namespace' => 'taxonomy',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'entries',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:taxonomy:entry_id',
                'slug' => 'entry_id',
                'namespace' => 'taxonomy',
                'type' => 'integer',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'entries',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
        );

        $this->streams->fields->add_fields($fields);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('taxonomy');

        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}