<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Taxonomy Events Class
 *
 * @category    events
 * @author      Aditya Satrya
 */
class Events_Taxonomy
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
		
		$this->ci->load->model('taxonomy/entries_m');

        Events::register('module_disabled', array($this, 'delete_entries_when_module_disabled'));
		Events::register('user_deleted', array($this, 'delete_entries_when_user_deleted'));
		Events::register('streams_post_delete_entry', array($this, 'delete_entries_when_streams_post_delete_entry'));
    }

    public function delete_entries_when_module_disabled($slug)
	{
		$this->ci->entries_m->delete_entries_by_namespace($slug);
	}
	
	public function delete_entries_when_user_deleted($deleted_ids)
	{
		foreach($deleted_ids as $deleted_id){
			$this->ci->entries_m->delete_entries_by_namespace_and_id('users', $deleted_id);
		}
	}
	
	public function delete_entries_when_streams_post_delete_entry($entry)
	{
		$this->ci->entries_m->delete_entries_by_namespace_slug_id($entry['stream']->stream_namespace, $entry['stream']->stream_slug, $entry['entry_id']);
	}

}
/* End of file events.php */