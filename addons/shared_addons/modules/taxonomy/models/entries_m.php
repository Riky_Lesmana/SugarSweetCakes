<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Vocabularies model
 *
 * @author Aditya Satrya
 */
class Entries_m extends MY_Model {
	
	public function get_entries($taxonomy_id, $namespace, $slug, $entry_id)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'entries';
		$params['namespace'] = 'taxonomy';
		$params['where'] = array(
			$this->db->dbprefix(NULL).'taxonomy_entries.entry_taxonomy = '.$taxonomy_id,
			$this->db->dbprefix(NULL)."taxonomy_entries.entry_namespace_slug = '".$namespace."'",
			$this->db->dbprefix(NULL)."taxonomy_entries.entry_stream_slug = '".$slug."'",
			$this->db->dbprefix(NULL).'taxonomy_entries.entry_id = '.$entry_id,
		);
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_entries_by_vocabulary($taxonomy_id, $namespace, $slug, $vocabulary_id)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'entries';
		$params['namespace'] = 'taxonomy';
		$params['where'] = array(
			$this->db->dbprefix(NULL).'taxonomy_entries.entry_taxonomy = '.$taxonomy_id,
			$this->db->dbprefix(NULL)."taxonomy_entries.entry_namespace_slug = '".$namespace."'",
			$this->db->dbprefix(NULL)."taxonomy_entries.entry_stream_slug = '".$slug."'",
			$this->db->dbprefix(NULL).'taxonomy_entries.entry_vocabulary = '.$vocabulary_id,
		);
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function insert_entry($taxonomy_id, $vocabulary_id, $stream_namespace, $stream_slug, $entry_id)
	{
		$this->load->driver('Streams');
		
		$entry_data = array();
		$entry_data['entry_taxonomy'] = $taxonomy_id;
		$entry_data['entry_vocabulary'] = $vocabulary_id;
		$entry_data['entry_namespace_slug'] = $stream_namespace;
		$entry_data['entry_stream_slug'] = $stream_slug;
		$entry_data['entry_id'] = $entry_id;
		
		$this->streams->entries->insert_entry($entry_data, 'entries', 'taxonomy');
	}
	
	public function delete_entries_by_namespace_slug_id($namespace_slug, $stream_slug, $id)
	{
		$this->db->where('entry_namespace_slug', $namespace_slug);
		$this->db->where('entry_stream_slug', $stream_slug);
		$this->db->where('entry_id', $id);
		$this->db->delete($this->db->dbprefix(NULL).'taxonomy_entries');
	}
	
	public function delete_entries_by_namespace($namespace_slug)
	{
		$this->db->where('entry_namespace_slug', $namespace_slug);
		$this->db->delete($this->db->dbprefix(NULL).'taxonomy_entries');
	}
	
	public function delete_entries_by_namespace_and_id($namespace_slug, $entry_id)
	{
		$this->db->where('entry_namespace_slug', $namespace_slug);
		$this->db->where('entry_id', $entry_id);
		$this->db->delete($this->db->dbprefix(NULL).'taxonomy_entries');
	}
	
	public function delete_entries_by_vocabulary($vocabulary_id)
	{
		$this->db->where('entry_vocabulary', $vocabulary_id);
		$this->db->delete($this->db->dbprefix(NULL).'taxonomy_entries');
	}
}