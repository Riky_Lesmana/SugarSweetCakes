<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Taxonomies model
 *
 * @author Aditya Satrya
 */
class Taxonomies_m extends MY_Model {
	
	public function get_taxonomy_by_slug($slug)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'taxonomies';
		$params['namespace'] = 'taxonomy';
		$params['where'] = "taxonomy_slug = '".$slug."'";
		$params['limit'] = 1;
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_taxonomy_by_id($id)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'taxonomies';
		$params['namespace'] = 'taxonomy';
		$params['where'] = $this->db->dbprefix(NULL)."taxonomy_taxonomies.id = '".$id."'";
		$params['limit'] = 1;
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_taxonomy_by_vocabulary_id($vocabulary_id)
	{
		$this->load->driver('Streams');
		
		/*********************************
		 * Get Vocabulary
		 *********************************/
		
		$params = array();
		$params['stream'] = 'vocabularies';
		$params['namespace'] = 'taxonomy';
		$params['where'] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.id = '.$vocabulary_id;
		
		$vocabulary = $this->streams->entries->get_entries($params);
		
		/*********************************
		 * Get Taxonomy
		 *********************************/
		 
		return $vocabulary['entries'][0]['vocabulary_taxonomy'];
	}
	
}