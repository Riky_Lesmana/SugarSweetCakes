<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Vocabularies model
 *
 * @author Aditya Satrya
 */
class Vocabularies_m extends MY_Model {
	
	public function get_vocabularies_by_taxonomy_id($id)
	{
		$this->load->driver('Streams');
		
		$params = array();
		$params['stream'] = 'vocabularies';
		$params['namespace'] = 'taxonomy';
		$params['where'] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.vocabulary_taxonomy = '.$id;
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_vocabularies_by_parent($parent_id)
	{
		$params = array();
		$params['stream'] = 'vocabularies';
		$params['namespace'] = 'taxonomy';
		$params['order_by'] = 'vocabulary_name';
		$params['sort'] = 'asc';
		$params['where'][] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.vocabulary_parent = '.$parent_id;
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function get_vocabularies_by_taxonomy_and_parent($taxonomy_id, $parent_id = NULL)
	{
		$params = array();
		$params['stream'] = 'vocabularies';
		$params['namespace'] = 'taxonomy';
		$params['order_by'] = 'vocabulary_name';
		$params['sort'] = 'asc';
		$params['where'][] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.vocabulary_taxonomy = '.$taxonomy_id;
		if($parent_id == NULL){
			$params['where'][] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.vocabulary_parent IS NULL';
		}else{
			$params['where'][] = $this->db->dbprefix(NULL).'taxonomy_vocabularies.vocabulary_parent = '.$parent_id;
		}
		
		return $this->streams->entries->get_entries($params);
	}
	
	public function delete_vocabularies_recursive($id)
	{
		// get children
		$children = $this->get_vocabularies_by_parent($id);
		
		if($children['total'] > 0){
			foreach($children['entries'] as $vocabulary){
				$this->delete_vocabularies_recursive($vocabulary['id']);
			}
		}
		
		// delete entries
		$this->entries_m->delete_entries_by_vocabulary($id);
		
		// delete vocabulary
		$this->streams->entries->delete_entry($id, 'vocabularies', 'taxonomy');
	}
	
}