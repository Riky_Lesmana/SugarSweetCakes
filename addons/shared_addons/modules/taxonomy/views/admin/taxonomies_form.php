<div class="page-header">
	<h1><?php echo lang('taxonomy:taxonomies:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['taxonomy_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['taxonomy_name']['input_title']);?> <?php echo $fields['taxonomy_name']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['taxonomy_name']['input']; ?>
			
			<?php if( $fields['taxonomy_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['taxonomy_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['taxonomy_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['taxonomy_slug']['input_title']);?> <?php echo $fields['taxonomy_slug']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['taxonomy_slug']['input']; ?>
			
			<?php if( $fields['taxonomy_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['taxonomy_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['taxonomy_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['taxonomy_description']['input_title']);?> <?php echo $fields['taxonomy_description']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['taxonomy_description']['input']; ?>
			
			<?php if( $fields['taxonomy_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['taxonomy_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['taxonomy_multiselect']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['taxonomy_multiselect']['input_title']);?> <?php echo $fields['taxonomy_multiselect']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['taxonomy_multiselect']['input']; ?>
			
			<?php if( $fields['taxonomy_multiselect']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['taxonomy_multiselect']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry_id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>