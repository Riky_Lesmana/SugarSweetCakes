<div class="page-header">
	<h1><?php echo lang('taxonomy:vocabularies:'.$mode).' '.lang('taxonomy:of').' "'.$taxonomy['taxonomy_name'].'"'; ?></h1>
</div>

<?php echo form_open_multipart(uri_string()); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['vocabulary_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['vocabulary_name']['input_title']);?> <?php echo $fields['vocabulary_name']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['vocabulary_name']['input']; ?>
			
			<?php if( $fields['vocabulary_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['vocabulary_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['vocabulary_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['vocabulary_slug']['input_title']);?> <?php echo $fields['vocabulary_slug']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['vocabulary_slug']['input']; ?>
			
			<?php if( $fields['vocabulary_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['vocabulary_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['vocabulary_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['vocabulary_description']['input_title']);?> <?php echo $fields['vocabulary_description']['required'];?></label>

		<div class="col-xs-10">
			<?php echo $fields['vocabulary_description']['input']; ?>
			
			<?php if( $fields['vocabulary_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['vocabulary_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['vocabulary_taxonomy']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['vocabulary_taxonomy']['input_title']);?> <?php echo $fields['vocabulary_taxonomy']['required'];?></label>

		<div class="col-xs-10">
			<p class="form-control-static"><?php echo $taxonomy['taxonomy_name']; ?></p>
			<?php echo form_hidden('vocabulary_taxonomy', $taxonomy['id']); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $fields['vocabulary_parent']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['vocabulary_parent']['input_title']);?> <?php echo $fields['vocabulary_parent']['required'];?></label>

		<div class="col-xs-10">
			<?php echo form_dropdown('vocabulary_parent', $vocabulary_choices, $fields['vocabulary_parent']['value'], 'class="form-control"'); ?>
			
			<?php if( $fields['vocabulary_parent']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['vocabulary_parent']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry_id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url($return); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>