<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$route['workflow/admin/workflows(:any)'] = 'admin_workflows$1';
$route['workflow/admin/statuses(:any)'] = 'admin_statuses$1';
$route['workflow/admin/transitions(:any)'] = 'admin_transitions$1';
$route['workflow/admin/items(:any)'] = 'admin_items$1';
$route['workflow/workflows(:any)'] = 'workflow_workflows$1';
$route['workflow/statuses(:any)'] = 'workflow_statuses$1';
$route['workflow/transitions(:any)'] = 'workflow_transitions$1';
$route['workflow/items(:any)'] = 'workflow_items$1';
