<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Manage workflow of Streams entry
 *
 */
class Admin extends Admin_Controller
{
    // This controller simply redirect to main section controller
	public function __construct()
    {
        parent::__construct();

        redirect('admin/workflow/workflows/index');
    }

}