<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Manage workflow of Streams entry
 *
 */
class Admin_items extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'items';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('workflow', 'access_items_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('workflow');
        $this->load->driver('Streams');
    }

    /**
	 * List all Items using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the items database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('workflow', 'view_all_items') AND ! group_has_role('workflow', 'view_own_items')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get Items entries.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'items';
		$params['namespace'] = 'workflow';
		$params['limit'] = Settings::get('records_per_page');
		//$params['order_by'] = 'created';
		//$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		//$params['where'] = ''; //string or array
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 4;
		$params['pag_base'] = current_url();
		if(! group_has_role('workflow', 'view_all_items')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
        $data['items'] = $this->streams->entries->get_entries($params);

		// -------------------------------------
		// Get Workflows entries.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		$params['order_by'] = 'workflow_name';
		$params['sort'] = 'asc';
		//$params['where'] = ''; //string or array
		$data['workflows'] = $this->streams->entries->get_entries($params);

		// -------------------------------------
		// Get statuses entries.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'statuses';
		$params['namespace'] = 'workflow';
		$params['order_by'] = 'status_name';
		$params['sort'] = 'asc';
		//$params['where'] = ''; //string or array
		$data['statuses'] = $this->streams->entries->get_entries($params);

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:items:plural'));
		$this->template->build('admin/items_index', $data);
    }
	
	/**
     * Display one Items
     *
     * We are using the Streams API to grab
     * data from the items database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'view_all_items') AND ! group_has_role('workflow', 'view_own_items')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['items'] = $this->streams->entries->get_entry($id, 'items', 'workflow', true);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_items')){
			if($data['items']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
		$attributes = array(
			'stream_slug' => 'items', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['items']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:items:view'));
        $this->template->build('admin/items_entry', $data);
    }
	
	/**
     * Delete a Items entry
     * 
     * @param   int [$id] The id of Items to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'delete_all_items') AND ! group_has_role('workflow', 'delete_own_items')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_items')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'items', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
        $this->streams->entries->delete_entry($id, 'items', 'workflow');
        $this->session->set_flashdata('error', lang('workflow:items:deleted'));
 
        redirect('admin/workflow/items/index');
    }

}