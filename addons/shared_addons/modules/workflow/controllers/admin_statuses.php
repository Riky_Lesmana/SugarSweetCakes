<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Manage workflow of Streams entry
 *
 */
class Admin_statuses extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'workflows';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('workflow', 'access_statuses_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('workflow');
        $this->load->driver('Streams');
    }

    /**
	 * List all Statuses using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the statuses database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index($workflow_id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'view_all_statuses') AND ! group_has_role('workflow', 'view_own_statuses')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Get workflow entry
		if(! $data['workflow_entry'] = $this->streams->entries->get_entry($workflow_id, 'workflows', 'workflow')){
			die();
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'statuses';
		$params['namespace'] = 'workflow';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		$params['order_by'] = 'status_sort_order';
		$params['sort'] = 'asc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		$params['where'] = 'status_workflow = '.$workflow_id; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 4;
		$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		if(! group_has_role('workflow', 'view_all_statuses')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// Get stream entries
        $data['statuses'] = $this->streams->entries->get_entries($params);

		// Get multiple relationship
		/*foreach($data['statuses']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
			$attributes = array(
				'stream_slug' => 'statuses', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['statuses']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:statuses:plural'));
		$this->template->build('admin/statuses_index', $data);
    }
	
	/**
     * Display one Statuses
     *
     * We are using the Streams API to grab
     * data from the statuses database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'view_all_statuses') AND ! group_has_role('workflow', 'view_own_statuses')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['statuses'] = $this->streams->entries->get_entry($id, 'statuses', 'workflow', true);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_statuses')){
			if($data['statuses']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
		$attributes = array(
			'stream_slug' => 'statuses', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['statuses']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:statuses:view'));
        $this->template->build('admin/statuses_entry', $data);
    }
	
	/**
     * Create a new Statuses entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($workflow_id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'create_statuses')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');

		if( ! $data['workflow_entry'] = $this->streams->entries->get_entry($workflow_id, 'workflows', 'workflow')){
			die();
		}
		
        $stream = $this->streams->streams->get_stream('statuses', 'workflow');
		$namespace = 'workflow';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/statuses/index/'.$data['workflow_entry']->id,
            'success_message' => lang('workflow:statuses:submit_success'),
            'failure_message' => lang('workflow:statuses:submit_failure'),
            'title' => 'lang:workflow:statuses:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
		$this->template->title(lang('workflow:workflows:singular').' "'.$data['workflow_entry']->workflow_name.'" | '.lang('workflow:statuses:new'));
        $this->template->build('admin/statuses_form', $data);
    }
	
	/**
     * Edit a Statuses entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Statuses to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'edit_all_statuses') AND ! group_has_role('workflow', 'edit_own_statuses')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_statuses')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'statuses', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
		$stream = $this->streams->streams->get_stream('statuses', 'workflow');
		$namespace = 'workflow';
		$mode = 'edit'; //'new', 'edit'

		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			die();
		}
		if( ! $data['workflow_entry'] = $this->streams->entries->get_entry($entry->status_workflow, 'workflows', 'workflow')){
			die();
		}

		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/statuses/index/'.$data['workflow_entry']->id,
            'success_message' => lang('workflow:statuses:submit_success'),
            'failure_message' => lang('workflow:statuses:submit_failure'),
            'title' => 'lang:workflow:statuses:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('workflow:statuses:edit'));
        $this->template->build('admin/statuses_form', $data);
    }
	
	/**
     * Delete a Statuses entry
     * 
     * @param   int [$id] The id of Statuses to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'delete_all_statuses') AND ! group_has_role('workflow', 'delete_own_statuses')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_statuses')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'statuses', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		if( ! $entry = $this->streams->entries->get_entry($id, 'statuses', 'workflow')){
			die();
		}
		if( ! $workflow_entry = $this->streams->entries->get_entry($entry->status_workflow, 'workflows', 'workflow')){
			die();
		}
		
        $this->streams->entries->delete_entry($id, 'statuses', 'workflow');
        $this->session->set_flashdata('error', lang('workflow:statuses:deleted'));
 
        redirect('admin/workflow/statuses/index/'.$workflow_entry->id);
    }

}