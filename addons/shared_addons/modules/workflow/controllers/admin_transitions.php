<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Manage workflow of Streams entry
 *
 */
class Admin_transitions extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'workflows';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('workflow', 'access_transitions_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('workflow');
        $this->load->driver('Streams');
		$this->load->library('Workflow');
		$this->load->model('groups/group_m');
    }

    /**
	 * List all Transitions using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the transitions database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index($workflow_id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'view_all_transitions') AND ! group_has_role('workflow', 'view_own_transitions')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

		// Get workflow entry
		if(! $data['workflow_entry'] = $this->streams->entries->get_entry($workflow_id, 'workflows', 'workflow')){
			die();
		}

		// -------------------------------------
		// Get Statuses entries.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'statuses';
		$params['namespace'] = 'workflow';
		$params['order_by'] = 'status_sort_order';
		$params['sort'] = 'asc'; //'asc', 'desc', 'random'
		$params['where'] = 'status_workflow = '.$workflow_id; //string or array
		$params['paginate'] = 'no';

        $data['statuses'] = $this->streams->entries->get_entries($params);

		// Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:transitions:plural'));
		$this->template->build('admin/transitions_index', $data);
    }
	
	/**
     * Display one Transitions
     *
     * We are using the Streams API to grab
     * data from the transitions database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'view_all_transitions') AND ! group_has_role('workflow', 'view_own_transitions')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['transitions'] = $this->streams->entries->get_entry($id, 'transitions', 'workflow', true);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_transitions')){
			if($data['transitions']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
		$attributes = array(
			'stream_slug' => 'transitions', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['transitions']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:transitions:view'));
        $this->template->build('admin/transitions_entry', $data);
    }
	
	/**
     * Create a new Transitions entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create($status_from_id, $status_to_id)
    {
		// Check permission
		if(! group_has_role('workflow', 'create_transitions')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');

		if( ! $data['status_from_entry'] = $this->streams->entries->get_entry($status_from_id, 'statuses', 'workflow')){
			die();
		}
		if( ! $data['status_to_entry'] = $this->streams->entries->get_entry($status_to_id, 'statuses', 'workflow')){
			die();
		}
		if( ! $data['workflow_entry'] = $this->streams->entries->get_entry($data['status_from_entry']->status_workflow, 'workflows', 'workflow')){
			die();
		}

		if($_POST){
			$_POST['transition_triggers'] = serialize($this->input->post('transition_triggers'));
		}
		
        $stream = $this->streams->streams->get_stream('transitions', 'workflow');
		$namespace = 'workflow';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/transitions/index/'.$data['workflow_entry']->id,
            'success_message' => lang('workflow:transitions:submit_success'),
            'failure_message' => lang('workflow:transitions:submit_failure'),
            'title' => 'lang:workflow:transitions:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}

		$data['groups'] = $this->group_m->get_all();
		
		// Build the form page.
        $this->template->title(lang('workflow:transitions:new'));
        $this->template->build('admin/transitions_form', $data);
    }
	
	/**
     * Edit a Transitions entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Transitions to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'edit_all_transitions') AND ! group_has_role('workflow', 'edit_own_transitions')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_transitions')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'transitions', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');
		
		$stream = $this->streams->streams->get_stream('transitions', 'workflow');
		$namespace = 'workflow';
		$mode = 'edit'; //'new', 'edit'

		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		if( ! $data['status_from_entry'] = $this->streams->entries->get_entry($entry->transition_from, 'statuses', 'workflow')){
			die();
		}
		if( ! $data['status_to_entry'] = $this->streams->entries->get_entry($entry->transition_to, 'statuses', 'workflow')){
			die();
		}
		if( ! $data['workflow_entry'] = $this->streams->entries->get_entry($data['status_from_entry']->status_workflow, 'workflows', 'workflow')){
			die();
		}
		$data['groups'] = $this->group_m->get_all();

		if($_POST){
			$_POST['transition_triggers'] = serialize($this->input->post('transition_triggers'));
		}

		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/transitions/index/'.$data['workflow_entry']->id,
            'success_message' => lang('workflow:transitions:submit_success'),
            'failure_message' => lang('workflow:transitions:submit_failure'),
            'title' => 'lang:workflow:transitions:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}
		
		// Build the form page.
        $this->template->title(lang('workflow:transitions:edit'));
        $this->template->build('admin/transitions_form', $data);
    }
	
	/**
     * Delete a Transitions entry
     * 
     * @param   int [$id] The id of Transitions to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'delete_all_transitions') AND ! group_has_role('workflow', 'delete_own_transitions')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_transitions')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'transitions', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		if( ! $transition_entry = $this->streams->entries->get_entry($id, 'transitions', 'workflow')){
			die();
		}
		if( ! $status_from_entry = $this->streams->entries->get_entry($transition_entry->transition_from, 'statuses', 'workflow')){
			die();
		}
		if( ! $workflow_entry = $this->streams->entries->get_entry($status_from_entry->status_workflow, 'workflows', 'workflow')){
			die();
		}
		
        $this->streams->entries->delete_entry($id, 'transitions', 'workflow');
        $this->session->set_flashdata('error', lang('workflow:transitions:deleted'));
 
        redirect('admin/workflow/transitions/index/'.$workflow_entry->id);
    }

}