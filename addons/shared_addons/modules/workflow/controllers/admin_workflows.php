<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Workflow Module
 *
 * Manage workflow of Streams entry
 *
 */
class Admin_workflows extends Admin_Controller
{
    // This will set the active section tab
    protected $section = 'workflows';

    public function __construct()
    {
        parent::__construct();

		// Check permission
		if(! group_has_role('workflow', 'access_workflows_backend')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}

        $this->lang->load('workflow');
        $this->load->driver('Streams');
		$this->load->model('workflows_m');
    }

    /**
	 * List all Workflows using Streams CP Driver
     *
     * We are using the Streams API to grab
     * data from the workflows database. It handles
     * pagination as well.
     *
     * @return	void
     */
    public function index()
    {
		// Check permission
		if(! group_has_role('workflow', 'view_all_workflows') AND ! group_has_role('workflow', 'view_own_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
        // -------------------------------------
		// Get our entries. We are simply specifying
		// the stream/namespace, and then setting the pagination up.
		// -------------------------------------
		$params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		
		// -------------------------------------
		// Limit and Offset
		// -------------------------------------
		$params['limit'] = Settings::get('records_per_page');
		//$params['offset'] = 0;
		
		// -------------------------------------
		// Ordering and Sorting
		// -------------------------------------
		//$params['order_by'] = 'created';
		//$params['sort'] = 'desc'; //'asc', 'desc', 'random'
		
		// -------------------------------------
		// Get the day.
		// For calendars and stuff
		// -------------------------------------
		//$params['date_by'] = 'created';
		//$params['year'] = 0;
		//$params['month'] = 0;
		//$params['day'] = 0;
		//$params['show_upcoming'] = 'yes';
		//$params['show_past'] = 'yes';
		
		// -------------------------------------
		// Where, Include, Disable
		// -------------------------------------
		//$params['where'] = ''; //string or array
		//$params['exclude'] = ''; //IDs of entries to exclude separated by a pipe character (|). Ex: 1|4|7
		//$params['exclude_called'] = 'no'; 
		//$params['disable'] = ''; //field name to exclude separated by a pipe character (|)
		
		// -------------------------------------
		// Pagination
		// -------------------------------------
		$params['paginate'] = 'yes';
		$params['pag_segment'] = 4;
		$params['pag_base'] = current_url();
		
		// -------------------------------------
		// Users
		// -------------------------------------
		if(! group_has_role('workflow', 'view_all_workflows')){
			$params['restrict_user'] = 'current'; //'current', user id, username
		}
		
		// Get stream entries
        $data['workflows'] = $this->streams->entries->get_entries($params);

		// Get multiple relationship
		/*foreach($data['workflows']['entries'] as $key => $entry){
			$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
			$attributes = array(
				'stream_slug' => 'workflows', // The stream of the related stream.
				'row_id' => $entry['id'], // The ID of the current entry row.
			);
			$data['workflows']['entries'][$key]['%field_slug%'] = $this->type->types->multiple->plugin_override($field, $attributes);
		}*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:workflows:plural'));
		$this->template->build('admin/workflows_index', $data);
    }
	
	/**
     * Display one Workflows
     *
     * We are using the Streams API to grab
     * data from the workflows database.
     *
     * @return  void
     */
    public function view($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'view_all_workflows') AND ! group_has_role('workflow', 'view_own_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Get our entry. We are simply specifying
        // the stream/namespace.
        $data['workflows'] = $this->streams->entries->get_entry($id, 'workflows', 'workflow', true);
		
		// Check view all/own permission
		if(! group_has_role('workflow', 'view_all_workflows')){
			if($data['workflows']->created_by != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}

		// Get multiple relationship
		/*$field = $this->fields_m->get_field_by_slug('%field_slug%', 'workflow');
		$attributes = array(
			'stream_slug' => 'workflows', // The stream of the related stream.
			'row_id' => $id, // The ID of the current entry row.
		);
		$data['workflows']->%field_slug% = $this->type->types->multiple->plugin_override($field, $attributes);*/

        // Build the page. See views/admin/index.php
        // for the view code.
        $this->template->title(lang('workflow:workflows:view'));
        $this->template->build('admin/workflows_entry', $data);
    }
	
	/**
     * Create a new Workflows entry
     *
     * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @return	void
     */
    public function create()
    {
		// Check permission
		if(! group_has_role('workflow', 'create_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');

		if($_POST){
			$namespace_stream = explode("|", $this->input->post('namespace_stream'));
			if(isset($namespace_stream[0]) AND isset($namespace_stream[1])){
				$_POST['workflow_namespace_slug'] = $namespace_stream[0];
				$_POST['workflow_stream_slug'] = $namespace_stream[1];
			}
		}
		
        $stream = $this->streams->streams->get_stream('workflows', 'workflow');
		$namespace = 'workflow';
		$mode = 'new'; //'new', 'edit'
		$entry = null;
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/workflows/index',
            'success_message' => lang('workflow:workflows:submit_success'),
            'failure_message' => lang('workflow:workflows:submit_failure'),
            'title' => 'lang:workflow:workflows:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}

		// Get streams
		$data['streams'] = $this->workflows_m->get_all_streams();
		
		// Build the form page.
        $this->template->title(lang('workflow:workflows:new'));
        $this->template->build('admin/workflows_form', $data);
    }
	
	/**
     * Edit a Workflows entry
     *
     * We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
	 * We are building entry form manually using the fields API
     * and displaying the output in a custom view file.
     *
     * @param   int [$id] The id of the Workflows to the be deleted.
     * @return	void
     */
    public function edit($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'edit_all_workflows') AND ! group_has_role('workflow', 'edit_own_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_workflows')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'workflows', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		// Load everything we need
		$this->load->library('streams_core/Fields');

		if($_POST){
			$namespace_stream = explode("|", $this->input->post('namespace_stream'));
			if(isset($namespace_stream[0]) AND isset($namespace_stream[1])){
				$_POST['workflow_namespace_slug'] = $namespace_stream[0];
				$_POST['workflow_stream_slug'] = $namespace_stream[1];
			}
		}
		
		$stream = $this->streams->streams->get_stream('workflows', 'workflow');
		$namespace = 'workflow';
		$mode = 'edit'; //'new', 'edit'
		if( ! $entry = $this->row_m->get_row($id, $stream, false)){
			$this->log_error('invalid_row', 'form');
		}
		$plugin = false;
		$recaptcha = false;
		$extra = array(
            'return' => 'admin/workflow/workflows/index',
            'success_message' => lang('workflow:workflows:submit_success'),
            'failure_message' => lang('workflow:workflows:submit_failure'),
            'title' => 'lang:workflow:workflows:edit',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$defaults = array();
		
		// Get our field form elements.
		$data['fields'] = $this->fields->build_form($stream, $mode, $entry, $plugin, $recaptcha, $skips, $extra, $defaults);
		$data['stream'] = $stream;
		$data['mode'] = $mode;
		$data['return'] = $extra['return'];
		$data['entry'] = $entry;
		
		// Make input_slug as the array key
		// in order to ease form templating.
		if(is_array($data['fields'])){
			$fields_arr = array();
			foreach($data['fields'] as $field){
				$fields_arr[$field['input_slug']] = $field;
			}
			$data['fields'] = $fields_arr;
		}

		// Get streams
		$data['streams'] = $this->workflows_m->get_all_streams();
		
		// Build the form page.
        $this->template->title(lang('workflow:workflows:edit'));
        $this->template->build('admin/workflows_form', $data);
    }
	
	/**
     * Edit a Workflows entry
     *
     * We're using the entry_form function
     * to generate the edit form. We're passing the
     * id of the entry, which will allow entry_form to
     * repopulate the data from the database.
     *
     * @param   int [$id] The id of the Workflows to the be deleted.
     * @return	void
     */
    public function edit_alt($id = 0)
    {
        // Check permission
		if(! group_has_role('workflow', 'edit_all_workflows') AND ! group_has_role('workflow', 'edit_own_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'edit_all_workflows')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'workflows', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
		$streams = 'workflows';
		$namespace = 'workflow';
		$mode = 'edit'; //'new', 'edit'
		$entry_id = $id;
		$view_override = true;
		$extra = array(
            'return' => 'admin/workflow/workflows/index',
            'success_message' => lang('workflow:workflows:submit_success'),
            'failure_message' => lang('workflow:workflows:submit_failure'),
            'title' => 'lang:workflow:workflows:new',
			//'required' => '<span>*</span>',
			//'email_notification' => false,
        );
		$skips = array();
		$tabs = false;
		$hidden = array();
		$defaults = array();
		
		$this->streams->cp->entry_form($streams, $namespace, $mode, $entry_id, $view_override, $extra, $skips, $tabs, $hidden, $defaults);
    }
    
    /**
     * Delete a Workflows entry
     * 
     * @param   int [$id] The id of Workflows to be deleted
     * @return  void
     */
    public function delete($id = 0)
    {
		// Check permission
		if(! group_has_role('workflow', 'delete_all_workflows') AND ! group_has_role('workflow', 'delete_own_workflows')){
			$this->session->set_flashdata('error', lang('cp:access_denied'));
			redirect('admin');
		}
		// Check view all/own permission
		if(! group_has_role('workflow', 'delete_all_workflows')){
			$created_by_user_id = $this->streams->entries->get_entry($id, 'workflows', 'workflow', true)->created_by_user_id;
			if($created_by_user_id != $this->current_user->id){
				$this->session->set_flashdata('error', lang('cp:access_denied'));
				redirect('admin');
			}
		}
		
        $this->streams->entries->delete_entry($id, 'workflows', 'workflow');
        $this->session->set_flashdata('error', lang('workflow:workflows:deleted'));
 
        redirect('admin/workflow/workflows/index');
    }

}