<?php defined('BASEPATH') or exit('No direct script access allowed');

class Module_Workflow extends Module
{
    public $version = '1.0';

    public function info()
    {
        $info = array();
		$info['name'] = array(
			'en' => 'Workflow',
			'id' => 'Workflow',
		);
		$info['description'] = array(
			'en' => 'Manage workflow of Streams entry',
			'id' => 'Manage workflow of Streams entry',
		);
		$info['frontend'] = true;
		$info['backend'] = true;
		$info['menu'] = 'structure';
		$info['roles'] = array(
			'access_workflows_backend', 'view_all_workflows', 'view_own_workflows', 'edit_all_workflows', 'edit_own_workflows', 'delete_all_workflows', 'delete_own_workflows', 'create_workflows',
			'access_statuses_backend', 'view_all_statuses', 'view_own_statuses', 'edit_all_statuses', 'edit_own_statuses', 'delete_all_statuses', 'delete_own_statuses', 'create_statuses',
			'access_transitions_backend', 'view_all_transitions', 'edit_all_transitions', 'delete_all_transitions', 'create_transitions',
			'access_items_backend', 'view_all_items', 'view_own_items', 'edit_all_items', 'edit_own_items', 'delete_all_items', 'delete_own_items', 'create_items',
		);

		if(group_has_role('workflow', 'access_workflows_backend')){
			$info['sections']['workflows']['name'] = 'workflow:workflows:plural';
			$info['sections']['workflows']['uri'] = 'admin/workflow/workflows/index';

			if(group_has_role('workflow', 'create_workflows')){
				$info['sections']['workflows']['shortcuts']['create'] = array(
					'name' => 'workflow:workflows:new',
					'uri' => 'admin/workflow/workflows/create',
					'class' => 'add'
				);
			}
		}

		if(group_has_role('workflow', 'access_items_backend')){
			$info['sections']['items']['name'] = 'workflow:items:plural';
			$info['sections']['items']['uri'] = 'admin/workflow/items/index';
		}

		return $info;
    }

    /**
     * Install
     *
     * This function will set up our streams
	 *
     */
    public function install()
    {
        // We're using the streams API to
        // do data setup.
        $this->load->driver('Streams');

        $this->load->language('workflow/workflow');

        // Add streams
        if ( ! $workflows_stream_id = $this->streams->streams->add_stream('lang:workflow:workflows:plural', 'workflows', 'workflow', 'workflow_', null)) return false;
        if ( ! $statuses_stream_id = $this->streams->streams->add_stream('lang:workflow:statuses:plural', 'statuses', 'workflow', 'workflow_', null)) return false;
        if ( ! $transitions_stream_id = $this->streams->streams->add_stream('lang:workflow:transitions:plural', 'transitions', 'workflow', 'workflow_', null)) return false;
        if ( ! $items_stream_id = $this->streams->streams->add_stream('lang:workflow:items:plural', 'items', 'workflow', 'workflow_', null)) return false;

        // Add some fields
        $fields = array(
            array(
                'name' => 'lang:workflow:workflow_name',
                'slug' => 'workflow_name',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'workflows',
                'title_column' => true,
                'required' => true,
                'unique' => true,
            ),
			array(
                'name' => 'lang:workflow:workflow_slug',
                'slug' => 'workflow_slug',
                'namespace' => 'workflow',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '_',
					'slug_field' => 'workflow_name',
				),
                'assign' => 'workflows',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:workflow:workflow_description',
                'slug' => 'workflow_description',
                'namespace' => 'workflow',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'workflows',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:workflow_stream_slug',
                'slug' => 'workflow_stream_slug',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'workflows',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:workflow_namespace_slug',
                'slug' => 'workflow_namespace_slug',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'workflows',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:status_name',
                'slug' => 'status_name',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'statuses',
                'title_column' => true,
                'required' => true,
                'unique' => false,
            ),
			array(
                'name' => 'lang:workflow:status_slug',
                'slug' => 'status_slug',
                'namespace' => 'workflow',
                'type' => 'slug',
                'extra' => array(
					'space_type' => '_',
					'slug_field' => 'status_name',
				),
                'assign' => 'statuses',
                'title_column' => false,
                'required' => true,
                'unique' => true,
            ),
            array(
                'name' => 'lang:workflow:status_description',
                'slug' => 'status_description',
                'namespace' => 'workflow',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'statuses',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:status_workflow',
                'slug' => 'status_workflow',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $workflows_stream_id,
					'link_uri' => 'admin/workflow/workflows/view/-id-',
				),
                'assign' => 'statuses',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:status_is_start',
                'slug' => 'status_is_start',
                'namespace' => 'workflow',
                'type' => 'choice',
                'extra' => array(
					'choice_data' => "true : Yes\nfalse : No",
					'choice_type' => 'dropdown',
					'default_value' => 'false',
					//'min_choices' => '',
					//'max_choices' => '',
				),
                'assign' => 'statuses',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
			array(
                'name' => 'lang:workflow:status_sort_order',
                'slug' => 'status_sort_order',
                'namespace' => 'workflow',
                'type' => 'integer',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'statuses',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:transition_from',
                'slug' => 'transition_from',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $statuses_stream_id,
					'link_uri' => 'admin/workflow/statuses/view/-id-',
				),
                'assign' => 'transitions',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:transition_to',
                'slug' => 'transition_to',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $statuses_stream_id,
					'link_uri' => 'admin/workflow/statuses/view/-id-',
				),
                'assign' => 'transitions',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:transition_triggers',
                'slug' => 'transition_triggers',
                'namespace' => 'workflow',
                'type' => 'textarea',
                'assign' => 'transitions',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_entry_id',
                'slug' => 'item_entry_id',
                'namespace' => 'workflow',
                'type' => 'integer',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_stream_slug',
                'slug' => 'item_stream_slug',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_namespace_slug',
                'slug' => 'item_namespace_slug',
                'namespace' => 'workflow',
                'type' => 'text',
                'extra' => array(
					//'max_length' => '',
					//'default_value' => '',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_workflow',
                'slug' => 'item_workflow',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $workflows_stream_id,
					'link_uri' => 'admin/workflow/workflows/view/-id-',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_from',
                'slug' => 'item_from',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $statuses_stream_id,
					'link_uri' => 'admin/workflow/statuses/view/-id-',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_to',
                'slug' => 'item_to',
                'namespace' => 'workflow',
                'type' => 'relationship',
                'extra' => array(
					'choose_stream' => $statuses_stream_id,
					'link_uri' => 'admin/workflow/statuses/view/-id-',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => true,
                'unique' => false,
            ),
            array(
                'name' => 'lang:workflow:item_entry_data',
                'slug' => 'item_entry_data',
                'namespace' => 'workflow',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
			array(
                'name' => 'lang:workflow:item_notes',
                'slug' => 'item_notes',
                'namespace' => 'workflow',
                'type' => 'textarea',
                'extra' => array(
					//'default_text' => '',
					//'allow_tags' => '',
					//'content_type' => '',
				),
                'assign' => 'items',
                'title_column' => false,
                'required' => false,
                'unique' => false,
            ),
        );

        $this->streams->fields->add_fields($fields);

		return true;
    }

    /**
     * Uninstall
     *
     * Uninstall our module - this should tear down
     * all information associated with it.
     */
    public function uninstall()
    {
        $this->load->driver('Streams');

        // For this teardown we are using the simple remove_namespace
        // utility in the Streams API Utilties driver.
        $this->streams->utilities->remove_namespace('workflow');

        return true;
    }

    public function upgrade($old_version)
    {
        return true;
    }

    public function help()
    {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}