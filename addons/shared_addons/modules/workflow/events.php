<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Workflow Events Class
 *
 * @category    events
 * @author      Aditya Satrya
 */
class Events_Workflow
{
    protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
		$this->ci->load->library('workflow/Workflow');

        Events::register('streams_post_insert_entry', array($this, 'create_workflow_item'));
		Events::register('streams_post_delete_entry', array($this, 'end_workflow_item'));
		Events::register('streams_post_update_entry', array($this, 'edit_entry_trigger'));
		Events::register('module_disabled', array($this, 'delete_all_entries'));
     }

    public function create_workflow_item($entry)
    {
        $params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		$params['where'] = "workflow_stream_slug = '".$entry['stream']->stream_slug."' AND workflow_namespace_slug = '".$entry['stream']->stream_namespace."'";

		$workflows = $this->ci->streams->entries->get_entries($params);

		foreach($workflows['entries'] as $workflow){
			$params = array();
			$params['stream'] = 'statuses';
			$params['namespace'] = 'workflow';
			$params['where'] = "status_workflow = ".$workflow['id']." AND status_is_start = 'true'";

			$start_statuses = $this->ci->streams->entries->get_entries($params);
			
			if($start_statuses['total'] > 0){
				$start_status = $start_statuses['entries'][0];
				
				$data_entry = array(
					'item_entry_id' => $entry['entry_id'],
					'item_stream_slug' => $entry['stream']->stream_slug,
					'item_namespace_slug' => $entry['stream']->stream_namespace,
					'item_workflow' => $workflow['id'],
					'item_from' => 0,
					'item_to' => $start_status['id'],
					'item_entry_data' => serialize($entry),
					'item_notes' => 'Event triggered: create_entry',
				);
				$this->ci->workflow->perform_transition($data_entry);
			}
		}
    }

	public function end_workflow_item($entry)
    {
        $params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		$params['where'] = "workflow_stream_slug = '".$entry['stream']->stream_slug."' AND workflow_namespace_slug = '".$entry['stream']->stream_namespace."'";

		$workflows = $this->ci->streams->entries->get_entries($params);

		foreach($workflows['entries'] as $workflow){
			$last_items = $this->ci->workflow->get_last_item($workflow['id'], $entry['entry_id']);
			if($last_items['total'] > 0){
				$last_item = $last_items['entries'][0];
				$data_entry = array(
					'item_entry_id' => $entry['entry_id'],
					'item_stream_slug' => $entry['stream']->stream_slug,
					'item_namespace_slug' => $entry['stream']->stream_namespace,
					'item_workflow' => $workflow['id'],
					'item_from' => $last_item['item_to']['id'],
					'item_to' => 0,
					'item_entry_data' => serialize($entry),
					'item_notes' => 'Event triggered: delete_entry',
				);
				$this->ci->workflow->perform_transition($data_entry);
			}
		}
    }

	public function edit_entry_trigger($entry)
	{
		$params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		$params['where'] = "workflow_stream_slug = '".$entry['stream']->stream_slug."' AND workflow_namespace_slug = '".$entry['stream']->stream_namespace."'";

		$workflows = $this->ci->streams->entries->get_entries($params);

		foreach($workflows['entries'] as $workflow){

			// Get last item in each workflow
			$last_items = $this->ci->workflow->get_last_item($workflow['id'], $entry['entry_id']);
			if($last_items['total'] > 0){
				$last_item = $last_items['entries'][0];

				// Check if needed to perform transition
				// Get transition
				$params = array();
				$params['stream'] = 'transitions';
				$params['namespace'] = 'workflow';
				$params['where'] = "transition_from = ".$last_item['item_to']['id'];

				$transitions = $this->ci->streams->entries->get_entries($params);
				foreach($transitions['entries'] as $transition){
					if(isset($transition['transition_triggers'])){
						$transition['transition_triggers'] = unserialize($transition['transition_triggers']);
					}

					if(isset($transition['transition_triggers']['event']) AND is_array($transition['transition_triggers']['event'])){
						foreach($transition['transition_triggers']['event'] as $event_trigger){
							if($event_trigger == 'edit_entry'){
								$data_entry = array(
									'item_entry_id' => $entry['entry_id'],
									'item_stream_slug' => $entry['stream']->stream_slug,
									'item_namespace_slug' => $entry['stream']->stream_namespace,
									'item_workflow' => $workflow['id'],
									'item_from' => $last_item['item_to']['id'],
									'item_to' => $transition['transition_to']['id'],
									'item_entry_data' => serialize($entry),
									'item_notes' => 'Event triggered: edit_entry',
								);
								$this->ci->workflow->perform_transition($data_entry);
							}
						}
					}
				}
			}
		}
	}
	
	function delete_all_entries($module_slug)
	{
		$this->ci->db->where('item_namespace_slug', $module_slug);
		$this->ci->db->delete('workflow_items'); 
	}

}
/* End of file events.php */