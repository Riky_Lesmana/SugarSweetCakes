<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * Organization Library
 *
 * @author Aditya Satrya
 */

class Workflow
{
	/**
	 * The Construct
	 */
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->driver('Streams');
		$this->CI->lang->load('workflow/workflow');
	}

	public function get_transitions_by_statuses($status_from_id, $status_to_id)
	{
		$params = array();
		$params['stream'] = 'transitions';
		$params['namespace'] = 'workflow';
		$params['where'] = 'transition_from = '.$status_from_id.' AND transition_to = '.$status_to_id;

        return $this->CI->streams->entries->get_entries($params);
	}

	public function get_last_item($workflow, $entry_id)
	{
		if(! is_numeric($workflow)){
			$params = array();
			$params['stream'] = 'workflows';
			$params['namespace'] = 'workflow';
			$params['where'] = "workflow_slug = '".$workflow."'";

			$workflows = $this->CI->streams->entries->get_entries($params);
			if($workflows['total'] > 0){
				$workflow = $workflows['entries'][0]['id'];
			}else{
				$workflow = 0;
			}
		}

		$params = array();
		$params['stream'] = 'items';
		$params['namespace'] = 'workflow';
		$params['where'] = "item_workflow = ".$workflow." AND item_entry_id = ".$entry_id;
		$params['limit'] = 1;
		$params['order_by'] = 'created';
		$params['sort'] = 'desc';

		return $this->CI->streams->entries->get_entries($params);
	}

	public function get_last_item_status($workflow, $entry_id)
	{
		$last_item = $this->get_last_item($workflow, $entry_id);
		if($last_item['total'] > 0){
			return $last_item['entries'][0]['item_to'];
		}else{
			return NULL;
		}
	}

	public function build_transition_form($workflow_slug, $entry_id, $stream_slug, $stream_namespace, $format = FALSE, $show_history = FALSE)
	{
		if($_POST){
			// Load form validation library first
			$this->CI->load->library('form_validation');

			// Set validation rules
			$this->CI->form_validation->set_rules('transition_status_to', lang('workflow:change_status_to'), 'required');

			// Run validation and process form input
			if($this->CI->form_validation->run() === true){
				$entry = $this->CI->streams->entries->get_entry($entry_id, $stream_slug, $stream_namespace);
				$data_entry = array(
					'item_entry_id' => $this->CI->input->post('transition_entry_id'),
					'item_stream_slug' => $this->CI->input->post('transition_stream_slug'),
					'item_namespace_slug' => $this->CI->input->post('transition_stream_namespace'),
					'item_workflow' => $this->CI->input->post('transition_workflow_id'),
					'item_from' => $this->CI->input->post('transition_status_from'),
					'item_to' => $this->CI->input->post('transition_status_to'),
					'item_entry_data' => serialize($entry),
					'item_notes' => $this->CI->input->post('transition_notes'),
				);
				
				if($this->perform_transition($data_entry)){
					$this->CI->session->set_flashdata('success', lang('workflow:status_changed_success_message'));
					redirect(uri_string());
				}else{
					$this->CI->session->set_flashdata('error', lang('workflow:transition_error'));
					redirect(uri_string());
				}
			}
		}

		// Return value
		$retval = array();
		$retval['entry_id'] = $entry_id;
		$retval['stream_slug'] = $stream_slug;
		$retval['stream_namespace'] = $stream_namespace;

		// Get workflow
		$params = array();
		$params['stream'] = 'workflows';
		$params['namespace'] = 'workflow';
		$params['where'] = "workflow_slug = '".$workflow_slug."'";
        $workflow = $this->CI->streams->entries->get_entries($params);

		if($workflow['total'] > 0){
			$workflow = $workflow['entries'][0];
			$retval['workflow'] = $workflow;
		}else{
			return 'Workflow is not valid.';
		}

		// Get Items history
		$retval['show_history'] = $show_history;
		$retval['items_history'] = $this->build_item_history_table($workflow['id'], $entry_id);

		// Get last Item
		$last_item = $this->get_last_item($workflow['id'], $entry_id);
		if($last_item['total'] > 0){
			// The Entry is already in the workflow
			$last_item = $last_item['entries'][0];
			$retval['last_item'] = $last_item;

			// Get available transitions
			$params = array();
			$params['stream'] = 'transitions';
			$params['namespace'] = 'workflow';
			$params['where'] = 'transition_from = '.$last_item['item_to']['id'];
			$transitions = $this->CI->streams->entries->get_entries($params);

			// Filter
			$available_statuses = array();
			foreach($transitions['entries'] as $transition){
				$triggers = unserialize($transition['transition_triggers']);
				if(isset($triggers['manual']) AND is_array($triggers['manual'])){
					if(isset($this->CI->current_user->group) AND in_array($this->CI->current_user->group, $triggers['manual'])){
						$available_statuses[] = $transition['transition_to'];
					}
				}
			}
		}else{
			// Newly created Entry
			// Get all statuses
			// Get available transitions
			$params = array();
			$params['stream'] = 'statuses';
			$params['namespace'] = 'workflow';
			$params['where'] = 'status_workflow = '.$workflow['id'];
			$params['order_by'] = 'status_sort_order';
			$params['sort'] = 'asc';
			$statuses = $this->CI->streams->entries->get_entries($params);
			$available_statuses = $statuses['entries'];
		}
		$retval['available_statuses'] = $available_statuses;
		
		if($format){
			$retval = $this->CI->template->load_view('workflow/partials/transition_form', $retval);
		}

		return $retval;
	}

	public function build_item_history_table($workflow_id, $entry_id, $format = FALSE)
	{
		$params = array();
		$params['stream'] = 'items';
		$params['namespace'] = 'workflow';
		$params['where'] = "item_workflow = ".$workflow_id." AND item_entry_id = ".$entry_id;
		$params['order_by'] = 'created';
		$params['sort'] = 'asc';
		
		$items = $this->CI->streams->entries->get_entries($params);

		// Format in table
		if($format){
			$items = $this->CI->template->load_view('workflow/partials/items_history_table', $items);
		}

		return $items;
	}

	public function _is_valid_status_from($workflow_id, $entry_id, $status_id)
	{
		if($status_id == 0){
			return TRUE;
		}

		$last_item = $this->get_last_item($workflow_id, $entry_id);
		if(isset($last_item['entries'][0]['item_to']['id']) AND $last_item['entries'][0]['item_to']['id'] == $status_id){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function _is_valid_status_to($status_from_id, $status_to_id)
	{
		if($status_from_id == 0 OR $status_to_id == 0){
			return TRUE;
		}

		$transitions = $this->get_transitions_by_statuses($status_from_id, $status_to_id);
		if($transitions['total'] > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	public function perform_transition($params)
	{
		if(! isset($params['item_entry_id'])){return FALSE;}
		if(! isset($params['item_stream_slug'])){return FALSE;}
		if(! isset($params['item_namespace_slug'])){return FALSE;}
		if(! isset($params['item_workflow'])){return FALSE;}
		if(! isset($params['item_from'])){return FALSE;}
		if(! isset($params['item_to'])){return FALSE;}
		if(! isset($params['item_entry_data'])){return FALSE;}
		
		if(! $this->_is_valid_status_from($params['item_workflow'], $params['item_entry_id'], $params['item_from'])){
			return FALSE;
		}
		if(! $this->_is_valid_status_to($params['item_from'], $params['item_to'])){
			return FALSE;
		}

		// -------------------------------------
		// Event: Pre Workflow Transition
		// -------------------------------------
		$trigger_data = array(
			'params'		=> $params,
		);
		Events::trigger('workflow_pre_transition', $trigger_data);
		
		$retval = $this->CI->streams->entries->insert_entry($params, 'items', 'workflow');
		if($retval){

			// -------------------------------------
			// Event: Post Workflow Transition
			// -------------------------------------
			$trigger_data = array(
				'params'		=> $params,
				'item_id'		=> $retval,
			);
			Events::trigger('workflow_post_transition', $trigger_data);

			return $retval;
		}else{
			return FALSE;
		}
	}
}