<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Streams Model in Workflow Module
 *
 * @author		Aditya Satrya
 */
class Workflows_m extends MY_Model {

	public $table;

	/**
	 * Streams cache
	 * Stored by slug
	 */
	public $streams_cache = array();

	public function __construct()
	{
		$this->table = STREAMS_TABLE;

		// We just grab all the streams now.
		// That way we don't have to do a separate DB
		// call for each.
		$this->run_cache();
	}

	/**
	 * Run Cache
	 *
	 * Puts streams data into cache to reduce
	 * database load.
	 *
	 * @access 	private
	 * @param 	string - type 'id' or 'slug'
	 * @return 	void
	 */
	private function run_cache()
	{
		foreach($this->db->get($this->table)->result() as $stream)
		{
			if (trim($stream->view_options) == '')
			{
				$stream->view_options = array();
			}
			else
			{
				$stream->view_options = unserialize($stream->view_options);

				// Just in case we get bad data
				if ( ! is_array($stream->view_options))
				{
					$stream->view_options = array();
				}
			}

			$this->streams_cache[$stream->id] = $stream;
			$this->streams_cache['ns'][$stream->stream_namespace][$stream->stream_slug] = $stream;
		}

	}

	/**
     * Get all streams
     *
     * @access	public
     * @param	[int limit]
     * @param	[int offset]
     * @return	obj
     */
    public function get_all_streams($limit = null, $offset = 0)
	{
		if ($limit) $this->db->limit($limit, $offset);

		$obj = $this->db
				->order_by('stream_namespace', 'ASC')
				->order_by('stream_slug', 'ASC')
				->get($this->table);

		if ($obj->num_rows() == 0)
		{
			return false;
		}

		$streams = $obj->result();

		// Go through and unserialize all the view_options
		foreach ($streams as $key => $stream)
		{
			$streams[$key]->view_options = unserialize($streams[$key]->view_options);
		}

		return $streams;
	}

}