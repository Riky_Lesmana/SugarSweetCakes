<div class="page-header">
	<h1><?php echo lang('workflow:items:view'); ?></h1>
	
	<div class="btn-group content-toolbar">
		
		<a href="<?php echo site_url('admin/workflow/items/index'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('workflow:back') ?>
		</a>
		
	</div>
</div>

<div>
	<div class="entry-detail-row">
		<div class="entry-detail-name"><?php echo lang('workflow:item_entry_data'); ?></div>
		<?php if($items->item_entry_data OR $items->item_entry_data == ''){ ?>
		<div class="entry-detail-value"><?php echo '<pre>'.print_r(unserialize($items->item_entry_data), TRUE).'</pre>'; ?></div>
		<?php }else{ ?>
		<div class="entry-detail-value">-</div>
		<?php } ?>
	</div>
</div>