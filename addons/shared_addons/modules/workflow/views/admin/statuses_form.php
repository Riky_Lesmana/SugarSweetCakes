<div class="page-header">
	<h1><?php echo lang('workflow:statuses:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_workflow']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_workflow']['input_title']);?> <?php echo $fields['status_workflow']['required'];?></label>

		<div class="col-sm-10">
			<span class="form-entry"><?php echo $workflow_entry->workflow_name; ?></span>
			<input type="hidden" name="<?php echo $fields['status_workflow']['input_slug']; ?>" value="<?php echo $workflow_entry->id ?>" />
			
			<?php if( $fields['status_workflow']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_workflow']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_name']['input_title']);?> <?php echo $fields['status_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['status_name']['input']; ?>
			
			<?php if( $fields['status_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_name']['instructions']); ?></small>
			<?php endif; ?>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_slug']['input_title']);?> <?php echo $fields['status_slug']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['status_slug']['input']; ?>
			
			<?php if( $fields['status_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_description']['input_title']);?> <?php echo $fields['status_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['status_description']['input']; ?>
			
			<?php if( $fields['status_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_is_start']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_is_start']['input_title']);?> <?php echo $fields['status_is_start']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['status_is_start']['input']; ?>
			
			<?php if( $fields['status_is_start']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_is_start']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['status_sort_order']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['status_sort_order']['input_title']);?> <?php echo $fields['status_sort_order']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['status_sort_order']['input']; ?>
			
			<?php if( $fields['status_sort_order']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['status_sort_order']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>
<input type="hidden" name="return" value="<?php echo $return; ?>" />

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>