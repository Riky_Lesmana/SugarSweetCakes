<div class="page-header">
	<h1><?php echo lang('workflow:transitions:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['transition_from']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['transition_from']['input_title']);?> <?php echo $fields['transition_from']['required'];?></label>

		<div class="col-sm-10">
			<span class="form-entry"><?php echo $status_from_entry->status_name ?></span>
			<input type="hidden" name="<?php echo $fields['transition_from']['input_slug'];?>" value="<?php echo $status_from_entry->id;?>" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['transition_to']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['transition_to']['input_title']);?> <?php echo $fields['transition_to']['required'];?></label>

		<div class="col-sm-10">
			<span class="form-entry"><?php echo $status_to_entry->status_name ?></span>
			<input type="hidden" name="<?php echo $fields['transition_to']['input_slug'];?>" value="<?php echo $status_to_entry->id;?>" />
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('workflow:transition_manual_triggers'); ?></label>

		<div class="col-sm-10">
			<?php $triggers = unserialize($fields['transition_triggers']['value']); ?>
			<?php foreach($groups as $group){ ?>
			<input type="checkbox" name="transition_triggers[manual][]" value="<?php echo $group->name; ?>" <?php if(isset($triggers['manual']) AND is_array($triggers['manual']) AND in_array($group->name, $triggers['manual'])){echo 'checked';} ?> />
			<?php echo $group->description; ?>
			<br />
			<?php } ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('workflow:transition_event_triggers'); ?></label>

		<div class="col-sm-10">
			<?php $triggers = unserialize($fields['transition_triggers']['value']); ?>
			<input type="checkbox" name="transition_triggers[event][]" value="edit_entry" <?php if(isset($triggers['event']) AND is_array($triggers['event']) AND in_array('edit_entry', $triggers['event'])){echo 'checked';} ?> />
			<?php echo lang('workflow:transition_event_edit_entry_triggers'); ?>
			<br />
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>