<div class="page-header">
	<h1><?php echo lang('workflow:workflows:'.$mode); ?></h1>
</div>

<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<div class="form-horizontal">

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['workflow_name']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['workflow_name']['input_title']);?> <?php echo $fields['workflow_name']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['workflow_name']['input']; ?>
			
			<?php if( $fields['workflow_name']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['workflow_name']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['workflow_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['workflow_slug']['input_title']);?> <?php echo $fields['workflow_slug']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['workflow_slug']['input']; ?>

			<?php if( $fields['workflow_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['workflow_slug']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['workflow_description']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['workflow_description']['input_title']);?> <?php echo $fields['workflow_description']['required'];?></label>

		<div class="col-sm-10">
			<?php echo $fields['workflow_description']['input']; ?>
			
			<?php if( $fields['workflow_description']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['workflow_description']['instructions']); ?></small>
			<?php endif; ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right" for="<?php echo $fields['workflow_stream_slug']['input_slug'];?>"><?php echo $this->fields->translate_label($fields['workflow_stream_slug']['input_title']);?> <?php echo $fields['workflow_stream_slug']['required'];?></label>

		<div class="col-sm-10">
			<select name="namespace_stream">
			<?php foreach($streams as $stream){ ?>
				<?php $value = $stream->stream_namespace.'|'.$stream->stream_slug; ?>
				<option value="<?php echo $value; ?>" <?php if($mode == 'edit' AND $entry->workflow_namespace_slug.'|'.$entry->workflow_stream_slug == $value){echo 'selected';} ?>>
					<?php echo $stream->stream_namespace.' / '.$stream->stream_slug; ?>
				</option>
			<?php } ?>
			</select>
			
			<?php if( $fields['workflow_stream_slug']['instructions'] != '' ): ?>
				<br /><small><?php echo $this->fields->translate_label($fields['workflow_stream_slug']['instructions']); ?></small>
			<?php endif; ?>
			
		</div>
	</div>

</div>

<?php if ($mode == 'edit'){ ?><input type="hidden" value="<?php echo $entry->id;?>" name="row_edit_id" /><?php } ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button type="submit" name="btnAction" value="save" class="btn btn-primary"><span><?php echo lang('buttons:save'); ?></span></button>
		<a href="<?php echo site_url(isset($return) ? $return : 'admin/streams/entries/index/'.$stream->id); ?>" class="btn btn-danger"><?php echo lang('buttons:cancel'); ?></a>
	</div>
</div>

<?php echo form_close();?>