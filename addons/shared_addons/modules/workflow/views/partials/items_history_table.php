<?php if ($items['total'] > 0): ?>

	<table class="table table-bordered" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th><?php echo lang('workflow:item_from'); ?></th>
				<th><?php echo lang('workflow:item_to'); ?></th>
				<th><?php echo lang('workflow:item_notes'); ?></th>
				<th><?php echo lang('workflow:created'); ?></th>
				<th><?php echo lang('workflow:created_by'); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($items['entries'] as $items_entry): ?>
			<tr>
				<td>
				<?php
				if($items_entry['item_from']['id'] == NULL){
					echo 'NEW';
				}else{
					echo $items_entry['item_from']['status_name'];
				}
				?>
				</td>
				<td>
				<?php
				if($items_entry['item_to']['id'] == NULL){
					echo 'DELETED';
				}else{
					echo $items_entry['item_to']['status_name'];
				}
				?>
				</td>

				<td><?php echo $items_entry['item_notes']; ?></td>

				<?php if($items_entry['created']){ ?>
				<td><?php echo format_date($items_entry['created'], 'd-m-Y G:i'); ?></td>
				<?php }else{ ?>
				<td>-</td>
				<?php } ?>

				<td><?php echo user_displayname($items_entry['created_by']['user_id'], true); ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php echo $items['pagination']; ?>

<?php else: ?>
	<div class="no_data"><?php echo lang('workflow:items:no_entry'); ?></div>
<?php endif;?>