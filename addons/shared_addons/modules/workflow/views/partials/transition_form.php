<?php echo form_open_multipart(uri_string(), 'class="streams_form"'); ?>

<input type="hidden" name="transition_entry_id" value="<?php echo $entry_id; ?>" />
<input type="hidden" name="transition_stream_slug" value="<?php echo $stream_slug; ?>" />
<input type="hidden" name="transition_stream_namespace" value="<?php echo $stream_namespace; ?>" />
<input type="hidden" name="transition_workflow_id" value="<?php echo $workflow['id']; ?>" />

<?php if($show_history){ ?>

<label><?php echo lang('workflow:items_history'); ?></label>
<div class="input" style="margin-bottom: 10px;">
	<?php $data['items'] = $items_history; ?>
	<?php echo $this->template->load_view('workflow/partials/items_history_table', $data); ?>
</div>

<?php } ?>

<label><?php echo lang('workflow:current_status'); ?></label>
<div class="input" style="margin-bottom: 10px;">
	<?php if(isset($last_item)){ ?>
		<?php echo $last_item['item_to']['status_name']; ?>
		<input type="hidden" name="transition_status_from" value="<?php echo $last_item['item_to']['id']; ?>" />
	<?php }else{ ?>
		NEW
		<input type="hidden" name="transition_status_from" value="0" />
	<?php } ?>
</div>

<label><?php echo lang('workflow:change_status_to'); ?></label>

<?php if(count($available_statuses) > 0){ ?>

<div class="input">
	<select name="transition_status_to">
		<option value="">--</option>
		<?php foreach($available_statuses as $status){ ?>
		<option value="<?php echo $status['id']; ?>"><?php echo $status['status_name']; ?></option>
		<?php } ?>
	</select>
</div>

<label><?php echo lang('workflow:item_notes'); ?></label>
<div class="input">
	<textarea name="transition_notes"></textarea>
</div>

<div class="form-actions">
	<button type="submit" name="btnAction" value="save" class=""><span><?php echo lang('buttons:save'); ?></span></button>
</div>

<?php }else{ ?>

<div class="input">
	<?php echo lang('workflow:no_transitions'); ?>
</div>

<?php } ?>

<?php echo form_close();?>