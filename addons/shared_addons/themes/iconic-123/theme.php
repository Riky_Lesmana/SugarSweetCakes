<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Theme_iconic_123 extends Theme
{
    public $name = 'Iconic Theme 123';
    public $author = 'Riky H. Lesmana';
    public $author_website = '';
    public $website = '';
    public $description = 'Iconic Theme 123 By Riky';
    public $version = '1.0.0';
}
 
 /* End of file theme.php */