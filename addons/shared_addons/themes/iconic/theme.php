<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Theme_iconic extends Theme
{
    public $name = 'Iconic Theme';
    public $author = 'Riky H. Lesmana';
    public $author_website = '';
    public $website = '';
    public $description = 'Iconic Theme By Riky';
    public $version = '1.0.0';
}
 
 /* End of file theme.php */