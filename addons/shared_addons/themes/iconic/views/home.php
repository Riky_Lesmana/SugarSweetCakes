<?php /*Asset::js('camera.js', false, 'home'); ?>
<?php echo Asset::render_js('home') ?>

<div class="row">
	<div class="col-xs-12">
		<div id="slider" class="camera_wrap">
			<div data-src="<?php echo Asset::get_filepath_img('slider/bridge.jpg'); ?>">
				<div class="camera_caption left-bottom fadeIn" data-destination="left-bottom">
					<div class="slider-caption">
						<h3><span>Lorem Ipsum</span></h3>
						<div class="slider-description fl"><p>Lorem ipsum dolor sit amet.</p></div>
						<div class="slider-more fl">
							<div class="fl read-more bg-color"><p><a href="#">Read more</a></p></div>
							<div class="arrow-right dark fl"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div data-src="<?php echo Asset::get_filepath_img('slider/leaf.jpg'); ?>">
				<div class="camera_caption left-bottom fadeIn" data-destination="left-bottom">
					<div class="slider-caption">
						<h3><span>Lorem Ipsum</span></h3>
						<div class="slider-description fl"><p>Lorem ipsum dolor sit amet.</p></div>
						<div class="slider-more fl">
							<div class="fl read-more bg-color"><p><a href="#">Read more</a></p></div>
							<div class="arrow-right dark fl"></div>
						</div>
					</div>
				</div>
			</div>
			
			<div data-src="<?php echo Asset::get_filepath_img('slider/sea.jpg'); ?>">
				<div class="camera_caption left-bottom fadeIn" data-destination="left-bottom">
					<div class="slider-caption">
						<h3><span>Lorem Ipsum</span></h3>
						<div class="slider-description fl"><p>Lorem ipsum dolor sit amet.</p></div>
						<div class="slider-more fl">
							<div class="fl read-more bg-color"><p><a href="#">Read more</a></p></div>
							<div class="arrow-right dark fl"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('#slider').camera({
		height: '35%',
        loader: 'bar',
        pagination: false,
        thumbnails: false,
        hover: false,
        playPause:false,
        opacityOnGrid: false,
        imagePath: '',
        fx: 'simpleFade, scrollRight, scrollBottom'
	});
</script>

<div id="" class="row vmargin"> 
	<div class="col-xs-12">
	<div class="view rep-bar">
		<div class="row">
			<div class="col-xs-9">
				<div id="" class="content-bar">
					<h3> sorena </h3>
					<h4> Easy to customize, Fully responsive, Powerful features </h4>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="representation">
					<h4 class="underline"><span>Begin the journey </span></h4>
					<p>We belive that support is much more important than anything else so we wont let your site down!</p>
					<a href=" # " class="btn btn-large btn-color"> PURCHASE </a>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>


<div class="row vmargin">
	<div class="col-xs-3">
		<div class="iconbox">
			<i class="bicon-Bulb"></i>
			<h4><col-xs->Responsive Design</col-xs-></h4>
			<div class="arrow_box">
				<p> Lorem sit amet ligula est, eget conseact etur lectus maecenas hendrerit suscipit.</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="iconbox">
			<i class="bicon-Work"></i>
			<h4><col-xs->Customization</col-xs-></h4>
			<div class="arrow_box">
				<p> Lorem sit amet ligula est, eget conseact etur lectus maecenas hendrerit suscipit.</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="iconbox">
			<i class="bicon-Stock"></i>
			<h4><col-xs->Fresh &amp; Clean</col-xs-></h4>
			<div class="arrow_box">
				<p> Lorem sit amet ligula est, eget conseact etur lectus maecenas hendrerit suscipit.</p>
			</div>
		</div>
	</div>
	<div class="col-xs-3">
		<div class="iconbox">
			<i class="bicon-Massage"></i>
			<h4><col-xs->Support</col-xs-></h4>
			<div class="arrow_box">
				<p> Lorem sit amet ligula est, eget conseact etur lectus maecenas hendrerit suscipit.</p>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">
		<h3 class="h-bg-1 center vmargin">
			<span>Recent works</span>
		</h3>
	</div>
</div>
	
<div class="row" id="portfolio">
	<div class="portfolio-thumb col-xs-3">
		<div class="view">
			<?php echo Asset::img('portfolio/camera.jpg', ''); ?>
			<div class="portfolio-mask"></div>
			<div class="portfolio-des">
				<div class="title">Lorem Ipsum</div>
			</div>
			<div class="portfolio-zoom">
				<a href="<?php echo Asset::get_filepath_img('portfolio/camera.jpg'); ?>" rel="prettyPhoto" title="" target="_blank">
					<?php echo Asset::img('zoom.png', 'zoom'); ?>
				</a>
			</div>
			<div class="portfolio-link">
				<a href="#">
					<?php echo Asset::img('link.png', 'link'); ?>
				</a>
			</div>
		</div>
	</div>
	
	<div class="portfolio-thumb col-xs-3">
		<div class="view">
			<?php echo Asset::img('portfolio/hanger.jpg', ''); ?>
			<div class="portfolio-mask"></div>
			<div class="portfolio-des">
				<div class="title">Lorem Ipsum</div>
			</div>
			<div class="portfolio-zoom">
				<a href="<?php echo Asset::get_filepath_img('portfolio/hanger.jpg'); ?>" rel="prettyPhoto" title="" target="_blank">
					<?php echo Asset::img('zoom.png', 'zoom'); ?>
				</a>
			</div>
			<div class="portfolio-link">
				<a href="#">
					<?php echo Asset::img('link.png', 'link'); ?>
				</a>
			</div>
		</div>
	</div>
	
	<div class="portfolio-thumb col-xs-3">
		<div class="view">
			<?php echo Asset::img('portfolio/longue.jpg', ''); ?>
			<div class="portfolio-mask"></div>
			<div class="portfolio-des">
				<div class="title">Lorem Ipsum</div>
			</div>
			<div class="portfolio-zoom">
				<a href="<?php echo Asset::get_filepath_img('portfolio/longue.jpg'); ?>" rel="prettyPhoto" title="" target="_blank">
					<?php echo Asset::img('zoom.png', 'zoom'); ?>
				</a>
			</div>
			<div class="portfolio-link">
				<a href="#">
					<?php echo Asset::img('link.png', 'link'); ?>
				</a>
			</div>
		</div>
	</div>
	
	<div class="portfolio-thumb col-xs-3">
		<div class="view">
			<?php echo Asset::img('portfolio/turbine.jpg', ''); ?>
			<div class="portfolio-mask"></div>
			<div class="portfolio-des">
				<div class="title">Lorem Ipsum</div>
			</div>
			<div class="portfolio-zoom">
				<a href="<?php echo Asset::get_filepath_img('portfolio/turbine.jpg'); ?>" rel="prettyPhoto" title="" target="_blank">
					<?php echo Asset::img('zoom.png', 'zoom'); ?>
				</a>
			</div>
			<div class="portfolio-link">
				<a href="#">
					<?php echo Asset::img('link.png', 'link'); ?>
				</a>
			</div>
		</div>
	</div>
	
</div>
<?php */ ?>