<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Theme_sugarsweetcakes extends Theme
{
    public $name = 'Sugar Sweet Cakes';
    public $author = 'Riky H. Lesmana';
    public $author_website = 'http://www.rikyhl.id';
    public $website = 'http://www.rikyhl.id';
    public $description = 'Sugar Sweet Cakes Theme By Riky';
    public $version = '1.0.0';
}
 
 /* End of file theme.php */