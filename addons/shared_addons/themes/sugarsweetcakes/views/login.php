<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
        {{ theme:partial name="meta" }}
	</head>
	<body>

<!-- ========= HEADER ============= -->
        {{ theme:partial name="header" }}
<!-- ========= END HEADER =========== -->

<!-- ========= main About Us ======== -->
		<div class="main" role="main">
			<div class="page-title">
				<h2>Log In</h2>
			</div><!-- page title -->

			<form class="container">
				<div class="about forms">
				<div class="clear">&nbsp;</div><!-- clear -->
					<table class="boxCenter" cellpadding="5" border="0">
						<tr>
							<td align="right" width="300">Username :</td>
							<td width="300"><input class="text" type="text" placeholder="username" /></td>
						</tr>
						<tr>
							<td align="right">Password :</td>
							<td><input class="text" type="password" placeholder="password" /></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="font-size: 13px;">
								<button class="button">Log In</button>
								Belum punya akun? Registrasi <a href="#" style="font-size: 13px;">disini</a>
							</td>
						</tr>
					</table>
				</div><!-- info about -->

				<div class="clear">&nbsp;</div><!-- clear -->
			</form><!-- container -->
		</div><!-- main -->
<!-- ========= end main About Us ==== -->

<!-- ======== FOOTER ================ -->
        {{ theme:partial name="footer" }}
<!-- ======== FOOTER ================ -->
	</body>
</html>