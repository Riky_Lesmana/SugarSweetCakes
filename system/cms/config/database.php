<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Development
$db[PYRO_DEVELOPMENT] = array(
	'hostname'		=> 	'localhost',
	'username'		=> 	'u895396432_ht',
	'password'		=> 	'su1986ga',
	'database'		=> 	'u895396432_ht',
	'dbdriver' 		=> 	'mysqli',
	'dbprefix' 		=>	'',
	'pconnect' 		=>	FALSE,
	'db_debug' 		=>	TRUE,
	'cache_on' 		=>	FALSE,
	'char_set' 		=>	'utf8',
	'dbcollat' 		=>	'utf8_unicode_ci',
	'port' 	 		=>	'3306',

	// 'Tough love': Forces strict mode to test your app for best compatibility
	'stricton' 		=> TRUE,
);

// Staging
/*
$db[PYRO_STAGING] = array(
	'hostname'		=> 	'',
	'username'		=> 	'',
	'password'		=> 	'',
	'database'		=> 	'pyrocms',
	'dbdriver' 		=> 	'mysql',
	'active_r' 		=>	TRUE,
	'pconnect' 		=>	FALSE,
	'db_debug' 		=>	FALSE,
	'cache_on' 		=>	FALSE,
	'char_set' 		=>	'utf8',
	'dbcollat' 		=>	'utf8_unicode_ci',
	'port' 	 		=>	3306,
);
*/

// Production
$db[PYRO_PRODUCTION] = array(
	'hostname'		=> 	'localhost',
	'username'		=> 	'root',
	'password'		=> 	'',
	'database'		=> 	'base_pyro',
	'dbdriver' 		=> 	'mysqli',
	'pconnect' 		=>	FALSE,
	'db_debug' 		=>	FALSE,
	'cache_on' 		=>	FALSE,
	'char_set' 		=>	'utf8',
	'dbcollat' 		=>	'utf8_unicode_ci',
	'port' 	 		=>	'3306',
);


// Check the configuration group in use exists
if ( ! array_key_exists(ENVIRONMENT, $db))
{
	show_error(sprintf(lang('error_invalid_db_group'), ENVIRONMENT));
}

// Assign the group to be used
$active_group = ENVIRONMENT;
$query_builder = TRUE;

/* End of file database.php */
