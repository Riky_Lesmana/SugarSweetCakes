<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home controller
 *
 * Description
 *
 */
class Home extends Public_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
	
	public function index()
	{
		$data['pageNow'] = 'home';

		$this->template->title('Welcome to Iconic Website')
			->set_breadcrumb('Home', '/')
			->set_layout('home.html')
			->build('home',$data);
	}
}