<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Category model
 *
 * @author Aditya Satrya
 */
class Home_m extends MY_Model {
	
	public function get_category()
	{
		$this->db->select('category_name as name, category_slug as slug');
		$this->db->order_by('category_name', 'asc');
		$query = $this->db->get('default_book_module_category');
		$result = $query->result_array();
		
        return $result;
	}
}