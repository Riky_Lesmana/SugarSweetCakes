<div class="page-header">
	<h1><?php echo lang('addons:modules:upload_title');?></h1>
</div>

<?php echo form_open_multipart('admin/addons/modules/upload', array('class' => 'form-horizontal'));?>	

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('addons:modules:upload_desc');?></label>
	<div class="col-sm-10">
		<input type="file" name="userfile" class="input" />
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('upload') )) ?>
	</div>
</div>

<?php echo form_close() ?>