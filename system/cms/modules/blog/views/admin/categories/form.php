<div class="page-header">
	<?php if ($this->controller == 'admin_categories' && $this->method === 'edit'): ?>
	<h1><?php echo sprintf(lang('cat:edit_title'), $category->title);?></h1>
	<?php else: ?>
	<h1><?php echo lang('cat:create_title');?></h1>
	<?php endif ?>
</div>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal"') ?>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right" for="title"><?php echo lang('global:title');?> <span>*</span></label>
	
	<div class="col-sm-10">
		<?php echo  form_input('title', $category->title, 'id="title"') ?>
	</div>
	
</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right" for="slug"><?php echo lang('global:slug') ?> <span>*</span></label>
	
	<div class="col-sm-10">
		<?php echo  form_input('slug', $category->slug, 'id="slug"') ?>
		<script>
			$('#title').slugify({ slug: '#slug', type: '-' });
		</script>
		
		<?php echo  form_hidden('id', $category->id) ?>
	</div>
	
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-10">
		<button class="btn btn-info" value="save" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save'); ?>
		</button>
		
		<a class="btn" href="<?php echo base_url(). 'admin/blog/categories' ?>">
			<i class="icon-remove bigger-110"></i>
			<?php echo lang('buttons:cancel'); ?>
		</a>
	</div>
</div>

<?php echo form_close() ?>