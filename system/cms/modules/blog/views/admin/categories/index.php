<div class="page-header">
	<h1><?php echo lang('cat:list_title') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>
	
<?php if ($categories): ?>

	<?php echo form_open('admin/blog/categories/delete') ?>

	<table class="table table-striped table-bordered table-hover">
		<thead>
		<tr>
			<th class="center" width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
			<th><?php echo lang('cat:category_label') ?></th>
			<th><?php echo lang('global:slug') ?></th>
			<th width="120"></th>
		</tr>
		</thead>
		<tbody>
			<?php foreach ($categories as $category): ?>
			<tr>
				<td class="center"><?php echo form_checkbox('action_to[]', $category->id) ?></td>
				<td><?php echo $category->title ?></td>
				<td><?php echo $category->slug ?></td>
				<td class="align-center buttons buttons-small">
					<?php echo anchor('admin/blog/categories/edit/'.$category->id, lang('global:edit'), 'class="btn btn-xs btn-blue"') ?>
					<?php echo anchor('admin/blog/categories/delete/'.$category->id, lang('global:delete'), 'class="confirm btn btn-xs btn-danger"') ;?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php $this->load->view('admin/partials/pagination') ?>

	<div class="table_action_buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete') )) ?>
	</div>

	<?php echo form_close() ?>

<?php else: ?>
	<div class="well"><?php echo lang('cat:no_categories') ?></div>
<?php endif ?>