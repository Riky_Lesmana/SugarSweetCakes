<?php Asset::js('ckeditor/ckeditor.js', 'wysiwyg'); ?>
<?php Asset::js('ckeditor/adapters/jquery.js', 'wysiwyg'); ?>
<?php echo Asset::render_js('wysiwyg') ?>

<script type="text/javascript">

	var instance;

	function update_instance()
	{
		instance = CKEDITOR.currentInstance;
	}

	(function($) {
		$(function(){

			pyro.init_ckeditor = function(){
				<?php echo $this->parser->parse_string(Settings::get('ckeditor_config'), $this, TRUE); ?>
			};
			pyro.init_ckeditor();

		});
	})(jQuery);
</script>

<div class="page-header">
<?php if ($this->method == 'create'): ?>
	<h1><?php echo lang('blog:create_title') ?></h1>
<?php else: ?>
	<h1><?php echo sprintf(lang('blog:edit_title'), $post->title) ?></h1>
<?php endif ?>
</div>

<?php echo form_open_multipart('', array('class' => 'form-horizontal')); ?>

<div class="row">

<div class="col-sm-8">

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:title') ?>*</label>
	
	<div class="col-sm-10">
	
		<input type="text" class="form-control" placeholder="<?php echo lang('global:title') ?>" name="title" id="title" value="<?php echo htmlspecialchars_decode($post->title); ?>" maxlength="100" />
		
	</div>
	
</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug') ?>*</label>
	
	<div class="col-sm-10">
	
		<input type="text" class="form-control" placeholder="<?php echo lang('global:slug'); ?>" id="slug" name="slug" value="<?php echo $post->slug; ?>" maxlength="100" />
		
		<script>
			$('#title').slugify({ slug: '#slug', type: '-' });
		</script>
		
	</div>
</div>

<div class="form-group">

	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('blog:content_label') ?>*</label>
	
	<div class="col-sm-10">
		
		<?php echo form_dropdown('type', array(
			'html' => 'html',
			'markdown' => 'markdown',
			'wysiwyg-simple' => 'wysiwyg-simple',
			'wysiwyg-advanced' => 'wysiwyg-advanced',
		), $post->type, 'style="margin-bottom:10px"') ?>
		<script>
			$("select[name='type']").change(function() {
				var textarea = $('#body');
				// Destroy existing WYSIWYG instance
				textarea.removeClass();
				var instance = CKEDITOR.instances[textarea.attr('id')];
				instance && instance.destroy();
				// Set up the new instance
				textarea.addClass($(this).val());
				textarea.addClass('form-control');
				pyro.init_ckeditor();
			});
		</script>
		
		<br />
		
		<?php echo form_textarea(array('id' => 'body', 'name' => 'body', 'value' => $post->body, 'rows' => 30, 'class' => $post->type.' form-control')) ?>
		
		<?php echo form_hidden('preview_hash', $post->preview_hash)?>
		
	</div>
	
</div>

<?php if ($stream_fields): ?>

	<?php 
	foreach ($stream_fields as $field){
		echo $this->load->view('admin/partials/streams/form_single_display', array('field' => $field), true);
	}
	?>

<?php endif; ?>

</div>

<div class="col-sm-4">

<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:publishing_options_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">
			
			<label><?php echo lang('blog:status_label') ?></label>
			<div class="row">
				<div class="col-xs-8 col-sm-11">
					<div class="input-group">
						
						<?php echo form_dropdown('status', array('draft' => lang('blog:draft_label'), 'live' => lang('blog:live_label')), $post->status, 'class="input-medium"') ?>
						
					</div>
				</div>
			</div>
			
			<hr />
			
			<label><?php echo lang('blog:date_label') ?></label>
			<div class="row">
				<div class="col-xs-8 col-sm-11">
					<div class="input-group">
						
						<span class="input-icon">
							<?php echo form_input('created_on', date("d-m-Y", $post->created_on), 'class="input-medium" id="created_on_date"') ?>
							<i class="icon-calendar blue"></i>
						</span>
						<script>
							$(document).ready(function(){
								$('#created_on_date').datepicker({autoclose: true, format: 'dd-mm-yyyy'});
							});
						</script>
						
						<span class="input-icon bootstrap-timepicker">
							<?php echo form_input('created_on_time', date("H:i", $post->created_on), 'class="input-small" id="created_on_time"') ?>
							<i class="icon-time blue"></i>
						</span>
						<script>
							$(document).ready(function(){
								$('#created_on_time').timepicker({
									minuteStep: 30,
									showSeconds: false,
									showMeridian: false
								});
							});
						</script>
						
					</div>
				</div>
			</div>
			
	
			<?php if ( ! module_enabled('comments')): ?>
				<?php echo form_hidden('comments_enabled', 'no'); ?>
			<?php else: ?>
			
			<hr />
	
			<label><?php echo lang('blog:comments_enabled_label');?></label>
			<div class="row">
				<div class="col-xs-8 col-sm-11">
					<div class="input-group">
						
						<?php echo form_dropdown('comments_enabled', array(
							'no' => lang('global:no'),
							'1 day' => lang('global:duration:1-day'),
							'1 week' => lang('global:duration:1-week'),
							'2 weeks' => lang('global:duration:2-weeks'),
							'1 month' => lang('global:duration:1-month'),
							'3 months' => lang('global:duration:3-months'),
							'always' => lang('global:duration:always'),
						), $post->comments_enabled ? $post->comments_enabled : '3 months') ?>
						
					</div>
				</div>
			</div>
			
			<?php endif; ?>
			
		</div>
	</div>
	
</div>

<hr />

<div class="widget-box">
	<div class="widget-header">
		<h4><?php echo lang('blog:taxonomy_title'); ?></h4>
	</div>

	<div class="widget-body">
		<div class="widget-main">
			
			<label><?php echo lang('blog:category_label') ?></label>
			<div class="row">
				<div class="col-xs-8 col-sm-11">
					<div class="input-group">
						
						<?php echo form_dropdown('category_id', array(lang('blog:no_category_select_label')) + $categories, @$post->category_id, 'class="input-large"') ?>
						<br />
						[ <?php echo anchor('admin/blog/categories/create', lang('blog:new_category_label'), 'target="_blank"') ?> ]
						
					</div>
				</div>
			</div>
			
			<?php if ( ! module_enabled('keywords')): ?>
				<?php echo form_hidden('keywords'); ?>
			<?php else: ?>
			
			<hr />
			
			<label><?php echo lang('global:keywords') ?></label>
			<div class="row">
				<div class="col-xs-8 col-sm-11">
					<div class="input-group">
						
						<?php echo form_textarea(array('id' => 'keywords', 'name' => 'keywords', 'value' => $post->keywords, 'rows' => 2, 'class' => 'form_control')); ?>
						<script>
							$(document).ready(function(){
								var tag_input = $('#keywords');
								// make tag input field
								tag_input.tag(
								  {
									placeholder:tag_input.attr('placeholder'),
									//enable typeahead by specifying the source array
									source: function(query, process){
										return $.getJSON('<?php echo base_url(); ?>admin/keywords/autocomplete', { query: query }, function (data) {
											return process(data);
										});
									}
								  }
								);
							});
						</script>
						
					</div>
				</div>
			</div>
			
			<?php endif; ?>
			
		</div>
	</div>
	
</div>

</div>

</div>

<div class="row">

<div class="col-sm-12">

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $post->id; endif; ?>" />

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" value="save" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save'); ?>
		</button>
		
		<button class="btn btn-info" value="save_exit" name="btnAction" type="submit">
			<i class="icon-save bigger-110"></i>
			<?php echo lang('buttons:save_exit'); ?>
		</button>
		
		<a class="btn" href="<?php echo base_url().'admin/blog' ?>">
			<i class="icon-remove bigger-110"></i>
			<?php echo lang('buttons:cancel'); ?>
		</a>
	</div>
</div>

</div>

</div>

<?php echo form_close() ?>