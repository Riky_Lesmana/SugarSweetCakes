<div class="page-header">
	<h1><?php echo lang('blog:posts_title') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($blog) : ?>
	<?php echo $this->load->view('admin/partials/filters') ?>
	
	<hr />

	<?php echo form_open('admin/blog/action') ?>
		<div id="filter-stage">
			<?php echo $this->load->view('admin/tables/posts') ?>
		</div>
	<?php echo form_close() ?>
<?php else : ?>
	<div class="well"><?php echo lang('blog:currently_no_posts') ?></div>
<?php endif ?>