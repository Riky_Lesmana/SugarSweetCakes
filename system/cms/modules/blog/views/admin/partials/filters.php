<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="lighter"><?php echo lang('global:filters') ?></h5>
		
		<div class="widget-toolbar">
			<a data-action="collapse" href="#">
				<i class="icon-chevron-up"></i>
			</a>
		</div>
	</div>

	<div class="widget-body">
		<div class="widget-main">
			
			<?php echo form_open('', array('class' => 'form-online', 'method' => 'get'), array('f_module' => $module_details['slug'])) ?>
				
				<label for="f_status"><?php echo lang('blog:status_label') ?></label>
				<?php echo form_dropdown('f_status', array(0 => lang('global:select-all'), 'draft'=>lang('blog:draft_label'), 'live'=>lang('blog:live_label')), $this->input->get('f_status')) ?>
				
				<label for="f_category"><?php echo lang('blog:category_label') ?></label>
				<?php echo form_dropdown('f_category', array(0 => lang('global:select-all')) + $categories, $this->input->get('f_category')) ?>
				
				<label for="f_keywords"><?php echo lang('global:keywords') ?></label>
				<?php echo form_input('f_keywords', $this->input->get('f_keywords'), 'class="input-medium" style=""') ?>
				
				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
					<i class="icon-ok"></i>
					<?php echo lang('buttons:submit'); ?>
				</button>
				
				<button href="<?php echo current_url() . '#'; ?>" class="btn btn-xs" type="reset">
					<i class="icon-remove"></i>
					<?php echo lang('buttons:clear'); ?>
				</button>
				
			<?php echo form_close() ?>
			
		</div>
	</div>
</div>