<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show Latest blog in your site with a widget.
 *
 * Intended for use on cms pages. Usage :
 * on a CMS page add:
 *
 *     {widget_area('name_of_area')}
 *
 * 'name_of_area' is the name of the widget area you created in the  admin
 * control panel
 *
 * @author  Erik Berman
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Blog\Widgets
 */
class Widget_Blog_post extends Widgets
{

	public $author = 'Riky H. Lesmana';

	public $website = 'http://www.rikyhl.id';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Blog posts by Category',
		'id' => 'Post blog berdasarkan Kategori',
	);

	public $description = array(
		'en' => 'Display blog posts by category',
		'id' => 'Menampilkan posting blog berdasarkan kategori',
	);

	// build form fields for the backend
	// MUST match the field name declared in the form.php file
	public $fields = array(
		array(
			'field' => 'limit',
			'label' => 'Number of posts',
		),
		array(
			'field' => 'category',
			'label' => 'Category Name',
		)
	);

	public function form($options)
	{
		class_exists('Blog_categories_m') OR $this->load->model('blog/blog_categories_m');

		$options['limit'] = ( ! empty($options['limit'])) ? $options['limit'] : 5;
		$options['subtitle'] = ( ! empty($options['subtitle'])) ? $options['subtitle'] : '';
		$options['link'] = ( ! empty($options['link'])) ? $options['link'] : '';
		$options['category'] = ( ! empty($options['category'])) ? $options['category'] : '';
		$options['categories'] = $this->blog_categories_m->order_by('title')->get_all();
		

		return array(
			'options' => $options
		);
	}

	public function run($options)
	{
		// load the blog module's model
		class_exists('Blog_m') OR $this->load->model(array('blog/blog_m'));

		// sets default number of posts to be shown
		//$options['link'] = ( ! empty($category[0]['language'])) ? $category[0]['language']."/".$category[0]['translation']['slug'] : $options['link'];
		$options['limit'] = ( ! empty($options['limit'])) ? $options['limit'] : 5;
		$options['subtitle'] = ( ! empty($options['subtitle'])) ? $options['subtitle'] : NULL;

		// retrieve the records using the blog module's model
		$blog_widget = $this->blog_m
			->limit($options['limit'])
			->where('category_id', 0)
			->get_many_by(array('status' => 'live'));

		// returns the variables to be used within the widget's view
		return array('blog_post_by_cat' => $blog_widget, 'subtitle' => $options['subtitle'],'limit'=>$options['limit'], 'link'=>$options['link']);
	}

}
