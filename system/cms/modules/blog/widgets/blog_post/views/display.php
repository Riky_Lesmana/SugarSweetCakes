<?php echo (isset($subtitle) && $subtitle!='') ? '<h1>'.$subtitle.'</h1>' : '' ; ?>
<div class="row">
	<?php 
	$column = ceil(12/$limit);
	foreach($blog_post_by_cat as $post_widget){ 
	?>
	<div class="col-xs-12 col-md-<?php echo $column ?>">
		<div class="widget-item">
				<a href="<?php echo BASE_URL.$link.'/'.date('Y/m', $post_widget->created_on).'/'.$post_widget->slug; ?>">
					<h2 class="consult-title"><?php echo $post_widget->title ?></h2>
				</a>
				<div class="sample-thumb">
					<a href="<?php echo BASE_URL.$link.'/'.date('Y/m', $post_widget->created_on).'/'.$post_widget->slug; ?>">
						<img src="<?php echo BASE_URL."files/thumb/".$post_widget->image."/300/300/fit"; ?>" alt="<?php echo $post_widget->title ?>" title="<?php echo $post_widget->title ?>" style="max-width: 100%; max-height: 300px">
					</a>
				</div>
				<p><?php echo $post_widget->intro ?></p>
				<a href="<?php echo BASE_URL.$link.'/'.date('Y/m', $post_widget->created_on).'/'.$post_widget->slug; ?>" class="ticket-btn">Read More</a>
		</div>
	</div>
	<?php /**/ } ?>
</div>
