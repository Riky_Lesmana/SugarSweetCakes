<ol>
	<li class="even">
		<label>Subtitle</label>
		<?php echo form_input('subtitle', $options['subtitle']) ?>
	</li>
	<li class="even">
		<label>Link Name</label>
		<?php echo form_input('link', $options['link']) ?>
	</li>
	<li class="even">
		<label>Number to display</label>
		<?php echo form_input('limit', $options['limit']) ?>
	</li>
	<li class="even">
		<label>Category</label>
		<select name="category">
		<?php
		foreach($options['categories'] as $value){
		?>
			<option value="<?php echo $value->id; ?>" <?php echo ($options['category']==$value->id) ? 'selected' : '' ; ?>><?php echo $value->title; ?></option>
		<?php } ?>
		</select>
	</li>
</ol>