<?php if ($this->method == 'edit'): ?>
	<div class="page-header">
    	<h1><?php echo sprintf(lang('groups:edit_title'), $group->name) ?></h1>
	</div>
<?php else: ?>
	<div class="page-header">
    	<h1><?php echo lang('groups:add_title') ?></h1>
	</div>
<?php endif ?>

<?php echo form_open(uri_string(), 'class="form-horizontal"') ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:name');?> <span>*</span></label>
	<div class="col-sm-10"><?php echo form_input('description', $group->description);?></div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('groups:short_name');?> <span>*</span></label>
	

	<?php if ( ! in_array($group->name, array('user', 'admin'))): ?>
	<div class="col-sm-10">
		<?php echo form_input('name', $group->name);?>
	</div>

	<?php else: ?>
	<label class="col-sm-10">
		<?php echo $group->name ?>
	</label>
	<?php endif ?>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>
	
<?php echo form_close();?>