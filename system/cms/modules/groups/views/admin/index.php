<div class="page-header">
	<h1><?php echo $module_details['name'] ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ($groups): ?>
	<table class="table table-striped table-bordered table-hover" cellspacing="0">
		<thead>
			<tr>
				<th width="40%"><?php echo lang('groups:name');?></th>
				<th><?php echo lang('groups:short_name');?></th>
				<th width="300"></th>
			</tr>
		</thead>
		
		<tbody>
		<?php foreach ($groups as $group):?>
			<tr>
				<td><?php echo $group->description ?></td>
				<td><?php echo $group->name ?></td>
				<td class="actions">
				<?php echo anchor('admin/groups/edit/'.$group->id, lang('buttons:edit'), 'class="btn btn-xs btn-info edit"') ?>
				<?php if ( ! in_array($group->name, array('user', 'admin'))): ?>
					<?php echo anchor('admin/groups/delete/'.$group->id, lang('buttons:delete'), 'class="confirm btn btn-xs btn-danger delete"') ?>
				<?php endif ?>
				<?php echo anchor('admin/permissions/group/'.$group->id, lang('permissions:edit').' &rarr;', 'class="btn btn-xs btn-info edit"') ?>
				</td>
			</tr>
		<?php endforeach;?>
		</tbody>
	</table>
	
	<?php $this->load->view('admin/partials/pagination') ?>

<?php else: ?>
	<div class="well"><?php echo lang('groups:no_groups');?></div>
<?php endif;?>