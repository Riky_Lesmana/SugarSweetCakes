<div class="page-header">
    <h1><?php echo lang('nav:group_create_title');?></h1>
</div>

<?php echo form_open('admin/navigation/groups/create', 'class="form-horizontal"') ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:title');?> <span>*</span></label>
	<div class="col-sm-10"><?php echo form_input('title', $navigation_group['title'], 'id="title" class="text"') ?></div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:slug');?> <span>*</span></label>
	<div class="col-sm-10">
		<?php echo form_input('abbrev', $navigation_group['abbrev'], 'id="slug" class="text"') ?>
		<script>
			$('#title').slugify({ slug: '#slug', type: '-' });
		</script>
	</div>
</div>
	
<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">    
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>
	
<?php echo form_close() ?>
    
