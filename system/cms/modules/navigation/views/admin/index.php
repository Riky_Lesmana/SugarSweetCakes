<div class="page-header">
	<h1><?php echo lang('nav:navigation_label') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<?php if ( ! empty($groups)): ?>
	<?php foreach ($groups as $group): ?>

		<div rel="<?php echo $group->id ?>" class="group-<?php echo $group->id ?> box widget-box">
			<div class="widget-header">
				<h5 title="<?php echo lang('nav:abbrev_label').': '.$group->abbrev ?>"><?php echo $group->title;?></h5>
				
				<div class="widget-toolbar">
					<?php echo anchor('admin/navigation/create/'.$group->id, '<i class="icon-plus blue"></i>', 'rel="'.$group->id.'" class="add ajax button"') ?>
					
					<?php echo anchor('admin/navigation/groups/delete/'.$group->id, '<i class="icon-trash red"></i>', array('class' => 'confirm',  'title' => lang('nav:group_delete_confirm'))) ?>

					<a data-action="collapse" href="#">
						<i class="icon-chevron-up"></i>
					</a>
				</div>
			</div>
			
			<div class="widget-body">
				<div class="widget-main">
					<?php if ( ! empty($navigation[$group->id])): ?>

					<section class="item">
						<div class="row">
							<div class="col-sm-6">
								<div id="link-list">
									<ul class="sortable">
										<?php echo tree_builder($navigation[$group->id], '<li id="link_{{ id }}"><div><a href="#" rel="'.$group->id.'" alt="{{ id }}">{{ title }}</a></div>{{ children }}</li>') ?>
									</ul>
								</div>
							</div>

							<div class="col-sm-6">
								<div id="link-details" class="well group-<?php echo $group->id ?>">

									<?php echo lang('nav:tree_explanation') ?>

								</div>
							</div>
						</div>
					</section>

					<?php else:?>

					<section class="item">
						<div class="row">
							<div class="col-sm-6">
								<div id="link-list" class="empty">
									<ul class="sortable">

										<p><?php echo lang('nav:group_no_links');?></p>

									</ul>
								</div>
							</div>

							<div class="col-sm-6">
								<div id="link-details" class="well group-<?php echo $group->id ?>">

									<?php echo lang('nav:tree_explanation') ?>

								</div>
							</div>
						</div>
					</section>
				
				<?php endif ?>
				</div>
			</div>
		</div>

	<?php endforeach ?>

<?php else: ?>
	<div class="blank-slate">
		<p><?php echo lang('nav:no_groups');?></p>
	</div>
<?php endif ?>