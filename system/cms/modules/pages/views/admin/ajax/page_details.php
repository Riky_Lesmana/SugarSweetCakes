<input id="page-id" type="hidden" value="<?php echo $page->id ?>" />
<input id="page-uri" type="hidden" value="<?php echo ( ! empty($page->uri)) ? $page->uri : $page->slug ?>" />

<div class="widget-main form-horizontal">
	
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b>ID:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo $page->id ?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('pages:status_label') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo lang("pages:{$page->status}_label") ?></p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('global:slug') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><a href="<?php echo !empty($page->uri) ? $page->uri : $page->slug ?>" target="_blank">
			/<?php echo !empty($page->uri) ? $page->uri : $page->slug ?>
			</a></p>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('pages:type_id_label') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo $page->page_type_title; ?></p>
		</div>
	</div>
</div>

<!-- Meta data tab -->
<?php if ($page->meta_title or $page->meta_keywords or $page->meta_description): ?>
<div class="widget-main form-horizontal">
	
	<?php if ($page->meta_title): ?>
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('pages:meta_title_label') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo $page->meta_title ?></p>
		</div>
	</div>
	<?php endif ?>
	
	<?php if ($page->meta_keywords): ?>
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('pages:meta_keywords_label') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo $page->meta_keywords ?></p>
		</div>
	</div>
	<?php endif ?>
	
	<?php if ($page->meta_description): ?>
	<div class="form-group">
		<label class="col-xs-4 no-padding-right">
			<b><?php echo lang('pages:meta_desc_label') ?>:</b>
		</label>
		<div class="col-xs-8">
			<p class="form-control-static"><?php echo $page->meta_description ?></p>
		</div>
	</div>
	<?php endif ?>
	
</div>	
<?php endif ?>

<div class="col-xs-12">
	<?php 

		if ($this->db->count_all('page_types') > 1)
		{
			echo anchor('admin/pages/choose_type?parent='.$page->id, lang('pages:create_label'), 'class="btn btn-sm btn-default"');
		}
		else
		{
			$type_id = $this->db->select('id')->limit(1)->get('page_types')->row()->id;
			echo anchor('admin/pages/create?parent='.$page->id.'&page_type='.$type_id, lang('pages:create_label'), 'class="btn btn-sm btn-default"');
		}

	?>
	<?php echo anchor('admin/pages/duplicate/'.$page->id, lang('pages:duplicate_label'), 'class="btn btn-sm btn-default"') ?>
	<?php echo anchor('admin/pages/edit/'.$page->id, lang('global:edit'), 'class="btn btn-sm btn-default"') ?>
	<?php echo anchor('admin/pages/delete/'.$page->id, lang('global:delete'), 'class="confirm btn btn-sm btn-danger"') ?>
</div>

<!-- inline scripts related to this page -->
<script type="text/javascript">

	/****************************
	 *	CONFIRMATION DIALOG
	 ****************************/
	$(document).ready(function(){
		$(".confirm").on(ace.click_event, function(event) {
			event.preventDefault();
			var link = $(this).attr('href');;
			bootbox.confirm("<?php echo lang('global:dialog:confirm_message'); ?>", function(result) {
				if(result) {
					window.location.href = link;
				}
			});
		});
	});
</script>