<div class="page-header">
	<h1><?php echo lang('pages:list_title') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<div class="col-sm-8">
	
	<section class="item">
		<div class="content">
			<div id="page-list">
			<ul class="sortable">
				<?php echo tree_builder($pages, '<li id="page_{{ id }}"><div><a href="'.site_url('admin/pages/edit/{{ id }}').'" class="{{ status }}" rel="{{ id }}">{{ title }}</a></div>{{ children }}</li>') ?>
			</ul>
			</div>
		</div>
	</section>
</div>

<div class="col-sm-4">
	
	<div class="widget-box">
		<div class="widget-header">
			<h4><?php echo lang('pages:tree_explanation_title') ?></h4>
		</div>

		<div id="page-details" class="widget-body">
			<div class="widget-main"><?php echo lang('pages:tree_explanation') ?></div>
		</div>
	</div>
	
</div>