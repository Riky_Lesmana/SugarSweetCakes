<div class="page-header">
	<h1><?php echo lang('global:delete'); ?> <?php echo lang('page_types:list_title_sing'); ?></h1>
</div>

<?php echo form_open($this->uri->uri_string()); ?>
	
	<p><?php echo sprintf(lang('page_types:delete_message'), $num_of_pages); ?></p>

	<?php if ($delete_stream): ?>
		<p><?php echo sprintf(lang('page_types:delete_streams_message'), $stream_name); ?></p>
	<?php endif; ?>

	<input type="hidden" name="do_delete" value="y" />
	
	<div class="clearfix form-actions">
		<div class="col-md-offset-2 col-md-10">
			<button type="submit" class="btn btn-danger"><?php echo lang('global:delete'); ?></button>
			<a href="<?php echo site_url('admin/pages/types'); ?>" class="btn"><?php echo lang('cancel_label'); ?></a>
		</div>
	</div>

</form>