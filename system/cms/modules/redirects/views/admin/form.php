<div class="page-header">
<?php if($this->method == 'add'): ?>
	<h1><?php echo lang('redirects:add_title');?></h1>
<?php else: ?>
	<h1><?php echo lang('redirects:edit_title');?></h1>
<?php endif ?>
</div>

<?php echo form_open(uri_string(), 'class="form-horizontal"') ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('redirects:type');?></label>
	<div class="col-sm-10">
		<?php echo form_dropdown('type', array('301' => lang('redirects:301'), '302' => lang('redirects:302')), !empty($redirect['type']) ? $redirect['type'] : '302');?>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('redirects:from');?></label>
	<div class="col-sm-10">
		<?php echo form_input('from', str_replace('%', '*', $redirect['from']));?>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('redirects:to');?></label>
	<div class="col-sm-10">
		<?php echo form_input('to', $redirect['to']);?>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>

<?php echo form_close() ?>