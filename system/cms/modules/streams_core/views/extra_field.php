<div class="form-group">
	
	<label class="col-sm-2 control-label no-padding-right"><?php echo $input_name ?></label>
	
	<div class="col-sm-10">
		<?php echo $input ?>
		
		<?php if( isset($instructions) and $instructions ): ?>
		<span class="help-inline">
			<span class="middle"><?php echo $instructions ?></span>
		</span>
		<?php endif ?>
	</div>
	
</div>