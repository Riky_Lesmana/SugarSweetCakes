<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users:role_manage_users'] = 'Manage users';
$lang['users:role_manage_user_profile_fields'] = 'Manage user profile fields';
$lang['users:role_create_users'] = 'Create users';
$lang['users:role_view_all_account'] = 'View all account data';
$lang['users:role_view_own_account'] = 'View own account data';
$lang['users:role_edit_all_account'] = 'Edit all account data';
$lang['users:role_edit_own_account'] = 'Edit own account data';
$lang['users:role_view_all_profile'] = 'View all profile';
$lang['users:role_view_own_profile'] = 'View own profile';
$lang['users:role_edit_all_profile'] = 'Edit all profile';
$lang['users:role_edit_own_profile'] = 'Edit own profile';
$lang['users:role_delete_users'] = 'Delete users';
$lang['users:administer_all_account'] = 'Administer all account data';