<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['users:role_manage_users'] = 'Pengaturan pengguna';
$lang['users:role_manage_user_profile_fields'] = 'Mengatur fields pada profil pengguna';
$lang['users:role_create_users'] = 'Menambahkan pengguna';
$lang['users:role_view_all_account'] = 'Melihat semua data akun';
$lang['users:role_view_own_account'] = 'Melihat data akun sendiri';
$lang['users:role_edit_all_account'] = 'Mengedit semua data akun';
$lang['users:role_edit_own_account'] = 'Mengedit data akun sendiri';
$lang['users:role_view_all_profile'] = 'Melihat profil semua pengguna';
$lang['users:role_view_own_profile'] = 'Melihat profil sendiri';
$lang['users:role_edit_all_profile'] = 'Mengedit profil semua pengguna';
$lang['users:role_edit_own_profile'] = 'Mengedit profil sendiri';
$lang['users:role_delete_users'] = 'Menghapus pengguna';
$lang['users:administer_all_account'] = 'Mengadministrasi semua akun';