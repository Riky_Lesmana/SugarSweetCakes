<div class="page-header">
	<?php if ($this->method === 'create'): ?>
		<h1><?php echo lang('user:add_title') ?></h1>
	<?php else: ?>
		<h1><?php echo sprintf(lang('user:edit_title'), $member->username) ?></h1>
	<?php endif ?>
</div>

<?php if ($this->method === 'create'): ?>
	<?php echo form_open_multipart(uri_string(), 'class="form-horizontal" autocomplete="off"') ?>
<?php else: ?>
	<?php echo form_open_multipart(uri_string(), 'class="form-horizontal"') ?>
	<?php echo form_hidden('row_edit_id', isset($member->row_edit_id) ? $member->row_edit_id : $member->profile_id); ?>
<?php endif ?>
	
<!-- Content tab -->
<?php if(group_has_role('users', 'edit_all_account') OR (group_has_role('users', 'edit_own_account') AND $this->current_user->id == isset($member->id) ? $member->id : $member->row_edit_id)){ ?>

<fieldset>
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('user_account_data_label'); ?></h3>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('global:email') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('email', $member->email, 'id="email" class="col-xs-10 col-sm-5"') ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:username') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('userad', $member->username, 'id="username" class="col-xs-10 col-sm-5" disabled');
			echo form_hidden('username', $member->username);
			 ?>
		</div>
	</div>
<?php if(group_has_role('users', 'administer_all_account')){ ?>
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:group_label') ?></label>
		<div class="col-sm-10">
			<?php echo form_dropdown('group_id', array(0 => lang('global:select-pick')) + $groups_select, $member->group_id, 'id="group_id"') ?>
		</div>
	</div>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('user:activate_label') ?></label>
		<div class="col-sm-10">
			<?php $options = array(0 => lang('user:do_not_activate'), 1 => lang('user:active'), 2 => lang('user:send_activation_email')) ?>
			<?php echo form_dropdown('active', $options, $member->active, 'id="active" class="col-xs-10 col-sm-5"') ?>
		</div>
	</div>
<?php } ?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right">
			<?php echo lang('global:password') ?>
			<?php if ($this->method == 'create'): ?> <span>*</span><?php endif ?>
		</label>
		<div class="col-sm-10">
			<?php echo form_password('password', '', 'id="password" autocomplete="off" class="col-xs-10 col-sm-5"') ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right"><?php echo lang('profile_display_name') ?> <span>*</span></label>
		<div class="col-sm-10">
			<?php echo form_input('display_name', $display_name, 'id="display_name" class="col-xs-10 col-sm-5"') ?>
		</div>
	</div>
</fieldset>	

<?php }else{ 

echo form_hidden('email', $member->email);
echo form_hidden('username', $member->username);
echo form_hidden('display_name', $display_name);
echo form_hidden('group_id', $member->group_id);
echo form_hidden('active', $member->active);
echo form_hidden('password', '');

} //if ?>

<?php //if(group_has_role('users', 'edit_all_profile') OR (group_has_role('users', 'edit_own_profile') AND $this->current_user->id == $member->id)){ ?>
<?php if(TRUE){ ?>

<fieldset>
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('profile_user_details_label'); ?></h3>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="photo"><?php echo lang('user:photo') ?></label>
		<div class="col-xs-10">
			<?php echo form_upload('photo', '', 'id="photo" class="col-xs-10 col-sm-5"') ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="facebook"><?php echo lang('user:facebook') ?></label>
		<div class="col-xs-10">
			<?php echo form_input('facebook', $member->facebook, 'id="facebook" class="col-xs-10 col-sm-5" placeholder="http://www.facebook.com/username"') ?>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="twitter"><?php echo lang('user:twitter') ?></label>
		<div class="col-xs-10">
			<?php echo form_input('twitter', $member->twitter, 'id="twitter" class="col-xs-10 col-sm-5" placeholder="http://www.twitter.com/username"') ?>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="biografi"><?php echo lang('user:biografi') ?></label>
		<div class="col-xs-10">
			<?php echo form_textarea('biografi', $member->biografi, 'id="biografi" class="col-xs-10 col-sm-5"') ?>
			
		</div>
	</div>

	<?php 
	$counter = 0;
	foreach($profile_fields as $field): 
	?>
	<?php if($counter == 0){ ?>
	<h3 class="header smaller lighter blue col-sm-12"><?php echo lang('profile_user_details_label'); ?></h3>
	<?php 
	$counter++;
	} //if 
	?>
	
	<div class="form-group">
		<label class="col-sm-2 control-label no-padding-right">
			<?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?>
			<?php if ($field['required']){ ?> <span>*</span><?php } ?>
		</label>
		<div class="col-sm-10">
			<?php echo $field['input'] ?>
		</div>
	</div>
	<?php endforeach ?>
</fieldset>

<?php }else{



} //if ?>

<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-sm btn-info" value="save_exit" name="btnAction" type="submit">
			<span><?php echo lang('buttons:save_exit'); ?></span>
		</button>
		
		<?php if ($this->method == 'create'){ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php }else{ ?>
		
		<a class="btn btn-sm btn-info cancel" href="<?php echo base_url(); ?>admin/users/view/<?php echo isset($member->id) ? $member->id : $member->row_edit_id; ?>"><?php echo lang('buttons:cancel'); ?></a>
		
		<?php } ?>
	</div>
</div>

<?php echo form_close() ?>

<script type="text/javascript">
	jQuery(function($) {
		$(".chosen-select").chosen();
		$('#chosen-multiple-style').on('click', function(e){
			var target = $(e.target).find('input[type=radio]');
			var which = parseInt(target.val());
			if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
			 else $('#form-field-select-4').removeClass('tag-input-style');
		});
	});
</script>
