<div class="page-header">
	<h1><?php echo lang('user:list_title') ?></h1>
	
	<?php file_partial('shortcuts'); ?>
</div>

<div class="tabbable tab-link">
	<ul class="nav nav-tabs">
		<?php
		$class = '';
		if($this->input->get('f_group') == NULL){
			$class = 'active';
		}
		?>
		<li class="<?php echo $class; ?>">
			<a href="<?php echo site_url('admin/users'); ?>"><?php echo lang('global:select-all'); ?></a>
		</li>

		<?php foreach($groups_select as $id => $group){
		$active_group_id = $this->input->get('f_group');

		$class = '';
		if($active_group_id == $id) $class = 'active';
		?>

		<li class="<?php echo $class; ?>">
			<a href="<?php echo site_url('admin/users').'?f_group='.$id; ?>"><?php echo $group; ?></a>
		</li>
		<?php } ?>
	</ul>
</div>

<?php template_partial('filters') ?>

<hr />
	
<?php echo form_open('admin/users/action') ?>

	<div id="filter-stage">
		<?php template_partial('tables/users') ?>
	</div>

	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('activate', 'delete') )) ?>
	</div>

<?php echo form_close() ?>