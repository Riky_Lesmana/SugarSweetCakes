<div class="widget-box">
	<div class="widget-header widget-header-small">
		<h5 class="lighter"><?php echo lang('global:filters') ?></h5>
		
		<div class="widget-toolbar">
			<a data-action="collapse" href="#">
				<i class="icon-chevron-up"></i>
			</a>
		</div>
	</div>

	<div class="widget-body">
		<div class="widget-main">
	
		<?php echo form_open('',  array('class' => 'form-inline', 'method' => 'get')) ?>
			<?php echo form_hidden('f_module', $module_details['slug']) ?>			
			<div class="form-group">
				<label><?php echo lang('user:active', 'f_active') ?></label>
				<?php echo form_dropdown('f_active', array(0 => lang('global:select-all'), 1 => lang('global:yes'), 2 => lang('global:no') ), $this->input->get('f_active')) ?>
			</div>

			<?php
			$active_group_id = $this->input->get('f_group');
			echo form_hidden('f_group', $active_group_id);
			?>
			
			<div class="form-group">
				<label for="f_keywords"><?php echo lang('global:keywords') ?></label>
				<?php echo form_input('f_keywords', $this->input->get('f_keywords')) ?></li>
			</div>
			
			<button href="<?php echo current_url() . '#'; ?>" class="btn btn-primary btn-xs" type="submit">
				<i class="icon-ok"></i>
				<?php echo lang('buttons:filter'); ?>
			</button>
			
			<a href="<?php echo site_url('admin/users').'?f_group='.$active_group_id; ?>" class="btn btn-xs">
				<i class="icon-remove"></i>
				<?php echo lang('buttons:clear'); ?>
			</a>
			
		<?php echo form_close() ?>
		</div>
	</div>
</div>