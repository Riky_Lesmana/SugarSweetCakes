<style type="text/css">
.light-gray-photo {
	color: #dedede;
	font-size: 200px;
	margin: 50px 50px;
}
</style>
<div class="page-header">
	<h1><?php echo $user->display_name; ?></h1>
	
	<?php if(group_has_role('users', 'edit_all_profile') OR group_has_role('users', 'edit_all_account')){ ?>
	
	<div class="btn-group" style="float: right; margin-top: -28px; margin-right: 10px">
		<a href="<?php echo site_url('admin/users'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>

		<a href="<?php echo base_url(); ?>admin/users/edit/<?php echo $user->id; ?>" class="btn btn-yellow btn-sm">
			<i class="icon-edit"></i>										
			<span class="no-text-shadow"><?php echo lang('user:edit_label'); ?></span>
		</a>
	</div>
	
	<?php }elseif((group_has_role('users', 'edit_own_profile') OR group_has_role('users', 'edit_own_account')) AND $user->id == $this->current_user->id){ ?>
	
	<div class="btn-group" style="float: right; margin-top: -28px; margin-right: 10px">
		<a href="<?php echo site_url('admin/users'); ?>" class="btn btn-sm btn-yellow">
			<i class="icon-arrow-left"></i>
			<?php echo lang('organization:back') ?>
		</a>
		
		<a href="<?php echo base_url(); ?>admin/users/edit/<?php echo $user->id; ?>" class="btn btn-yellow btn-sm">
			<i class="icon-edit"></i>										
			<span class="no-text-shadow"><?php echo lang('user:edit_label'); ?></span>
		</a>
	</div>
		
	<?php } ?>
	
</div>

<div class="row">
	<div class="col-xs-12 col-sm-3 center">
		<div>
			<span class="profile-picture">
			<?php if($user->photo!=""){
			?>
				<img src="<?php echo base_url();?>files/thumb/<?php echo $user->photo; ?>/260/315" />
			<?php
			}else{
			?>
				<i class="icon-user light-gray-photo bigger-210"></i>
			<?php	
			}
			?>
			</span>
		</div>

		<div class="space-6"></div>

		<div class="profile-contact-info">
			<div class="profile-contact-links align-left">
			
				<div class="profile-status">
				<?php if($user->active){ ?>
					<i class="icon-ok bigger-120 green"></i>&nbsp;	
					<?php echo lang('user:active') ?>
				<?php }else{ ?>
					<i class="icon-remove light-orange bigger-110"></i>
					<span><?php echo lang('user:not_active'); ?></span>
				<?php } ?>
				</div>
				
				<div class="profile-status">
					<i class="icon-calendar bigger-120 pink"></i>&nbsp;	
					<span><?php echo lang('user:joined_label');?> <?php echo format_date($user->created_on) ?></span>
				</div>
				
				<div class="profile-status">
					<i class="icon-time bigger-120 blue"></i>&nbsp;	
					<span><?php echo lang('user:last_visit_label');?> <?php echo ($user->last_login > 0 ? format_date($user->last_login) : lang('user:never_label')) ?></span>
				</div>
				
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-9">
		
		<?php if(group_has_role('users', 'view_all_account') OR (group_has_role('users', 'view_own_account') AND $this->current_user->id == $user->id)){ ?>
		
		<h4 class="blue">
			<span class="middle"><?php echo lang('user_account_data_label'); ?></span>
		</h4>

		<div class="profile-user-info">
			<div class="profile-info-row">
				<div class="profile-info-name">
					<?php echo lang('global:email') ?>
					<i class="icon-envelope light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
					<span><?php echo $user->email; ?></span>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name">
					<?php echo lang('user:username') ?>
					<i class="icon-user light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
					<span><?php echo $user->username; ?></span>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name">
					<?php echo lang('profile_display_name') ?>
					<i class="icon-user light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
					<span><?php echo $user->display_name; ?></span>
				</div>
			</div>
		</div>
		
		<?php } //if ?>
		
		<?php if(group_has_role('users', 'view_all_profile') OR (group_has_role('users', 'view_own_profile') AND $this->current_user->id == $user->id)){ ?>
		
		<h4 class="blue">
			<span class="middle"><?php echo lang('profile_user_details_label'); ?></span>
		</h4>

		<div class="profile-user-info">

			<div class="profile-info-row">
				<div class="profile-info-name">
					Facebook
					<i class="icon-facebook light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
				<?php if($user->facebook!=''){ ?>
					<span><?php echo $user->facebook; ?></span>
				<?php }else{ ?>
					<span> - </span>
				<?php } ?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name">
					Twitter
					<i class="icon-twitter light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
				<?php if($user->twitter!=''){ ?>
					<span><?php echo $user->twitter; ?></span>
				<?php }else{ ?>
					<span> - </span>
				<?php } ?>
				</div>
			</div>

			<div class="profile-info-row">
				<div class="profile-info-name">
					Tentang Saya
					<i class="icon-book light-orange bigger-110"></i>
				</div>

				<div class="profile-info-value">
				<?php if($user->biografi!=''){ ?>
					<span style="white-space: pre-wrap;"><?php echo $user->biografi; ?></span>
				<?php }else{ ?>
					<span> - </span>
				<?php } ?>
				</div>
			</div>
		
		</div>
		
		<?php } //if ?>
		
	</div><!-- /span -->
</div>