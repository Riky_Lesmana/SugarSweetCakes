<?php if (!empty($users)): ?>
	<table border="0" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th with="30" class="align-center"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
				<th><?php echo lang('user:name_label');?></th>
				<th class="collapse"><?php echo lang('user:email_label');?></th>

				<?php if($this->input->get('f_group') == NULL){ ?>
				<th><?php echo lang('user:group_label');?></th>
				<?php } ?>

				<? /*<th><?php echo lang('organization:units:plural');?></th>*/ ?>
				<th class="collapse"><?php echo lang('user:active') ?></th>
				<th class="collapse"><?php echo lang('user:joined_label');?></th>
				<th class="collapse"><?php echo lang('user:last_visit_label');?></th>
				<th width="200"></th>
			</tr>
		</thead>
		
		<tbody>
			<?php $link_profiles = Settings::get('enable_profiles') ?>
			<?php foreach ($users as $member): ?>
				<tr>
					<td class="align-center"><?php echo form_checkbox('action_to[]', $member->id) ?></td>
					<td>
					<?php if ($link_profiles) : ?>
						<?php echo anchor('admin/users/preview/' . $member->id, $member->display_name, 'target="_blank" class="modal-large"') ?>
					<?php else: ?>
						<?php echo $member->display_name ?>
					<?php endif ?>
					</td>
					<td class="collapse"><?php echo mailto($member->email) ?></td>

					<?php if($this->input->get('f_group') == NULL){ ?>
					<td><?php echo $member->group_name ?></td>
					<?php } ?>

					<?php $units = $this->organization->get_membership_by_user($member->id); ?>
					<?php
					/* Nonaktifkan Satuan Organisasi
					if($units['total'] > 0){
						$unit_str = array();
						foreach($units['entries'] as $unit){
							$unit_str[] = '<a href="'.site_url('admin/organization/units/view/'.$unit['membership_unit']['id']).'">'.$unit['membership_unit']['unit_name'].'</a>';
						}
					}else{
						$unit_str = array();
					}
					?>
					<td><?php echo implode(', ', $unit_str); ?></td>*/ ?>

					<td class="collapse"><?php echo $member->active ? lang('global:yes') : lang('global:no')  ?></td>
					<td class="collapse"><?php echo format_date($member->created_on) ?></td>
					<td class="collapse"><?php echo ($member->last_login > 0 ? format_date($member->last_login) : lang('user:never_label')) ?></td>
					<td class="actions">
						<?php 
						if(group_has_role('users', 'view_all_account') OR group_has_role('users', 'view_all_profile')){
							echo anchor('admin/users/view/' . $member->id, lang('global:view'), array('class'=>'btn btn-xs btn-info'));
						}elseif((group_has_role('users', 'view_own_account') OR group_has_role('users', 'view_own_profile')) AND $member->id == $this->current_user->id){
							echo anchor('admin/users/view/' . $member->id, lang('global:view'), array('class'=>'btn btn-xs btn-info'));
						}
						?>
						
						<?php 
						if(group_has_role('users', 'edit_all_account') OR group_has_role('users', 'edit_all_profile')){
							echo anchor('admin/users/edit/' . $member->id, lang('global:edit'), array('class'=>'btn btn-xs btn-info edit'));
						}elseif((group_has_role('users', 'edit_own_account') OR group_has_role('users', 'edit_own_profile')) AND $member->id == $this->current_user->id){
							echo anchor('admin/users/edit/' . $member->id, lang('global:edit'), array('class'=>'btn btn-xs btn-info edit'));
						}
						?>
						
						<?php 
						if(group_has_role('users', 'delete_users')){
							echo anchor('admin/users/delete/' . $member->id, lang('global:delete'), array('class'=>'confirm btn btn-xs btn-danger delete'));
						}
						?>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	
	<?php $this->load->view('admin/partials/pagination') ?>
<?php endif ?>