<h2 class="page-title" id="page_title"><?php echo lang('user:register_header') ?></h2>

<p>
	<span id="active_step"><?php echo lang('user:register_step1') ?></span> -&gt;
	<span><?php echo lang('user:register_step2') ?></span>
</p>

<?php if ( ! empty($error_string)):?>
<!-- Woops... -->
<div class="error-box">
	<?php echo $error_string;?>
</div>
<?php endif;?>

<?php echo form_open_multipart('register', array('id' => 'register')) ?>

<div class="form-horizontal">
	<?php if ( ! Settings::get('auto_username')): ?>
	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="username"><?php echo lang('user:username') ?></label>

		<div class="col-xs-10">
			<input name="username" type="text" value="<?php echo $_user->username ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>
	<?php endif ?>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="email"><?php echo lang('global:email') ?></label>

		<div class="col-xs-10">
			<input name="email" type="text" value="<?php echo $_user->email ?>" class="col-xs-10 col-sm-5" id="" />
			<?php echo form_input('d0ntf1llth1s1n', ' ', 'class="default-form" style="display:none"') ?>
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="password"><?php echo lang('global:password') ?></label>

		<div class="col-xs-10">
			<input name="password" type="password" value="<?php echo $_user->password ?>" class="col-xs-10 col-sm-5" id="" />
		</div>
	</div>

	<?php foreach($profile_fields as $field) { if($field['required'] and $field['field_slug'] != 'display_name') { ?>
	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="<?php echo $field['field_slug'] ?>"><?php echo (lang($field['field_name'])) ? lang($field['field_name']) : $field['field_name'];  ?></label>

		<div class="col-xs-10">
			<?php echo $field['input'] ?>
		</div>
	</div>
	<?php } } ?>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="photos"><?php echo lang('user:photo') ?></label>
		<div class="col-xs-10">
			<input name="photos" type="file" value="" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="facebook"><?php echo lang('user:facebook') ?></label>
		<div class="col-xs-10">
			<input name="facebook" type="text" value="<?php if(isset($_POST['facebook'])) echo $_POST['facebook']; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="twitter"><?php echo lang('user:twitter') ?></label>
		<div class="col-xs-10">
			<input name="twitter" type="text" value="<?php if(isset($_POST['twitter'])) echo $_POST['twitter']; ?>" class="col-xs-10 col-sm-5" id="" />
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="biografi"><?php echo lang('user:biografi') ?></label>
		<div class="col-xs-10">
			<textarea name="biografi" rows="6" class="col-xs-10 col-sm-5" id=""><?php if(isset($_POST['biografi'])) echo $_POST['biografi']; ?></textarea>
			
		</div>
	</div>

	<div class="form-group">
		<label class="col-xs-2 control-label no-padding-right" for="biografi"></label>
		<div class="col-xs-10 text-left">
			<input type="submit" value="Register" class="btn btn-primary btn-xs" id="" />
			
		</div>
	</div>

</div>


<?php echo form_close() ?>
<script type="text/javascript">
$(document).ready(function() {
	$("input[name='nick_name']").addClass('col-xs-10').addClass('col-md-5').removeClass('form-control');
});
</script>