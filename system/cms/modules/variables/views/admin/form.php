<div class="page-header">
	<?php if ($this->method == 'create'): ?>
	<h1><?php echo lang('variables:create_title');?></h1>
	<?php else: ?>
	<h1><?php echo sprintf(lang('variables:edit_title'), $variable->name);?></h1>
	<?php endif ?>
</div>

<?php echo form_open($this->uri->uri_string(), 'class="form-horizontal" id="variables"') ?>
<?php if ($this->method == 'edit') echo form_hidden('variable_id', $variable->id) ?>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('name_label');?> <span>*</span></label>
	<div class="col-sm-10"><?php echo  form_input('name', $variable->name) ?></div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('variables:data_label');?> <span>*</span></label>
	<div class="col-sm-10"><?php echo  form_input('data', $variable->data) ?></div>
</div>
		
<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel') )) ?>
	</div>
</div>

</div>

<?php echo form_close() ?>