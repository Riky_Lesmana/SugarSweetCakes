<div class="page-header">
	<h1><?php echo lang(sprintf('widgets:%s_area', ($this->method === 'create' ? 'add' : 'edit'))) ?></h1>
</div>

<?php echo form_open(uri_string(), 'class="form-horizontal"') ?>
	
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('widgets:widget_area_title') ?></label>
	<div class="col-sm-10">
		<?php echo form_input('title', $area->title, 'class="new-area-title"') ?>
		<span class="required-icon tooltip"><?php echo lang('required_label') ?></span>
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right"><?php echo lang('widgets:widget_area_slug') ?></label>
	<div class="col-sm-10">
		<?php echo form_input('slug', $area->slug, 'class="new-area-slug"') ?>
		<span class="required-icon tooltip"><?php echo lang('required_label') ?></span>
	</div>
</div>

<div class="clearfix form-actions">
	<div class="col-md-offset-2 col-md-9">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
	</div>
</div>

<?php echo form_close() ?>