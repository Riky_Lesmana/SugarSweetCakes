<?php echo form_open(uri_string(), 'class="widget-form"') ?>

	<div class="page-header">
		<h1><?php echo $widget->title ?></h1>
	</div>

	<?php echo form_hidden('widget_id', $widget->id) ?>
	<?php echo isset($widget->instance_id) ? form_hidden('widget_instance_id', $widget->instance_id) : null ?>
	<?php echo isset($error) && $error ? $error : null ?>
	
	<div class="form-group">
		<label class="form-field-8"><?php echo lang('widgets:instance_title') ?></label>
		<br />
		<?php echo form_input('title', set_value('title', isset($widget->instance_title) ? $widget->instance_title : '')) ?>
		<span class="required-icon tooltip"><?php echo lang('required_label') ?></span>
	</div>

	<div class="form-group">
		<?php echo form_checkbox('show_title', true, isset($widget->options['show_title']) ? $widget->options['show_title'] : false) ?>
		<label><?php echo lang('widgets:show_title') ?></label>
	</div>

	<?php if (isset($widget_areas)): ?>
	<div class="form-group">
		<label><?php echo lang('widgets:widget_area') ?>:</label>
		<br />
		<?php echo form_dropdown('widget_area_id', $widget_areas, $widget->widget_area_id) ?>
	</div>
	<?php else: ?>
		<?php echo form_hidden('widget_area_id', $area_id) ?>
	<?php endif ?>
	
	<?php echo $form ? $form : null ?>

	<div class="clearfix form-actions">
		<div class="col-md-offset-2 col-md-9">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))) ?>
		</div>
	</div>
<?php echo form_close() ?>