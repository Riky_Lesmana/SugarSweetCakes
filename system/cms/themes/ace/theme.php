<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Ace extends Theme {

    public $name			= 'Ace - Responsive Admin Theme';
    public $author			= 'EasyMind';
    public $author_website	= 'https://wrapbootstrap.com/theme/ace-responsive-admin-template-WB0B30DGR';
    public $website			= 'https://wrapbootstrap.com/theme/ace-responsive-admin-template-WB0B30DGR';
    public $description		= 'Ace (v1.2) is a lightweight, feature-rich and easy to use admin template based on Bootstrap 3';
    public $version			= '1.2.0';
	public $type			= 'admin';
	
	/**
	 * Run() is triggered when the theme is loaded for use
	 *
	 * This should contain the main logic for the theme.
	 *
	 * @access	public
	 * @return	void
	 */
	public function run()
	{
		
	}
}

/* End of file theme.php */