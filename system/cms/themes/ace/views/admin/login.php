<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title><?php echo $this->settings->site_name; ?> - <?php echo lang('login_title');?></title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- -----------------
		STYLES
		------------------ -->
		
		<?php Asset::css('bootstrap.min.css'); ?>
		<?php Asset::css('font-awesome.min.css'); ?>
		<?php Asset::css('ace-fonts.css'); ?>
		<?php Asset::css('ace.css'); ?>
		<?php Asset::css('ace-rtl.min.css'); ?>
		<?php echo Asset::render_css('global') ?>
		
		<!--[if IE 7]>
		<?php Asset::css('font-awesome-ie7.min.css', false, 'ie7_styles'); ?>
		<?php echo Asset::render_css('ie7_styles') ?>
		<![endif]-->

		<!--[if lte IE 8]>
		<?php Asset::css('ace-ie.min.css', false, 'ie8_styles'); ?>
		<?php echo Asset::render_css('ie8_styles') ?>
		<![endif]-->

		<!-- -----------------
		SCRIPTS 
		------------------ -->
		
		<!--[if !IE]> -->
		<?php Asset::js('jquery-2.0.3.js', false, 'notie_scripts'); ?>
		<?php echo Asset::render_js('notie_scripts') ?>
		<!-- <![endif]-->

		<!--[if IE]>
		<?php Asset::js('jquery-1.10.2.js', false, 'ie_scripts'); ?>
		<?php echo Asset::render_js('ie_scripts') ?>
		<![endif]-->

		<!--[if lt IE 9]>
		<?php Asset::js('html5shiv.js', false, 'ie9_scripts'); ?>
		<?php Asset::js('respond.min.js', false, 'ie9_scripts'); ?>
		<?php echo Asset::render_js('ie9_scripts') ?>
		<![endif]-->
		
	</head>

	<body class="login-layout">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<?php 
								echo Asset::img('logo.png', $this->settings->site_name, array(
									'style' => 'margin-top: 20px; margin-bottom: -10px;', 
									'width' => 100)) 
								?>
								<h1>
									<span class="white"><?php echo $this->settings->site_name; ?></span>
								</h1>
								<h4 class="blue"><?php echo $this->settings->site_slogan; ?></h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<?php $this->load->view('admin/partials/notices') ?>
											
											<h4 class="header blue lighter bigger">
												<i class="icon-lock green"></i>
												<?php echo lang('login_title'); ?>
											</h4>

											<div class="space-6"></div>

											<?php echo form_open('admin/login'); ?>
											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="email" type="text" class="form-control" placeholder="<?php echo lang('global:username_or_email'); ?>" />
															<i class="icon-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="password" type="password" class="form-control" placeholder="<?php echo lang('global:password'); ?>" />
															<i class="icon-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input type="checkbox" class="ace" name="remember" id="remember-check" checked />
															<span class="lbl"> <?php echo lang('user:remember'); ?></span>
														</label>

														<button name="submit" type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="icon-key"></i>
															<?php echo lang('login_label'); ?>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
											<?php echo form_close(); ?>

										</div><!-- /widget-main -->
									</div><!-- /widget-body -->
								</div><!-- /login-box -->
							</div><!-- /position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
		</div><!-- /.main-container -->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			function show_box(id) {
			 jQuery('.widget-box.visible').removeClass('visible');
			 jQuery('#'+id).addClass('visible');
			}
		</script>
	</body>
	
	<?php echo Asset::render() ?>
</html>
