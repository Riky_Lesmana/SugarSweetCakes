<div class="page-header">
	<?php if(isset($template['page_title'])) { echo '<h1>'.lang_label($template['page_title']).'</h1>'; } ?>
	
	<?php file_partial('shortcuts'); ?>
</div>

<section class="item">
	<div class="content">
		<?php echo $content; ?>
	</div>
</section>