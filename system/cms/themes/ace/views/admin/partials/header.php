<div class="navbar navbar-default" id="navbar">
	<script type="text/javascript">
		try{ace.settings.check('navbar' , 'fixed')}catch(e){}
	</script>

	<div class="navbar-container" id="navbar-container">
		<div class="navbar-header pull-left">
			<a href="<?php echo base_url() . 'admin' ?>" class="navbar-brand">
				<small>
					<?php echo Asset::img('logo_alt.png', $this->settings->site_name, array(
						'style' => 'margin-top: -3px; max-height: 22px;', )) ?>
					<?php echo $this->settings->site_name; ?>
				</small>
			</a><!-- /.brand -->
		</div><!-- /.navbar-header -->

		<div class="navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
				
				<li class="light-blue">
					<a target="window" href="<?php echo base_url(); ?>">
						<i class="icon-external-link"></i>
						<span><?php echo lang('view_site'); ?></span>
					</a>
				</li>

				<li class="light-blue">
					<a data-toggle="dropdown" href="#" class="dropdown-toggle">
						<?php echo Asset::img('default_avatar.png', $this->current_user->username, array('class' => 'nav-user-photo', 'width' => 36)) ?>
						<span class="user-info">
							<small><?php echo lang('logged_in_as'); ?></small>
							<?php echo $this->current_user->display_name; ?>
						</span>

						<i class="icon-caret-down"></i>
					</a>

					<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
						<li>
							<a href="<?php echo base_url() . 'admin/users/view'; ?>">
								<i class="icon-user"></i>
								<?php echo lang_label('Profile'); ?>
							</a>
						</li>

						<li class="divider"></li>

						<li>
							<a href="<?php echo base_url() . 'admin/logout'; ?>">
								<i class="icon-off"></i>
								<?php echo lang('logout_label'); ?>
							</a>
						</li>
					</ul>
				</li>
			</ul><!-- /.ace-nav -->
		</div><!-- /.navbar-header -->
	</div><!-- /.container -->
</div>