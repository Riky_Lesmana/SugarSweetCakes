<ul class="nav nav-list">
	
	<li class="<?php if($this->uri->uri_string() == 'admin'){ ?>active<?php } ?>">
		<a href="/admin">
			<i class="icon-dashboard"></i>
			<span class="menu-text"> <?php echo lang('global:dashboard'); ?> </span>
		</a>
	</li>
	
	<?php foreach ($menu_items as $key => $menu_item){ ?>
		
		<?php
		// skip settings and profile menu
		// display it in another menu area
		if($key === 'Profil' OR $key === 'Profile'){
			continue;
		}
		
		// set default icon for core modules menu
		$icon_name = 'chevron-sign-right';
		if($key == 'Content' OR $key == 'Konten'){
			$icon_name = 'edit';
		}elseif($key == 'Koleksi'){
			$icon_name = 'book';
		}elseif($key == 'Pemesanan'){
			$icon_name = 'money';
		}elseif($key == 'Structure'){
			$icon_name = 'list-alt';
		}elseif($key == 'Data'){
			$icon_name = 'list';
		}elseif($key == 'Users' OR $key == 'Pengguna'){
			$icon_name = 'user';
		}elseif($key == 'Add-ons' OR $key == 'Pengaya'){
			$icon_name = 'plus';
		}elseif($key == 'Settings' OR $key == 'Pengaturan'){
			$icon_name = 'cog';
		}
		?>
		
		<?php if (is_array($menu_item)) { ?>
			
			<?php $is_submenu_active = is_active_menu($menu_item, $active_section); ?>
			
			<li class="<?php if($is_submenu_active === TRUE){ ?>active open<?php } ?>">
				<a href="<?php echo current_url(); ?>#" class="dropdown-toggle">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"> <?php echo lang_label($key); ?> </span>

					<b class="arrow icon-angle-down"></b>
				</a>
				
				<ul class="submenu">
				
				<?php foreach ($menu_item as $lang_key => $uri) { ?>
				
					<?php 
					/***************************
					 *	Don't have submenu
					 ***************************/
					if(! is_array($uri)){
					?>

					<li class="<?php if(strpos($this->uri->uri_string(), $uri) === 0){ ?>active open<?php } ?>">
					
						<a href="<?php echo site_url($uri); ?>">
							<i class="icon-double-angle-right"></i>
							<?php echo lang_label($lang_key); ?>
						</a>
						
					</li>
					
					<?php 
					/***************************
					 *	Have submenu
					 ***************************/
					}else{ // if 
					?>
					
					<?php $is_section_active = is_active_menu($uri, $active_section); ?>
					
					<li class="<?php if($is_section_active){ ?>active open<?php } ?>">
					
					<a href="#" class="dropdown-toggle">
						<i class="icon-double-angle-right"></i>
						<?php echo lang_label($lang_key); ?>
						<b class="arrow icon-angle-down"></b>
					</a>
					
					<ul class="submenu">
					
					<?php foreach($uri as $section_name => $section_uri){ ?>
						<?php $is_section_active = is_active_menu($section_uri, $active_section); ?>
						<li class="<?php if($is_section_active){ ?>active<?php } ?>">
							<a href="<?php echo site_url($section_uri); ?>">
								<i class="icon-chevron-sign-right"></i>
								
								<?php echo $section_name; ?>
							</a>
						</li>
					
					<?php } // foreach ?>
					
					</ul>
					
					</li>
					
					<?php } // if ?>
					
				<?php } ?>

				</ul>
			</li>
			
		<?php }else{ ?>
			
			<li class="<?php if(strpos($this->uri->uri_string(), $menu_item) === 0){ ?>active open<?php } ?>">
				<a href="<?php echo base_url() . $menu_item; ?>">
					<i class="icon-<?php echo $icon_name; ?>"></i>
					<span class="menu-text"><?php echo $key; ?></span>
				</a>
			</li>
			
		<?php } ?>
	<?php } ?>
	
</ul><!-- /.nav-list -->